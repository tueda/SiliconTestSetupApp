#pragma once

#include <windows.h>
#include <iostream>
#include "prologix_gpibusb.h"
#include "IVCVCtrl.h"
#include "TUSBPIOCtrl.h"
#include "TeslaProbeCtrl.h"
#include "DataHandler.h"
#include <vector>
#include <map>

#define uA 1e-6
#define kHz 1e3
#define pF 1e-12
#define kOhm 1e3

ref class SiliconTestSetup
{
private:
	DataHandler* dh;
	serial_interface * si;
	TeslaProbeCtrl *tpc;
	TUSBPIOCtrl *usbpio;
	IVCVCtrl *ivcv;
	int LCRfunctype; // 0: Cp-D ,   1: Z-theta(deg)

	std::vector<double> *vol1;
	std::vector<double> *curr;
	std::vector<double> *vol2;
	std::vector<double> *cap;
	std::vector<double> *deg;
	std::vector<double>* freq;

	bool isGPIBOpen;
//	std::vector<power_supply*> *psl;
//	std::vector<LCR_meter*> *lcrl;
	std::map<power_supply*, std::string>* psl;
	std::map<LCR_meter*, std::string> * lcrl;

	// active device for IV/CV measurement
	power_supply* activeps;
	std::string* activepsname;
	LCR_meter * activelcr;
	std::string* activelcrname;

	//  active device for QA pieces
	power_supply* activepsHV;
	std::string* activepsHVname;
	power_supply* activepsVtest;
	std::string* activepsVtestname;
	LCR_meter* activelcr1;
	std::string* activelcr1name;
	LCR_meter* activelcr2;
	std::string* activelcr2name;



	// run type for IV/CV measurement
	bool runIV;
	bool runCV;
	bool runCF;

public:
	SiliconTestSetup();
	~SiliconTestSetup() {}
	bool isneg;
	float curlimit ;
	float volstart;
	float volend;
	float volstep ;
	float interval;
	float rampingt;
	float frequency;
	float freqstart;
	float freqend;
	float fixedvolt;
	float singlevolt;
	float singlefreq;
	float voltrange;


	int initialize();
	std::string getNow();
	TUSBPIOCtrl* getTUSBPIO() { return usbpio; }
	void LegacyRun();
	bool getGPIBStatus() {return isGPIBOpen;}
	DataHandler* getDataHandler() {return dh; }
	int OpenApplication(char* szCmd);
	int OpenTeslaT200Control();
	int CreateNewConsole(char* szCmd);
	int setPowerSupply(std::string str, int addr);
	int setLCRmeter(std::string str, int addr);
	int removePowerSupply(std::string str, int addr);
	int removeLCRmeter(std::string str, int addr);
	std::vector<double> Volt1() { return *vol1; }
	std::vector<double> Current() { return *curr; }
	std::vector<double> Volt2() { return *vol2; }
	std::vector<double> Capacitance() { return *cap; }
	std::vector<double> Degree() { return *deg; }
	void DoOpenCalibration();
	void DoShortCalibration();
	std::vector<double> Frequency() { return *freq; }
	void setLCRfunctype(int type) { LCRfunctype = type; }
	void RecordHeaderToFile();
//	void RecodeDataToFile();
	void ReadSingleCurrent(float &cur);
	void ReadSingleImpedance(float &cap, float &deg);
/*
	void SetTestV(std::string psname);
	void SetHV(std::string psname);
	void SetCint(std::string lcrname);
	void SetCcp(std::string lcrname);
*/
	void ClearData();
	void setCurrentActiveDevice(std::string psname, std::string lcrname);
	void setCurrentActiveDeviceQA(std::string psHVname, std::string psVtestname, std::string lcr1name, std::string lcr2name);
	int stopRun();
	int runIVCVScan(bool iv, bool cv);
	int runCFScan();

	std::map<power_supply*, std::string>* getPowerSupplies() { return psl; }
	std::map<LCR_meter*, std::string>* getLCRmeters() { return lcrl; }
};

