#pragma once
#include "SiliconTestSetup.h"
#include "IVCVControlGUI.h"
#include "TUSBPIOControlGUI.h"
#include <iostream>
#include <sstream>
#include <string>
#include <msclr/marshal_cppstd.h>
#include "StripQAControlGUI.h"

namespace SiliconTestSetupApp {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ControlGUI の概要
	/// </summary>
	public ref class ControlGUI : public System::Windows::Forms::Form
	{
	public:
		ControlGUI(void)
		{
			InitializeComponent();
			//
			//TODO: ここにコンストラクター コードを追加します
			//
		}

	protected:
		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		~ControlGUI()
		{
			if (components)
			{
				delete components;
			}
		}
	private: SiliconTestSetup^ sts;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button3;

	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Timer^ timer1;
	private: System::Windows::Forms::GroupBox^ groupBox1;
	private: System::Windows::Forms::ComboBox^ comboBox1;
	private: System::Windows::Forms::ComboBox^ comboBox2;
	private: System::Windows::Forms::ComboBox^ comboBox3;
	private: System::Windows::Forms::ComboBox^ comboBox4;
	private: System::Windows::Forms::ComboBox^ comboBox5;
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::TextBox^ textBox3;
	private: System::Windows::Forms::TextBox^ textBox4;
	private: System::Windows::Forms::TextBox^ textBox5;
	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::Button^ button5;
	private: System::Windows::Forms::Button^ button6;
	private: System::Windows::Forms::Button^ button7;
	private: System::Windows::Forms::Button^ button8;
	private: System::Windows::Forms::GroupBox^ groupBox2;
	private: System::Windows::Forms::Button^ button9;
	private: System::Windows::Forms::TextBox^ textBox6;
	private: System::Windows::Forms::ComboBox^ comboBox6;
	private: System::Windows::Forms::Button^ button10;
	private: System::Windows::Forms::TextBox^ textBox7;
	private: System::Windows::Forms::ComboBox^ comboBox7;
	private: System::Windows::Forms::Button^ button11;
	private: System::Windows::Forms::TextBox^ textBox8;
	private: System::Windows::Forms::ComboBox^ comboBox8;
	private: System::Windows::Forms::Button^ button12;
	private: System::Windows::Forms::TextBox^ textBox9;
	private: System::Windows::Forms::ComboBox^ comboBox9;
	private: System::Windows::Forms::GroupBox^ groupBox3;
	private: System::Windows::Forms::Button^ button13;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Button^ button14;


	private: System::ComponentModel::IContainer^ components;
	protected:

	private:
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(ControlGUI::typeid));
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox3 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox4 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox5 = (gcnew System::Windows::Forms::ComboBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox6 = (gcnew System::Windows::Forms::ComboBox());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox7 = (gcnew System::Windows::Forms::ComboBox());
			this->button11 = (gcnew System::Windows::Forms::Button());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox8 = (gcnew System::Windows::Forms::ComboBox());
			this->button12 = (gcnew System::Windows::Forms::Button());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox9 = (gcnew System::Windows::Forms::ComboBox());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->button13 = (gcnew System::Windows::Forms::Button());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->button14 = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::SystemColors::ActiveCaptionText;
			this->button1->Font = (gcnew System::Drawing::Font(L"Century", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button1->ForeColor = System::Drawing::SystemColors::ControlLight;
			this->button1->Location = System::Drawing::Point(17, 591);
			this->button1->Margin = System::Windows::Forms::Padding(5, 4, 5, 4);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(410, 51);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Legacy Text based IV/CV software";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &ControlGUI::button1_Click);
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::Color::Lime;
			this->button2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 16, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button2->Location = System::Drawing::Point(33, 436);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(162, 100);
			this->button2->TabIndex = 1;
			this->button2->Text = L"Simple IV/CV";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &ControlGUI::button2_Click);
			// 
			// button3
			// 
			this->button3->Font = (gcnew System::Drawing::Font(L"Century", 10, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->button3->ForeColor = System::Drawing::Color::Blue;
			this->button3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button3.Image")));
			this->button3->Location = System::Drawing::Point(10, 668);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(417, 160);
			this->button3->TabIndex = 2;
			this->button3->Text = L"TUSB Form";
			this->button3->TextAlign = System::Drawing::ContentAlignment::TopLeft;
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &ControlGUI::button3_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label1->Location = System::Drawing::Point(7, 15);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(62, 24);
			this->label1->TabIndex = 3;
			this->label1->Text = L"GPIB";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label2->ForeColor = System::Drawing::Color::Red;
			this->label2->Location = System::Drawing::Point(72, 15);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(60, 24);
			this->label2->TabIndex = 4;
			this->label2->Text = L"Error";
			// 
			// timer1
			// 
			this->timer1->Tick += gcnew System::EventHandler(this, &ControlGUI::timer1_Tick);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(548, 16);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(137, 42);
			this->groupBox1->TabIndex = 5;
			this->groupBox1->TabStop = false;
			// 
			// comboBox1
			// 
			this->comboBox1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"keithley2410", L"keithley6517A", L"keithley6517B" });
			this->comboBox1->Location = System::Drawing::Point(25, 38);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(171, 26);
			this->comboBox1->TabIndex = 6;
			this->comboBox1->Text = L"keithley2410";
			// 
			// comboBox2
			// 
			this->comboBox2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"keithley2410", L"keithley6517A", L"keithley6517B" });
			this->comboBox2->Location = System::Drawing::Point(25, 82);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(171, 26);
			this->comboBox2->TabIndex = 7;
			this->comboBox2->Text = L"keithley6517A";
			// 
			// comboBox3
			// 
			this->comboBox3->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->comboBox3->FormattingEnabled = true;
			this->comboBox3->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"keithley2410", L"keithley6517A", L"keithley6517B" });
			this->comboBox3->Location = System::Drawing::Point(25, 128);
			this->comboBox3->Name = L"comboBox3";
			this->comboBox3->Size = System::Drawing::Size(171, 26);
			this->comboBox3->TabIndex = 8;
			this->comboBox3->Text = L"keithley6517A";
			// 
			// comboBox4
			// 
			this->comboBox4->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->comboBox4->FormattingEnabled = true;
			this->comboBox4->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"keithley2410", L"keithley6517A", L"keithley6517B" });
			this->comboBox4->Location = System::Drawing::Point(25, 172);
			this->comboBox4->Name = L"comboBox4";
			this->comboBox4->Size = System::Drawing::Size(171, 26);
			this->comboBox4->TabIndex = 9;
			// 
			// comboBox5
			// 
			this->comboBox5->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->comboBox5->FormattingEnabled = true;
			this->comboBox5->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"keithley2410", L"keithley6517A", L"keithley6517B" });
			this->comboBox5->Location = System::Drawing::Point(25, 218);
			this->comboBox5->Name = L"comboBox5";
			this->comboBox5->Size = System::Drawing::Size(171, 26);
			this->comboBox5->TabIndex = 10;
			// 
			// textBox1
			// 
			this->textBox1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox1->Location = System::Drawing::Point(217, 38);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(31, 25);
			this->textBox1->TabIndex = 11;
			this->textBox1->Text = L"24";
			// 
			// textBox2
			// 
			this->textBox2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox2->Location = System::Drawing::Point(217, 82);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(31, 25);
			this->textBox2->TabIndex = 12;
			this->textBox2->Text = L"28";
			// 
			// textBox3
			// 
			this->textBox3->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox3->Location = System::Drawing::Point(217, 128);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(31, 25);
			this->textBox3->TabIndex = 13;
			this->textBox3->Text = L"29";
			// 
			// textBox4
			// 
			this->textBox4->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox4->Location = System::Drawing::Point(217, 172);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(31, 25);
			this->textBox4->TabIndex = 14;
			// 
			// textBox5
			// 
			this->textBox5->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox5->Location = System::Drawing::Point(217, 218);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(31, 25);
			this->textBox5->TabIndex = 15;
			// 
			// button4
			// 
			this->button4->BackColor = System::Drawing::Color::Silver;
			this->button4->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button4->Location = System::Drawing::Point(267, 33);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(65, 39);
			this->button4->TabIndex = 16;
			this->button4->Text = L"set";
			this->button4->UseVisualStyleBackColor = false;
			this->button4->Click += gcnew System::EventHandler(this, &ControlGUI::button4_Click);
			// 
			// button5
			// 
			this->button5->BackColor = System::Drawing::Color::Silver;
			this->button5->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button5->Location = System::Drawing::Point(267, 78);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(65, 39);
			this->button5->TabIndex = 17;
			this->button5->Text = L"set";
			this->button5->UseVisualStyleBackColor = false;
			this->button5->Click += gcnew System::EventHandler(this, &ControlGUI::button5_Click);
			// 
			// button6
			// 
			this->button6->BackColor = System::Drawing::Color::Silver;
			this->button6->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button6->Location = System::Drawing::Point(267, 123);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(65, 39);
			this->button6->TabIndex = 18;
			this->button6->Text = L"set";
			this->button6->UseVisualStyleBackColor = false;
			this->button6->Click += gcnew System::EventHandler(this, &ControlGUI::button6_Click);
			// 
			// button7
			// 
			this->button7->BackColor = System::Drawing::Color::Silver;
			this->button7->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button7->Location = System::Drawing::Point(267, 168);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(65, 39);
			this->button7->TabIndex = 19;
			this->button7->Text = L"set";
			this->button7->UseVisualStyleBackColor = false;
			this->button7->Click += gcnew System::EventHandler(this, &ControlGUI::button7_Click);
			// 
			// button8
			// 
			this->button8->BackColor = System::Drawing::Color::Silver;
			this->button8->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button8->Location = System::Drawing::Point(267, 213);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(65, 39);
			this->button8->TabIndex = 20;
			this->button8->Text = L"set";
			this->button8->UseVisualStyleBackColor = false;
			this->button8->Click += gcnew System::EventHandler(this, &ControlGUI::button8_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->button8);
			this->groupBox2->Controls->Add(this->button7);
			this->groupBox2->Controls->Add(this->button6);
			this->groupBox2->Controls->Add(this->button5);
			this->groupBox2->Controls->Add(this->button4);
			this->groupBox2->Controls->Add(this->textBox5);
			this->groupBox2->Controls->Add(this->textBox4);
			this->groupBox2->Controls->Add(this->textBox3);
			this->groupBox2->Controls->Add(this->textBox2);
			this->groupBox2->Controls->Add(this->textBox1);
			this->groupBox2->Controls->Add(this->comboBox5);
			this->groupBox2->Controls->Add(this->comboBox4);
			this->groupBox2->Controls->Add(this->comboBox3);
			this->groupBox2->Controls->Add(this->comboBox2);
			this->groupBox2->Controls->Add(this->comboBox1);
			this->groupBox2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 15, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->groupBox2->Location = System::Drawing::Point(17, 84);
			this->groupBox2->Margin = System::Windows::Forms::Padding(5, 4, 5, 4);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(5, 4, 5, 4);
			this->groupBox2->Size = System::Drawing::Size(348, 255);
			this->groupBox2->TabIndex = 21;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Power Supply";
			// 
			// button9
			// 
			this->button9->BackColor = System::Drawing::Color::Silver;
			this->button9->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button9->Location = System::Drawing::Point(603, 147);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(65, 39);
			this->button9->TabIndex = 24;
			this->button9->Text = L"set";
			this->button9->UseVisualStyleBackColor = false;
			this->button9->Click += gcnew System::EventHandler(this, &ControlGUI::button9_Click);
			// 
			// textBox6
			// 
			this->textBox6->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox6->Location = System::Drawing::Point(553, 152);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(31, 25);
			this->textBox6->TabIndex = 23;
			this->textBox6->Text = L"2";
			// 
			// comboBox6
			// 
			this->comboBox6->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->comboBox6->FormattingEnabled = true;
			this->comboBox6->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"hp4192A", L"hp4284A" });
			this->comboBox6->Location = System::Drawing::Point(393, 152);
			this->comboBox6->Name = L"comboBox6";
			this->comboBox6->Size = System::Drawing::Size(139, 26);
			this->comboBox6->TabIndex = 22;
			this->comboBox6->Text = L"hp4192A";
			// 
			// button10
			// 
			this->button10->BackColor = System::Drawing::Color::Silver;
			this->button10->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button10->Location = System::Drawing::Point(603, 192);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(65, 39);
			this->button10->TabIndex = 27;
			this->button10->Text = L"set";
			this->button10->UseVisualStyleBackColor = false;
			this->button10->Click += gcnew System::EventHandler(this, &ControlGUI::button10_Click);
			// 
			// textBox7
			// 
			this->textBox7->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox7->Location = System::Drawing::Point(553, 196);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(31, 25);
			this->textBox7->TabIndex = 26;
			this->textBox7->Text = L"3";
			// 
			// comboBox7
			// 
			this->comboBox7->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->comboBox7->FormattingEnabled = true;
			this->comboBox7->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"hp4192A", L"hp4284A" });
			this->comboBox7->Location = System::Drawing::Point(393, 196);
			this->comboBox7->Name = L"comboBox7";
			this->comboBox7->Size = System::Drawing::Size(139, 26);
			this->comboBox7->TabIndex = 25;
			this->comboBox7->Text = L"hp4284A";
			// 
			// button11
			// 
			this->button11->BackColor = System::Drawing::Color::Silver;
			this->button11->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button11->Location = System::Drawing::Point(603, 237);
			this->button11->Name = L"button11";
			this->button11->Size = System::Drawing::Size(65, 39);
			this->button11->TabIndex = 30;
			this->button11->Text = L"set";
			this->button11->UseVisualStyleBackColor = false;
			this->button11->Click += gcnew System::EventHandler(this, &ControlGUI::button11_Click);
			// 
			// textBox8
			// 
			this->textBox8->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox8->Location = System::Drawing::Point(553, 242);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(31, 25);
			this->textBox8->TabIndex = 29;
			this->textBox8->Text = L"4";
			// 
			// comboBox8
			// 
			this->comboBox8->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->comboBox8->FormattingEnabled = true;
			this->comboBox8->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"hp4192A", L"hp4284A" });
			this->comboBox8->Location = System::Drawing::Point(393, 242);
			this->comboBox8->Name = L"comboBox8";
			this->comboBox8->Size = System::Drawing::Size(139, 26);
			this->comboBox8->TabIndex = 28;
			this->comboBox8->Text = L"hp4284A";
			// 
			// button12
			// 
			this->button12->BackColor = System::Drawing::Color::Silver;
			this->button12->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button12->Location = System::Drawing::Point(603, 280);
			this->button12->Name = L"button12";
			this->button12->Size = System::Drawing::Size(65, 39);
			this->button12->TabIndex = 33;
			this->button12->Text = L"set";
			this->button12->UseVisualStyleBackColor = false;
			this->button12->Click += gcnew System::EventHandler(this, &ControlGUI::button12_Click);
			// 
			// textBox9
			// 
			this->textBox9->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox9->Location = System::Drawing::Point(553, 285);
			this->textBox9->Name = L"textBox9";
			this->textBox9->Size = System::Drawing::Size(31, 25);
			this->textBox9->TabIndex = 32;
			// 
			// comboBox9
			// 
			this->comboBox9->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->comboBox9->FormattingEnabled = true;
			this->comboBox9->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"hp4192A", L"hp4284A" });
			this->comboBox9->Location = System::Drawing::Point(393, 285);
			this->comboBox9->Name = L"comboBox9";
			this->comboBox9->Size = System::Drawing::Size(139, 26);
			this->comboBox9->TabIndex = 31;
			// 
			// groupBox3
			// 
			this->groupBox3->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 15, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->groupBox3->Location = System::Drawing::Point(383, 84);
			this->groupBox3->Margin = System::Windows::Forms::Padding(5, 4, 5, 4);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Padding = System::Windows::Forms::Padding(5, 4, 5, 4);
			this->groupBox3->Size = System::Drawing::Size(300, 232);
			this->groupBox3->TabIndex = 34;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"LCR meter";
			// 
			// button13
			// 
			this->button13->Font = (gcnew System::Drawing::Font(L"Century", 9.75F, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->button13->ForeColor = System::Drawing::Color::Blue;
			this->button13->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button13.Image")));
			this->button13->ImageAlign = System::Drawing::ContentAlignment::TopCenter;
			this->button13->Location = System::Drawing::Point(437, 578);
			this->button13->Margin = System::Windows::Forms::Padding(5, 4, 5, 4);
			this->button13->Name = L"button13";
			this->button13->Size = System::Drawing::Size(248, 249);
			this->button13->TabIndex = 35;
			this->button13->Text = L"Tesla T200";
			this->button13->TextAlign = System::Drawing::ContentAlignment::TopLeft;
			this->button13->UseVisualStyleBackColor = true;
			this->button13->Click += gcnew System::EventHandler(this, &ControlGUI::button13_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Elephant", 24, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->ForeColor = System::Drawing::Color::Blue;
			this->label3->Location = System::Drawing::Point(13, 16);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(374, 62);
			this->label3->TabIndex = 36;
			this->label3->Text = L"Control Panel";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Century", 24, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label4->ForeColor = System::Drawing::Color::Magenta;
			this->label4->Location = System::Drawing::Point(167, 343);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(315, 57);
			this->label4->TabIndex = 37;
			this->label4->Text = L"Applications";
			// 
			// button14
			// 
			this->button14->BackColor = System::Drawing::Color::Aqua;
			this->button14->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 16, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button14->Location = System::Drawing::Point(234, 437);
			this->button14->Name = L"button14";
			this->button14->Size = System::Drawing::Size(162, 99);
			this->button14->TabIndex = 38;
			this->button14->Text = L"Strip QA";
			this->button14->UseVisualStyleBackColor = false;
			this->button14->Click += gcnew System::EventHandler(this, &ControlGUI::button14_Click);
			// 
			// ControlGUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(10, 18);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(703, 844);
			this->Controls->Add(this->button14);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->button13);
			this->Controls->Add(this->button12);
			this->Controls->Add(this->textBox9);
			this->Controls->Add(this->comboBox9);
			this->Controls->Add(this->button11);
			this->Controls->Add(this->textBox8);
			this->Controls->Add(this->comboBox8);
			this->Controls->Add(this->button10);
			this->Controls->Add(this->textBox7);
			this->Controls->Add(this->comboBox7);
			this->Controls->Add(this->button9);
			this->Controls->Add(this->textBox6);
			this->Controls->Add(this->comboBox6);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->groupBox3);
			this->Name = L"ControlGUI";
			this->Text = L"ControlGUI";
			this->Load += gcnew System::EventHandler(this, &ControlGUI::ControlGUI_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
	private:int SetPowerSupply(String^ det, String^ addr, System::Windows::Forms::Button^ button) {
		std::string str = msclr::interop::marshal_as<std::string>(det);
		if (addr == L"" || str == "") {
			String^ mess_str = L"Please put source meter and GPIB address";
			System::Windows::Forms::MessageBox::Show(mess_str);
			button->BackColor = System::Drawing::Color::Tomato;
			return -1;
		}
		int id = int::Parse(addr);
		std::cout << str << " " << id << std::endl;
		if (button->BackColor == System::Drawing::Color::LimeGreen) {
			sts->removePowerSupply(str, id);
			button->BackColor = System::Drawing::Color::Silver;
		}else{
			int ret=sts->setPowerSupply(str, id);
			if (ret == 0)button->BackColor = System::Drawing::Color::LimeGreen;
			else button->BackColor = System::Drawing::Color::Tomato;
		}
		return 0;
	}
	private:int SetLCRmeter(String^ det, String^ addr, System::Windows::Forms::Button^ button) {
		std::string str = msclr::interop::marshal_as<std::string>(det);
		if (addr == L"" || str == "") {
			String^ mess_str = L"Please put LCR meter and GPIB address";
			System::Windows::Forms::MessageBox::Show(mess_str);
			button->BackColor = System::Drawing::Color::Tomato;
			return -1;
		}
		int id = int::Parse(addr);
		std::cout << str << " " << id << std::endl;
		if (button->BackColor == System::Drawing::Color::LimeGreen) {
			sts->removeLCRmeter(str, id);
			button->BackColor = System::Drawing::Color::Silver;
		}else{
			int ret=sts->setLCRmeter(str, id);
			if(ret==0)button->BackColor = System::Drawing::Color::LimeGreen;
			else button->BackColor = System::Drawing::Color::Tomato;
		}
		return 0;

	}
#pragma endregion
	private: System::Void ControlGUI_Load(System::Object^ sender, System::EventArgs^ e) {
		sts = gcnew SiliconTestSetup();
		sts->initialize();
		timer1->Start();

	}
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {

		sts->LegacyRun();
	}
	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {
		IVCVControlGUI^ ivcvform = gcnew IVCVControlGUI(sts);
		ivcvform->Show();

	}
	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) {
		sts->getTUSBPIO()->CloseDevice();
		TUSBPIOControlGUI^ tusbform = gcnew TUSBPIOControlGUI();
		tusbform->Show();
	}
	private: System::Void timer1_Tick(System::Object^ sender, System::EventArgs^ e) {
		
		if (sts->getGPIBStatus()) {
			this->label2->ForeColor = System::Drawing::Color::Green;
			this->label2->Text = L"OK";
		}
		else {
			this->label2->ForeColor = System::Drawing::Color::Red;
			this->label2->Text = L"Error";
		}
			
	}
	private: System::Void button13_Click(System::Object^ sender, System::EventArgs^ e) {
		std::cout << sts->OpenTeslaT200Control() << std::endl;
	}

	private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e) {
		// 1st power supply	
		SetPowerSupply(comboBox1->Text, textBox1->Text,button4);

	}
	private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e) {
		// 2nd power supply
		SetPowerSupply(comboBox2->Text, textBox2->Text, button5);

	}
	private: System::Void button6_Click(System::Object^ sender, System::EventArgs^ e) {
		// 3rd power supply
		SetPowerSupply(comboBox3->Text, textBox3->Text, button6);

	}
	private: System::Void button7_Click(System::Object^ sender, System::EventArgs^ e) {
		// 4th power supply
		SetPowerSupply(comboBox4->Text, textBox4->Text, button7);

	}
	private: System::Void button8_Click(System::Object^ sender, System::EventArgs^ e) {
		// 5th power supply
		SetPowerSupply(comboBox5->Text, textBox5->Text, button8);

	}
	private: System::Void button9_Click(System::Object^ sender, System::EventArgs^ e) {
		// 1st LCR meter
		SetLCRmeter(comboBox6->Text, textBox6->Text, button9);

	}
	private: System::Void button10_Click(System::Object^ sender, System::EventArgs^ e) {
		// 2nd LCR meter
		SetLCRmeter(comboBox7->Text, textBox7->Text, button10);

	}
	private: System::Void button11_Click(System::Object^ sender, System::EventArgs^ e) {
		// 3rd LCR meter
		SetLCRmeter(comboBox8->Text, textBox8->Text, button11);

	}
	private: System::Void button12_Click(System::Object^ sender, System::EventArgs^ e) {
		// 4th LCR meter 
		SetLCRmeter(comboBox9->Text, textBox9->Text, button12);

	}
private: System::Void button14_Click(System::Object^ sender, System::EventArgs^ e) {
	StripQAControlGUI^ QAform = gcnew StripQAControlGUI(sts);
	QAform->Show();
}
};
}
