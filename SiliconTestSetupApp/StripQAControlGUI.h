#pragma once
#include "SiliconTestSetup.h"
#include "IVCVControlGUI.h"
#include "TUSBPIOCtrl.h"
#include "windows.h"
//#include "number.h"
#include <iostream>
#include <sstream>
#include <string>
#include <msclr/marshal_cppstd.h>


namespace SiliconTestSetupApp {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// StripQAControlGUI の概要
	/// </summary>
	public ref class StripQAControlGUI : public System::Windows::Forms::Form
	{
	public:
		StripQAControlGUI(SiliconTestSetup^ _sts)
		{
			sts = _sts;
			InitializeComponent();

		}
		StripQAControlGUI(void)
		{

			InitializeComponent();
			//
			//TODO: ここにコンストラクター コードを追加します
			//
		}

	protected:
		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		~StripQAControlGUI()
		{
			if (components)
			{
				delete components;
			}
		}
	private: double nowTime = 0;
	private: SiliconTestSetup^ sts;
	private: Thread^ tscan;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::Button^ button5;
	private: System::Windows::Forms::GroupBox^ groupBox1;
	private: System::Windows::Forms::GroupBox^ groupBox2;
	private: System::Windows::Forms::GroupBox^ groupBox3;
	private: System::Windows::Forms::GroupBox^ groupBox4;
	private: System::Windows::Forms::GroupBox^ groupBox5;
	private: System::Windows::Forms::GroupBox^ groupBox6;
	private: System::Windows::Forms::Label^ label1;


	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::ComboBox^ comboBox4;
	private: System::Windows::Forms::ComboBox^ comboBox3;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::ComboBox^ comboBox5;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::ComboBox^ comboBox6;
	private: System::Windows::Forms::GroupBox^ groupBox7;
	private: System::Windows::Forms::TextBox^ textBox5;
	private: System::Windows::Forms::Label^ label9;
	private: System::Windows::Forms::TextBox^ textBox4;
	private: System::Windows::Forms::Label^ label8;
	private: System::Windows::Forms::TextBox^ textBox3;
	private: System::Windows::Forms::Label^ label7;
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::RadioButton^ radioButton2;
	private: System::Windows::Forms::RadioButton^ radioButton1;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::TextBox^ textBox6;
	private: System::Windows::Forms::RadioButton^ radioButton3;
	private: System::Windows::Forms::Label^ label10;
	private: System::Windows::Forms::TextBox^ textBox7;
	private: System::Windows::Forms::RadioButton^ radioButton4;
	private: System::Windows::Forms::Label^ label11;
	private: System::Windows::Forms::Label^ label13;
	private: System::Windows::Forms::TextBox^ textBox8;
	private: System::Windows::Forms::TextBox^ textBox9;
	private: System::Windows::Forms::Label^ label12;
	private: System::Windows::Forms::TextBox^ textBox10;
	private: System::Windows::Forms::RadioButton^ radioButton5;
	private: System::Windows::Forms::Label^ label14;
	private: System::Windows::Forms::TextBox^ textBox11;
	private: System::Windows::Forms::RadioButton^ radioButton6;
	private: System::Windows::Forms::Label^ label15;
	private: System::Windows::Forms::Label^ label16;
	private: System::Windows::Forms::Label^ label17;
	private: System::Windows::Forms::TextBox^ textBox12;
	private: System::Windows::Forms::TextBox^ textBox13;
	private: System::Windows::Forms::TextBox^ textBox19;
	private: System::Windows::Forms::Label^ label23;
	private: System::Windows::Forms::TextBox^ textBox14;
	private: System::Windows::Forms::Label^ label18;
	private: System::Windows::Forms::TextBox^ textBox18;
	private: System::Windows::Forms::TextBox^ textBox15;
	private: System::Windows::Forms::Label^ label22;
	private: System::Windows::Forms::Label^ label19;
	private: System::Windows::Forms::Label^ label21;
	private: System::Windows::Forms::TextBox^ textBox16;
	private: System::Windows::Forms::TextBox^ textBox17;
	private: System::Windows::Forms::Label^ label20;
	private: System::Windows::Forms::TextBox^ textBox25;
	private: System::Windows::Forms::TextBox^ textBox20;
	private: System::Windows::Forms::Label^ label29;
	private: System::Windows::Forms::RadioButton^ radioButton7;
	private: System::Windows::Forms::Label^ label24;
	private: System::Windows::Forms::TextBox^ textBox21;
	private: System::Windows::Forms::RadioButton^ radioButton8;
	private: System::Windows::Forms::TextBox^ textBox22;
	private: System::Windows::Forms::Label^ label25;
	private: System::Windows::Forms::Label^ label28;
	private: System::Windows::Forms::Label^ label26;
	private: System::Windows::Forms::TextBox^ textBox24;
	private: System::Windows::Forms::Label^ label27;
	private: System::Windows::Forms::TextBox^ textBox23;
	private: System::Windows::Forms::Button^ button7;
	private: System::Windows::Forms::Button^ button6;
	private: System::Windows::Forms::GroupBox^ groupBox8;
	private: System::Windows::Forms::GroupBox^ groupBox9;
	private: System::Windows::Forms::Button^ button8;
	private: System::Windows::Forms::Button^ button9;
private: System::Windows::Forms::GroupBox^ groupBox10;
private: System::Windows::Forms::RadioButton^ radioButton11;
private: System::Windows::Forms::RadioButton^ radioButton10;
private: System::Windows::Forms::RadioButton^ radioButton9;

	protected:

	private:
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox10 = (gcnew System::Windows::Forms::GroupBox());
			this->radioButton11 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton10 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton9 = (gcnew System::Windows::Forms::RadioButton());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->radioButton2 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton1 = (gcnew System::Windows::Forms::RadioButton());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->radioButton3 = (gcnew System::Windows::Forms::RadioButton());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->radioButton4 = (gcnew System::Windows::Forms::RadioButton());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->textBox10 = (gcnew System::Windows::Forms::TextBox());
			this->radioButton5 = (gcnew System::Windows::Forms::RadioButton());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->textBox11 = (gcnew System::Windows::Forms::TextBox());
			this->radioButton6 = (gcnew System::Windows::Forms::RadioButton());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->textBox12 = (gcnew System::Windows::Forms::TextBox());
			this->textBox13 = (gcnew System::Windows::Forms::TextBox());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->textBox19 = (gcnew System::Windows::Forms::TextBox());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->textBox14 = (gcnew System::Windows::Forms::TextBox());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->textBox18 = (gcnew System::Windows::Forms::TextBox());
			this->textBox15 = (gcnew System::Windows::Forms::TextBox());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->textBox16 = (gcnew System::Windows::Forms::TextBox());
			this->textBox17 = (gcnew System::Windows::Forms::TextBox());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->textBox25 = (gcnew System::Windows::Forms::TextBox());
			this->textBox20 = (gcnew System::Windows::Forms::TextBox());
			this->label29 = (gcnew System::Windows::Forms::Label());
			this->radioButton7 = (gcnew System::Windows::Forms::RadioButton());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->textBox21 = (gcnew System::Windows::Forms::TextBox());
			this->radioButton8 = (gcnew System::Windows::Forms::RadioButton());
			this->textBox22 = (gcnew System::Windows::Forms::TextBox());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->label28 = (gcnew System::Windows::Forms::Label());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->textBox24 = (gcnew System::Windows::Forms::TextBox());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->textBox23 = (gcnew System::Windows::Forms::TextBox());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->comboBox6 = (gcnew System::Windows::Forms::ComboBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->comboBox5 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox4 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox3 = (gcnew System::Windows::Forms::ComboBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox7 = (gcnew System::Windows::Forms::GroupBox());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->groupBox8 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox9 = (gcnew System::Windows::Forms::GroupBox());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			this->groupBox10->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->groupBox6->SuspendLayout();
			this->groupBox7->SuspendLayout();
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(16, 279);
			this->button1->Margin = System::Windows::Forms::Padding(2);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(88, 59);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Rbias";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(15, 279);
			this->button2->Margin = System::Windows::Forms::Padding(2);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(88, 59);
			this->button2->TabIndex = 1;
			this->button2->Text = L"Rint";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(16, 279);
			this->button3->Margin = System::Windows::Forms::Padding(2);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(88, 59);
			this->button3->TabIndex = 2;
			this->button3->Text = L"Cint";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button3_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(15, 279);
			this->button4->Margin = System::Windows::Forms::Padding(2);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(88, 59);
			this->button4->TabIndex = 3;
			this->button4->Text = L"Ccpl";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button4_Click);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(11, 279);
			this->button5->Margin = System::Windows::Forms::Padding(2);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(88, 59);
			this->button5->TabIndex = 4;
			this->button5->Text = L"PTP";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button5_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->groupBox10);
			this->groupBox1->Controls->Add(this->textBox5);
			this->groupBox1->Controls->Add(this->label9);
			this->groupBox1->Controls->Add(this->textBox4);
			this->groupBox1->Controls->Add(this->label8);
			this->groupBox1->Controls->Add(this->textBox3);
			this->groupBox1->Controls->Add(this->label7);
			this->groupBox1->Controls->Add(this->textBox2);
			this->groupBox1->Controls->Add(this->label6);
			this->groupBox1->Controls->Add(this->textBox1);
			this->groupBox1->Controls->Add(this->radioButton2);
			this->groupBox1->Controls->Add(this->radioButton1);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->button1);
			this->groupBox1->Location = System::Drawing::Point(26, 141);
			this->groupBox1->Margin = System::Windows::Forms::Padding(2);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(2);
			this->groupBox1->Size = System::Drawing::Size(120, 351);
			this->groupBox1->TabIndex = 5;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Rbias";
			// 
			// groupBox10
			// 
			this->groupBox10->Controls->Add(this->radioButton11);
			this->groupBox10->Controls->Add(this->radioButton10);
			this->groupBox10->Controls->Add(this->radioButton9);
			this->groupBox10->Location = System::Drawing::Point(0, 228);
			this->groupBox10->Name = L"groupBox10";
			this->groupBox10->Size = System::Drawing::Size(120, 46);
			this->groupBox10->TabIndex = 13;
			this->groupBox10->TabStop = false;
			this->groupBox10->Text = L"Measure Bias";
			// 
			// radioButton11
			// 
			this->radioButton11->AutoSize = true;
			this->radioButton11->Location = System::Drawing::Point(87, 18);
			this->radioButton11->Name = L"radioButton11";
			this->radioButton11->Size = System::Drawing::Size(31, 16);
			this->radioButton11->TabIndex = 2;
			this->radioButton11->Text = L"R";
			this->radioButton11->UseVisualStyleBackColor = true;
			this->radioButton11->CheckedChanged += gcnew System::EventHandler(this, &StripQAControlGUI::radioButton11_CheckedChanged);
			// 
			// radioButton10
			// 
			this->radioButton10->AutoSize = true;
			this->radioButton10->Location = System::Drawing::Point(39, 18);
			this->radioButton10->Name = L"radioButton10";
			this->radioButton10->Size = System::Drawing::Size(47, 16);
			this->radioButton10->TabIndex = 1;
			this->radioButton10->Text = L"Cent";
			this->radioButton10->UseVisualStyleBackColor = true;
			this->radioButton10->CheckedChanged += gcnew System::EventHandler(this, &StripQAControlGUI::radioButton10_CheckedChanged);
			// 
			// radioButton9
			// 
			this->radioButton9->AutoSize = true;
			this->radioButton9->Checked = true;
			this->radioButton9->Location = System::Drawing::Point(0, 18);
			this->radioButton9->Name = L"radioButton9";
			this->radioButton9->Size = System::Drawing::Size(29, 16);
			this->radioButton9->TabIndex = 0;
			this->radioButton9->TabStop = true;
			this->radioButton9->Text = L"L";
			this->radioButton9->UseVisualStyleBackColor = true;
			this->radioButton9->CheckedChanged += gcnew System::EventHandler(this, &StripQAControlGUI::radioButton9_CheckedChanged);
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(62, 206);
			this->textBox5->Margin = System::Windows::Forms::Padding(2);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(50, 19);
			this->textBox5->TabIndex = 12;
			this->textBox5->Text = L"10";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(-2, 190);
			this->label9->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(95, 12);
			this->label9->TabIndex = 11;
			this->label9->Text = L"Current limit [uA]";
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(62, 161);
			this->textBox4->Margin = System::Windows::Forms::Padding(2);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(50, 19);
			this->textBox4->TabIndex = 10;
			this->textBox4->Text = L"3";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(4, 141);
			this->label8->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(70, 12);
			this->label8->TabIndex = 9;
			this->label8->Text = L"wait time [s]";
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(62, 119);
			this->textBox3->Margin = System::Windows::Forms::Padding(2);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(50, 19);
			this->textBox3->TabIndex = 8;
			this->textBox3->Text = L"1";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(4, 121);
			this->label7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(60, 12);
			this->label7->TabIndex = 7;
			this->label7->Text = L"Step V [V]";
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(62, 90);
			this->textBox2->Margin = System::Windows::Forms::Padding(2);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(50, 19);
			this->textBox2->TabIndex = 6;
			this->textBox2->Text = L"5";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(4, 91);
			this->label6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(56, 12);
			this->label6->TabIndex = 5;
			this->label6->Text = L"End V [V]";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(62, 62);
			this->textBox1->Margin = System::Windows::Forms::Padding(2);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(50, 19);
			this->textBox1->TabIndex = 4;
			this->textBox1->Text = L"0";
			// 
			// radioButton2
			// 
			this->radioButton2->AutoSize = true;
			this->radioButton2->Location = System::Drawing::Point(11, 35);
			this->radioButton2->Margin = System::Windows::Forms::Padding(2);
			this->radioButton2->Name = L"radioButton2";
			this->radioButton2->Size = System::Drawing::Size(103, 16);
			this->radioButton2->TabIndex = 3;
			this->radioButton2->Text = L"Test V Positive";
			this->radioButton2->UseVisualStyleBackColor = true;
			this->radioButton2->CheckedChanged += gcnew System::EventHandler(this, &StripQAControlGUI::radioButton2_CheckedChanged);
			// 
			// radioButton1
			// 
			this->radioButton1->AutoSize = true;
			this->radioButton1->Checked = true;
			this->radioButton1->Location = System::Drawing::Point(11, 17);
			this->radioButton1->Margin = System::Windows::Forms::Padding(2);
			this->radioButton1->Name = L"radioButton1";
			this->radioButton1->Size = System::Drawing::Size(107, 16);
			this->radioButton1->TabIndex = 2;
			this->radioButton1->TabStop = true;
			this->radioButton1->Text = L"Test V Negative";
			this->radioButton1->UseVisualStyleBackColor = true;
			this->radioButton1->CheckedChanged += gcnew System::EventHandler(this, &StripQAControlGUI::radioButton1_CheckedChanged);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(4, 63);
			this->label5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(62, 12);
			this->label5->TabIndex = 1;
			this->label5->Text = L"Start V [V]";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->textBox6);
			this->groupBox2->Controls->Add(this->radioButton3);
			this->groupBox2->Controls->Add(this->label10);
			this->groupBox2->Controls->Add(this->button4);
			this->groupBox2->Controls->Add(this->textBox7);
			this->groupBox2->Controls->Add(this->radioButton4);
			this->groupBox2->Controls->Add(this->label11);
			this->groupBox2->Controls->Add(this->label13);
			this->groupBox2->Controls->Add(this->textBox8);
			this->groupBox2->Controls->Add(this->textBox9);
			this->groupBox2->Controls->Add(this->label12);
			this->groupBox2->Location = System::Drawing::Point(160, 141);
			this->groupBox2->Margin = System::Windows::Forms::Padding(2);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(2);
			this->groupBox2->Size = System::Drawing::Size(120, 351);
			this->groupBox2->TabIndex = 6;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Ccpl";
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(67, 207);
			this->textBox6->Margin = System::Windows::Forms::Padding(2);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(50, 19);
			this->textBox6->TabIndex = 20;
			// 
			// radioButton3
			// 
			this->radioButton3->AutoSize = true;
			this->radioButton3->Location = System::Drawing::Point(11, 35);
			this->radioButton3->Margin = System::Windows::Forms::Padding(2);
			this->radioButton3->Name = L"radioButton3";
			this->radioButton3->Size = System::Drawing::Size(48, 16);
			this->radioButton3->TabIndex = 14;
			this->radioButton3->Text = L"Z-θ";
			this->radioButton3->UseVisualStyleBackColor = true;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(4, 187);
			this->label10->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(70, 12);
			this->label10->TabIndex = 19;
			this->label10->Text = L"wait time [s]";
			// 
			// textBox7
			// 
			this->textBox7->Location = System::Drawing::Point(67, 160);
			this->textBox7->Margin = System::Windows::Forms::Padding(2);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(50, 19);
			this->textBox7->TabIndex = 18;
			// 
			// radioButton4
			// 
			this->radioButton4->AutoSize = true;
			this->radioButton4->Checked = true;
			this->radioButton4->Location = System::Drawing::Point(11, 17);
			this->radioButton4->Margin = System::Windows::Forms::Padding(2);
			this->radioButton4->Name = L"radioButton4";
			this->radioButton4->Size = System::Drawing::Size(51, 16);
			this->radioButton4->TabIndex = 13;
			this->radioButton4->TabStop = true;
			this->radioButton4->Text = L"Cp-D";
			this->radioButton4->UseVisualStyleBackColor = true;
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(8, 143);
			this->label11->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(77, 12);
			this->label11->TabIndex = 17;
			this->label11->Text = L"Step freq [Hz]";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(9, 63);
			this->label13->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(79, 12);
			this->label13->TabIndex = 13;
			this->label13->Text = L"Start freq [Hz]";
			// 
			// textBox8
			// 
			this->textBox8->Location = System::Drawing::Point(67, 117);
			this->textBox8->Margin = System::Windows::Forms::Padding(2);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(50, 19);
			this->textBox8->TabIndex = 16;
			// 
			// textBox9
			// 
			this->textBox9->Location = System::Drawing::Point(67, 77);
			this->textBox9->Margin = System::Windows::Forms::Padding(2);
			this->textBox9->Name = L"textBox9";
			this->textBox9->Size = System::Drawing::Size(50, 19);
			this->textBox9->TabIndex = 14;
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(9, 101);
			this->label12->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(73, 12);
			this->label12->TabIndex = 15;
			this->label12->Text = L"End freq [Hz]";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->textBox10);
			this->groupBox3->Controls->Add(this->radioButton5);
			this->groupBox3->Controls->Add(this->label14);
			this->groupBox3->Controls->Add(this->button3);
			this->groupBox3->Controls->Add(this->textBox11);
			this->groupBox3->Controls->Add(this->radioButton6);
			this->groupBox3->Controls->Add(this->label15);
			this->groupBox3->Controls->Add(this->label16);
			this->groupBox3->Controls->Add(this->label17);
			this->groupBox3->Controls->Add(this->textBox12);
			this->groupBox3->Controls->Add(this->textBox13);
			this->groupBox3->Location = System::Drawing::Point(298, 141);
			this->groupBox3->Margin = System::Windows::Forms::Padding(2);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Padding = System::Windows::Forms::Padding(2);
			this->groupBox3->Size = System::Drawing::Size(120, 351);
			this->groupBox3->TabIndex = 7;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Cint";
			// 
			// textBox10
			// 
			this->textBox10->Location = System::Drawing::Point(61, 207);
			this->textBox10->Margin = System::Windows::Forms::Padding(2);
			this->textBox10->Name = L"textBox10";
			this->textBox10->Size = System::Drawing::Size(50, 19);
			this->textBox10->TabIndex = 28;
			// 
			// radioButton5
			// 
			this->radioButton5->AutoSize = true;
			this->radioButton5->Location = System::Drawing::Point(16, 35);
			this->radioButton5->Margin = System::Windows::Forms::Padding(2);
			this->radioButton5->Name = L"radioButton5";
			this->radioButton5->Size = System::Drawing::Size(48, 16);
			this->radioButton5->TabIndex = 22;
			this->radioButton5->Text = L"Z-θ";
			this->radioButton5->UseVisualStyleBackColor = true;
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(-2, 186);
			this->label14->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(70, 12);
			this->label14->TabIndex = 27;
			this->label14->Text = L"wait time [s]";
			// 
			// textBox11
			// 
			this->textBox11->Location = System::Drawing::Point(61, 159);
			this->textBox11->Margin = System::Windows::Forms::Padding(2);
			this->textBox11->Name = L"textBox11";
			this->textBox11->Size = System::Drawing::Size(50, 19);
			this->textBox11->TabIndex = 26;
			// 
			// radioButton6
			// 
			this->radioButton6->AutoSize = true;
			this->radioButton6->Checked = true;
			this->radioButton6->Location = System::Drawing::Point(16, 17);
			this->radioButton6->Margin = System::Windows::Forms::Padding(2);
			this->radioButton6->Name = L"radioButton6";
			this->radioButton6->Size = System::Drawing::Size(51, 16);
			this->radioButton6->TabIndex = 21;
			this->radioButton6->TabStop = true;
			this->radioButton6->Text = L"Cp-D";
			this->radioButton6->UseVisualStyleBackColor = true;
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(3, 141);
			this->label15->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(77, 12);
			this->label15->TabIndex = 25;
			this->label15->Text = L"Step freq [Hz]";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(4, 62);
			this->label16->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(79, 12);
			this->label16->TabIndex = 21;
			this->label16->Text = L"Start freq [Hz]";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(4, 99);
			this->label17->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(73, 12);
			this->label17->TabIndex = 23;
			this->label17->Text = L"End freq [Hz]";
			// 
			// textBox12
			// 
			this->textBox12->Location = System::Drawing::Point(61, 116);
			this->textBox12->Margin = System::Windows::Forms::Padding(2);
			this->textBox12->Name = L"textBox12";
			this->textBox12->Size = System::Drawing::Size(50, 19);
			this->textBox12->TabIndex = 24;
			// 
			// textBox13
			// 
			this->textBox13->Location = System::Drawing::Point(61, 76);
			this->textBox13->Margin = System::Windows::Forms::Padding(2);
			this->textBox13->Name = L"textBox13";
			this->textBox13->Size = System::Drawing::Size(50, 19);
			this->textBox13->TabIndex = 22;
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->textBox19);
			this->groupBox4->Controls->Add(this->label23);
			this->groupBox4->Controls->Add(this->textBox14);
			this->groupBox4->Controls->Add(this->button2);
			this->groupBox4->Controls->Add(this->label18);
			this->groupBox4->Controls->Add(this->textBox18);
			this->groupBox4->Controls->Add(this->textBox15);
			this->groupBox4->Controls->Add(this->label22);
			this->groupBox4->Controls->Add(this->label19);
			this->groupBox4->Controls->Add(this->label21);
			this->groupBox4->Controls->Add(this->textBox16);
			this->groupBox4->Controls->Add(this->textBox17);
			this->groupBox4->Controls->Add(this->label20);
			this->groupBox4->Location = System::Drawing::Point(440, 141);
			this->groupBox4->Margin = System::Windows::Forms::Padding(2);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Padding = System::Windows::Forms::Padding(2);
			this->groupBox4->Size = System::Drawing::Size(120, 351);
			this->groupBox4->TabIndex = 8;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"Rint";
			// 
			// textBox19
			// 
			this->textBox19->Location = System::Drawing::Point(62, 26);
			this->textBox19->Margin = System::Windows::Forms::Padding(2);
			this->textBox19->Name = L"textBox19";
			this->textBox19->Size = System::Drawing::Size(50, 19);
			this->textBox19->TabIndex = 24;
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(4, 27);
			this->label23->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(66, 12);
			this->label23->TabIndex = 23;
			this->label23->Text = L"Bias V [-V]";
			// 
			// textBox14
			// 
			this->textBox14->Location = System::Drawing::Point(62, 217);
			this->textBox14->Margin = System::Windows::Forms::Padding(2);
			this->textBox14->Name = L"textBox14";
			this->textBox14->Size = System::Drawing::Size(50, 19);
			this->textBox14->TabIndex = 22;
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(-2, 197);
			this->label18->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(95, 12);
			this->label18->TabIndex = 21;
			this->label18->Text = L"Current limit [uA]";
			// 
			// textBox18
			// 
			this->textBox18->Location = System::Drawing::Point(62, 57);
			this->textBox18->Margin = System::Windows::Forms::Padding(2);
			this->textBox18->Name = L"textBox18";
			this->textBox18->Size = System::Drawing::Size(50, 19);
			this->textBox18->TabIndex = 14;
			// 
			// textBox15
			// 
			this->textBox15->Location = System::Drawing::Point(62, 167);
			this->textBox15->Margin = System::Windows::Forms::Padding(2);
			this->textBox15->Name = L"textBox15";
			this->textBox15->Size = System::Drawing::Size(50, 19);
			this->textBox15->TabIndex = 20;
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(4, 59);
			this->label22->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(62, 12);
			this->label22->TabIndex = 13;
			this->label22->Text = L"Start V [V]";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(4, 147);
			this->label19->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(70, 12);
			this->label19->TabIndex = 19;
			this->label19->Text = L"wait time [s]";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(4, 87);
			this->label21->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(56, 12);
			this->label21->TabIndex = 15;
			this->label21->Text = L"End V [V]";
			// 
			// textBox16
			// 
			this->textBox16->Location = System::Drawing::Point(62, 115);
			this->textBox16->Margin = System::Windows::Forms::Padding(2);
			this->textBox16->Name = L"textBox16";
			this->textBox16->Size = System::Drawing::Size(50, 19);
			this->textBox16->TabIndex = 18;
			// 
			// textBox17
			// 
			this->textBox17->Location = System::Drawing::Point(62, 85);
			this->textBox17->Margin = System::Windows::Forms::Padding(2);
			this->textBox17->Name = L"textBox17";
			this->textBox17->Size = System::Drawing::Size(50, 19);
			this->textBox17->TabIndex = 16;
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(4, 116);
			this->label20->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(60, 12);
			this->label20->TabIndex = 17;
			this->label20->Text = L"Step V [V]";
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->textBox25);
			this->groupBox5->Controls->Add(this->textBox20);
			this->groupBox5->Controls->Add(this->label29);
			this->groupBox5->Controls->Add(this->radioButton7);
			this->groupBox5->Controls->Add(this->label24);
			this->groupBox5->Controls->Add(this->button5);
			this->groupBox5->Controls->Add(this->textBox21);
			this->groupBox5->Controls->Add(this->radioButton8);
			this->groupBox5->Controls->Add(this->textBox22);
			this->groupBox5->Controls->Add(this->label25);
			this->groupBox5->Controls->Add(this->label28);
			this->groupBox5->Controls->Add(this->label26);
			this->groupBox5->Controls->Add(this->textBox24);
			this->groupBox5->Controls->Add(this->label27);
			this->groupBox5->Controls->Add(this->textBox23);
			this->groupBox5->Location = System::Drawing::Point(580, 141);
			this->groupBox5->Margin = System::Windows::Forms::Padding(2);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Padding = System::Windows::Forms::Padding(2);
			this->groupBox5->Size = System::Drawing::Size(131, 351);
			this->groupBox5->TabIndex = 9;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"PTP";
			// 
			// textBox25
			// 
			this->textBox25->Location = System::Drawing::Point(67, 65);
			this->textBox25->Margin = System::Windows::Forms::Padding(2);
			this->textBox25->Name = L"textBox25";
			this->textBox25->Size = System::Drawing::Size(50, 19);
			this->textBox25->TabIndex = 26;
			// 
			// textBox20
			// 
			this->textBox20->Location = System::Drawing::Point(67, 255);
			this->textBox20->Margin = System::Windows::Forms::Padding(2);
			this->textBox20->Name = L"textBox20";
			this->textBox20->Size = System::Drawing::Size(50, 19);
			this->textBox20->TabIndex = 34;
			// 
			// label29
			// 
			this->label29->AutoSize = true;
			this->label29->Location = System::Drawing::Point(9, 67);
			this->label29->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label29->Name = L"label29";
			this->label29->Size = System::Drawing::Size(66, 12);
			this->label29->TabIndex = 25;
			this->label29->Text = L"Bias V [-V]";
			// 
			// radioButton7
			// 
			this->radioButton7->AutoSize = true;
			this->radioButton7->Location = System::Drawing::Point(11, 35);
			this->radioButton7->Margin = System::Windows::Forms::Padding(2);
			this->radioButton7->Name = L"radioButton7";
			this->radioButton7->Size = System::Drawing::Size(103, 16);
			this->radioButton7->TabIndex = 18;
			this->radioButton7->Text = L"Test V Positive";
			this->radioButton7->UseVisualStyleBackColor = true;
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Location = System::Drawing::Point(4, 235);
			this->label24->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(95, 12);
			this->label24->TabIndex = 33;
			this->label24->Text = L"Current limit [uA]";
			// 
			// textBox21
			// 
			this->textBox21->Location = System::Drawing::Point(67, 96);
			this->textBox21->Margin = System::Windows::Forms::Padding(2);
			this->textBox21->Name = L"textBox21";
			this->textBox21->Size = System::Drawing::Size(50, 19);
			this->textBox21->TabIndex = 26;
			// 
			// radioButton8
			// 
			this->radioButton8->AutoSize = true;
			this->radioButton8->Checked = true;
			this->radioButton8->Location = System::Drawing::Point(11, 17);
			this->radioButton8->Margin = System::Windows::Forms::Padding(2);
			this->radioButton8->Name = L"radioButton8";
			this->radioButton8->Size = System::Drawing::Size(107, 16);
			this->radioButton8->TabIndex = 17;
			this->radioButton8->TabStop = true;
			this->radioButton8->Text = L"Test V Negative";
			this->radioButton8->UseVisualStyleBackColor = true;
			// 
			// textBox22
			// 
			this->textBox22->Location = System::Drawing::Point(67, 206);
			this->textBox22->Margin = System::Windows::Forms::Padding(2);
			this->textBox22->Name = L"textBox22";
			this->textBox22->Size = System::Drawing::Size(50, 19);
			this->textBox22->TabIndex = 32;
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(9, 97);
			this->label25->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(62, 12);
			this->label25->TabIndex = 25;
			this->label25->Text = L"Start V [V]";
			// 
			// label28
			// 
			this->label28->AutoSize = true;
			this->label28->Location = System::Drawing::Point(9, 155);
			this->label28->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label28->Name = L"label28";
			this->label28->Size = System::Drawing::Size(60, 12);
			this->label28->TabIndex = 29;
			this->label28->Text = L"Step V [V]";
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Location = System::Drawing::Point(9, 186);
			this->label26->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(70, 12);
			this->label26->TabIndex = 31;
			this->label26->Text = L"wait time [s]";
			// 
			// textBox24
			// 
			this->textBox24->Location = System::Drawing::Point(67, 124);
			this->textBox24->Margin = System::Windows::Forms::Padding(2);
			this->textBox24->Name = L"textBox24";
			this->textBox24->Size = System::Drawing::Size(50, 19);
			this->textBox24->TabIndex = 28;
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(9, 125);
			this->label27->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(56, 12);
			this->label27->TabIndex = 27;
			this->label27->Text = L"End V [V]";
			// 
			// textBox23
			// 
			this->textBox23->Location = System::Drawing::Point(67, 153);
			this->textBox23->Margin = System::Windows::Forms::Padding(2);
			this->textBox23->Name = L"textBox23";
			this->textBox23->Size = System::Drawing::Size(50, 19);
			this->textBox23->TabIndex = 30;
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->label4);
			this->groupBox6->Controls->Add(this->comboBox6);
			this->groupBox6->Controls->Add(this->label3);
			this->groupBox6->Controls->Add(this->comboBox5);
			this->groupBox6->Controls->Add(this->comboBox4);
			this->groupBox6->Controls->Add(this->comboBox3);
			this->groupBox6->Controls->Add(this->label2);
			this->groupBox6->Controls->Add(this->label1);
			this->groupBox6->Location = System::Drawing::Point(26, 14);
			this->groupBox6->Margin = System::Windows::Forms::Padding(2);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Padding = System::Windows::Forms::Padding(2);
			this->groupBox6->Size = System::Drawing::Size(430, 109);
			this->groupBox6->TabIndex = 10;
			this->groupBox6->TabStop = false;
			this->groupBox6->Text = L"Select Devices";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(233, 68);
			this->label4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(26, 12);
			this->label4->TabIndex = 8;
			this->label4->Text = L"Cint";
			// 
			// comboBox6
			// 
			this->comboBox6->FormattingEnabled = true;
			this->comboBox6->Location = System::Drawing::Point(269, 66);
			this->comboBox6->Margin = System::Windows::Forms::Padding(2);
			this->comboBox6->Name = L"comboBox6";
			this->comboBox6->Size = System::Drawing::Size(134, 20);
			this->comboBox6->TabIndex = 7;
			this->comboBox6->SelectedIndexChanged += gcnew System::EventHandler(this, &StripQAControlGUI::comboBox6_SelectedIndexChanged);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(233, 27);
			this->label3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(28, 12);
			this->label3->TabIndex = 6;
			this->label3->Text = L"Ccpl";
			// 
			// comboBox5
			// 
			this->comboBox5->FormattingEnabled = true;
			this->comboBox5->Location = System::Drawing::Point(269, 25);
			this->comboBox5->Margin = System::Windows::Forms::Padding(2);
			this->comboBox5->Name = L"comboBox5";
			this->comboBox5->Size = System::Drawing::Size(134, 20);
			this->comboBox5->TabIndex = 5;
			this->comboBox5->SelectedIndexChanged += gcnew System::EventHandler(this, &StripQAControlGUI::comboBox5_SelectedIndexChanged);
			// 
			// comboBox4
			// 
			this->comboBox4->FormattingEnabled = true;
			this->comboBox4->Location = System::Drawing::Point(39, 66);
			this->comboBox4->Margin = System::Windows::Forms::Padding(2);
			this->comboBox4->Name = L"comboBox4";
			this->comboBox4->Size = System::Drawing::Size(134, 20);
			this->comboBox4->TabIndex = 4;
			this->comboBox4->SelectedIndexChanged += gcnew System::EventHandler(this, &StripQAControlGUI::comboBox4_SelectedIndexChanged);
			// 
			// comboBox3
			// 
			this->comboBox3->FormattingEnabled = true;
			this->comboBox3->Location = System::Drawing::Point(39, 25);
			this->comboBox3->Margin = System::Windows::Forms::Padding(2);
			this->comboBox3->Name = L"comboBox3";
			this->comboBox3->Size = System::Drawing::Size(134, 20);
			this->comboBox3->TabIndex = 3;
			this->comboBox3->SelectedIndexChanged += gcnew System::EventHandler(this, &StripQAControlGUI::comboBox3_SelectedIndexChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(4, 68);
			this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(21, 12);
			this->label2->TabIndex = 2;
			this->label2->Text = L"HV";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(4, 27);
			this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(36, 12);
			this->label1->TabIndex = 1;
			this->label1->Text = L"TestV";
			// 
			// groupBox7
			// 
			this->groupBox7->Controls->Add(this->button7);
			this->groupBox7->Controls->Add(this->button6);
			this->groupBox7->Location = System::Drawing::Point(468, 14);
			this->groupBox7->Margin = System::Windows::Forms::Padding(2);
			this->groupBox7->Name = L"groupBox7";
			this->groupBox7->Padding = System::Windows::Forms::Padding(2);
			this->groupBox7->Size = System::Drawing::Size(103, 109);
			this->groupBox7->TabIndex = 11;
			this->groupBox7->TabStop = false;
			this->groupBox7->Text = L"LCR Calibration";
			this->groupBox7->Enter += gcnew System::EventHandler(this, &StripQAControlGUI::groupBox7_Enter);
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(15, 66);
			this->button7->Margin = System::Windows::Forms::Padding(2);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(53, 37);
			this->button7->TabIndex = 6;
			this->button7->Text = L"Short";
			this->button7->UseVisualStyleBackColor = true;
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(15, 16);
			this->button6->Margin = System::Windows::Forms::Padding(2);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(53, 37);
			this->button6->TabIndex = 5;
			this->button6->Text = L"Open";
			this->button6->UseVisualStyleBackColor = true;
			// 
			// groupBox8
			// 
			this->groupBox8->Location = System::Drawing::Point(575, 20);
			this->groupBox8->Margin = System::Windows::Forms::Padding(2);
			this->groupBox8->Name = L"groupBox8";
			this->groupBox8->Padding = System::Windows::Forms::Padding(2);
			this->groupBox8->Size = System::Drawing::Size(299, 103);
			this->groupBox8->TabIndex = 12;
			this->groupBox8->TabStop = false;
			this->groupBox8->Text = L"Save File";
			// 
			// groupBox9
			// 
			this->groupBox9->Location = System::Drawing::Point(721, 141);
			this->groupBox9->Margin = System::Windows::Forms::Padding(2);
			this->groupBox9->Name = L"groupBox9";
			this->groupBox9->Padding = System::Windows::Forms::Padding(2);
			this->groupBox9->Size = System::Drawing::Size(154, 93);
			this->groupBox9->TabIndex = 13;
			this->groupBox9->TabStop = false;
			this->groupBox9->Text = L"Status";
			// 
			// button8
			// 
			this->button8->Location = System::Drawing::Point(721, 248);
			this->button8->Margin = System::Windows::Forms::Padding(2);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(154, 59);
			this->button8->TabIndex = 35;
			this->button8->Text = L"All measure START";
			this->button8->UseVisualStyleBackColor = true;
			// 
			// button9
			// 
			this->button9->Location = System::Drawing::Point(721, 319);
			this->button9->Margin = System::Windows::Forms::Padding(2);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(154, 59);
			this->button9->TabIndex = 36;
			this->button9->Text = L"STOP";
			this->button9->UseVisualStyleBackColor = true;
			this->button9->Click += gcnew System::EventHandler(this, &StripQAControlGUI::button9_Click);
			// 
			// StripQAControlGUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 12);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1260, 499);
			this->Controls->Add(this->button9);
			this->Controls->Add(this->button8);
			this->Controls->Add(this->groupBox9);
			this->Controls->Add(this->groupBox8);
			this->Controls->Add(this->groupBox7);
			this->Controls->Add(this->groupBox6);
			this->Controls->Add(this->groupBox5);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Margin = System::Windows::Forms::Padding(2);
			this->Name = L"StripQAControlGUI";
			this->Text = L"StripQAControlGUI";
			this->Load += gcnew System::EventHandler(this, &StripQAControlGUI::StripQAControlGUI_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox10->ResumeLayout(false);
			this->groupBox10->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			this->groupBox7->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion
		bool RB = false;
		bool CB = false;
		bool LB = false;
	private: System::Void StripQAControlGUI_Load(System::Object^ sender, System::EventArgs^ e) {
		bool setpsHV = false;
		bool setpsVtest = false;
		bool setlcr1 = false;
		bool setlcr2 = false;
		std::cout << sts->getPowerSupplies()->size() << "  power supply added..." << std::endl;
		cli::array< System::Object^  >^ ary1 = gcnew cli::array<System::Object^>(sts->getPowerSupplies()->size());
		int ii = 0;
		for (auto xx : *(sts->getPowerSupplies())) {
			std::stringstream ss; ss.str("");
			ss << xx.second << ":" << xx.first->get_address();
			ary1[ii] = gcnew System::String(ss.str().c_str());
			System::Console::WriteLine(ary1[ii++]);
		}
		this->comboBox3->Items->AddRange(ary1);
		if (sts->getPowerSupplies()->size() != 0) {
			this->comboBox3->Text = (String^)ary1[0];
			setpsVtest = true;
		}
		this->comboBox4->Items->AddRange(ary1);
		if (sts->getPowerSupplies()->size() > 1) {
			this->comboBox4->Text = (String^)ary1[1];
			setpsHV = true;
		}
		std::cout << sts->getLCRmeters()->size() << "  LCR meter added..." << std::endl;
		cli::array< System::Object^  >^ ary2 = gcnew cli::array<System::Object^>(sts->getLCRmeters()->size());
		ii = 0;
		for (auto xx : *(sts->getLCRmeters())) {
			std::stringstream ss; ss.str("");
			ss << xx.second << ":" << xx.first->get_address();
			ary2[ii] = gcnew System::String(ss.str().c_str());
			System::Console::WriteLine(ary2[ii++]);
		}
		this->comboBox5->Items->AddRange(ary2);
		if (sts->getLCRmeters()->size() != 0) {
			this->comboBox5->Text = (String^)ary2[0];
			setlcr1 = true;
		}
		this->comboBox6->Items->AddRange(ary2);
		if (sts->getLCRmeters()->size() > 1) {
			this->comboBox6->Text = (String^)ary2[1];
			setlcr2 = true;
		}
		/*
		for (auto xx : *(sts->getLCRmeters())) {
			std::cout << xx.first << " " << xx.second->get_address() << std::endl;
		*/
		if (sts->getPowerSupplies()->size() < 2 && sts->getLCRmeters()->size() < 2) {
			String^ mess_str = L"2 device needed.  Please select 2 devices for PS and LCR in Control Panel.";
			System::Windows::Forms::MessageBox::Show(mess_str);
			return;
		}


		String^ psHVdev;
		String^ psVtestdev;
		String^ lcr1dev;
		String^ lcr2dev;

		if (setpsVtest)	psVtestdev = (String^)ary1[0];
		else psVtestdev = L"";
		if (setpsHV) psHVdev = (String^)ary1[1];
		else psHVdev = L"";
		if (setlcr1) lcr1dev = (String^)ary2[0];
		else lcr1dev = L"";
		if (setlcr2) lcr2dev = (String^)ary2[1];
		else lcr1dev = L"";
		std::cout << msclr::interop::marshal_as<std::string>(psVtestdev) << " " << std::endl;
		std::cout << msclr::interop::marshal_as<std::string>(psHVdev) << " " << std::endl;
		std::cout << msclr::interop::marshal_as<std::string>(lcr1dev) << " " << std::endl;
		std::cout << msclr::interop::marshal_as<std::string>(lcr2dev) << std::endl;
		sts->setCurrentActiveDeviceQA(msclr::interop::marshal_as<std::string>(psVtestdev), msclr::interop::marshal_as<std::string>(psHVdev),
									msclr::interop::marshal_as<std::string>(lcr1dev), msclr::interop::marshal_as<std::string>(lcr2dev));
		
	}
		   void RunRbias() {
			   TUSBPIOCtrl usbpio(0);
			   usbpio.CloseDevice();
			   usbpio.OpenDevice();
			   if (LB == true){
			   Tusbpio_Dev1_Write(0, 2, 0x4);
			   Tusbpio_Dev1_Write(0, 1, 0x40);
			   }
			   if (CB == true) {
				   Tusbpio_Dev1_Write(0, 2, 0x4);
				   Tusbpio_Dev1_Write(0, 1, 0x80);

			   }
			   if (CB == true) {
				   Tusbpio_Dev1_Write(0, 2, 0x4);
				   Tusbpio_Dev1_Write(0, 1, 0x10);

			   }
			   usbpio.SetSWon(0);
			   std::cout << "====  Rbias  ==== " << std::endl;
			  // sts->isneg = 0;//false
			   sts->curlimit = fabs(float::Parse(this->textBox5->Text));//uA
			   sts->volstart = fabs(float::Parse(this->textBox1->Text));//V
			   sts->volend = fabs(float::Parse(this->textBox2->Text));//V
			   sts->volstep = fabs(float::Parse(this->textBox3->Text));//V
			   sts->interval = fabs(float::Parse(this->textBox4->Text));//s
			   sts->rampingt = 200;//us
			   sts->frequency = 100;//kHz(don't use)
			   sts->freqstart = 0.1;//kHz(don't use)
			   sts->freqend = 1000;//kHz(don't use)
			   sts->fixedvolt = 0;//V(don't use)
			   std::cout << "All parameter set!!" << std::endl;
			   //	sts.dh = new DataHandler;
			   sts->getDataHandler()->SetDataDirPath("C:\\work\\Silicon\\SiliconTestSetupApp\\Data\\StripQA\\");
			   sts->getDataHandler()->SetFilename("test");
			   sts->runIVCVScan(true, false);
			   Sleep(10000);
			   usbpio.SetSWoff();
	}
	void RunCcp(){
		TUSBPIOCtrl usbpio(0);
		usbpio.CloseDevice();
		usbpio.OpenDevice();
		Tusbpio_Dev1_Write(0, 2, 0x4);
		Tusbpio_Dev1_Write(0, 1, 0x40);
		usbpio.SetSWon(0);
		std::cout << "====  Ccpl  ==== " << std::endl;
		// sts->isneg = 0;//false
		sts->curlimit = fabs(float::Parse(this->textBox5->Text));//uA,don't use
		sts->volstart = fabs(float::Parse(this->textBox1->Text));//V, don't use
		sts->volend = fabs(float::Parse(this->textBox2->Text));//V don't use
		sts->volstep = fabs(float::Parse(this->textBox3->Text));//V  don't use
		sts->interval = fabs(float::Parse(this->textBox6->Text));//s  
		sts->rampingt = 200;//us don't use
		sts->frequency = 100;//kHz(don't use)
		sts->freqstart = fabs(float::Parse(this->textBox9->Text));//kHz(don't use)
		sts->freqend = fabs(float::Parse(this->textBox8->Text));//kHz(don't use)
		sts->fixedvolt = 0;//V(don't use)
		std::cout << "All parameter set!!" << std::endl;
		//	sts.dh = new DataHandler;
		sts->getDataHandler()->SetDataDirPath("C:\\work\\Silicon\\SiliconTestSetupApp\\Data\\StripQA\\");
		sts->getDataHandler()->SetFilename("test");
		sts->runIVCVScan(true, false);
		Sleep(10000);
		usbpio.SetSWoff();

	}
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
	//Rbias	
	//	TUSBPIOCtrl(0);
		

		ThreadStart^ threadDelegate = gcnew ThreadStart(this,&StripQAControlGUI::RunRbias);
		tscan = gcnew Thread(threadDelegate);
		tscan->IsBackground = true;
		tscan->Start();
		/*
		sts->runIVCVScan(true, false);
		Sleep(10000);
		usbpio.SetSWoff();
		

		*/


	}
	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) {
	//	TUSBPIOCtrl(0);
		TUSBPIOCtrl usbpio(0);
		usbpio.CloseDevice();
		usbpio.OpenDevice();
	//	Tusbpio_Dev1_Write(0, 0, 0x10);
		usbpio.SetBit(0, 0x10);
		usbpio.SetSWon(0);
		Sleep(10000);
		usbpio.SetSWoff();

	}
private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {
	//TUSBPIOCtrl(0);
	//Rint
	TUSBPIOCtrl usbpio(0);
	usbpio.CloseDevice();
	usbpio.OpenDevice();
	//Tusbpio_Dev1_Write(0,0,0x20);
	usbpio.SetBit(0, 0x20);
	Tusbpio_Dev1_Write(0,1,0x4);
	Tusbpio_Dev1_Write(0,2,0x1);
	usbpio.SetSWon(0);
	Sleep(10000);
	usbpio.SetSWoff();

}
private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e) {
//	TUSBPIOCtrl(0);
	//PTP
	TUSBPIOCtrl usbpio(0);
	usbpio.CloseDevice();
	usbpio.OpenDevice();
	Tusbpio_Dev1_Write(0,2, 0x2);
	Tusbpio_Dev1_Write(0,1, 0xB);
	usbpio.SetSWon(0);
	Sleep(10000);
	usbpio.SetSWoff();

}


private: System::Void groupBox7_Enter(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void comboBox3_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	sts->setCurrentActiveDeviceQA("",msclr::interop::marshal_as<std::string>((String^)comboBox3->Text), "", "");
}
private: System::Void comboBox4_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	sts->setCurrentActiveDeviceQA(msclr::interop::marshal_as<std::string>((String^)comboBox4->Text), "", "", "");
}
private: System::Void comboBox5_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	sts->setCurrentActiveDeviceQA("", "", msclr::interop::marshal_as<std::string>((String^)comboBox5->Text), "");
}
private: System::Void comboBox6_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	sts->setCurrentActiveDeviceQA("", "", "", msclr::interop::marshal_as<std::string>((String^)comboBox6->Text));
}
private: System::Void button9_Click(System::Object^ sender, System::EventArgs^ e) {
	tscan->Abort();
	delete tscan;
	sts->stopRun();
}
private: System::Void radioButton1_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	sts->isneg = 1;//negative
}
private: System::Void radioButton2_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	sts->isneg = 0;//positive
}
private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e) {
	ThreadStart^ threadDelegate = gcnew ThreadStart(this, &StripQAControlGUI::RunCcp);
	tscan = gcnew Thread(threadDelegate);
	tscan->IsBackground = true;
	tscan->Start();
}
private: System::Void radioButton9_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	LB = true;
}

private: System::Void radioButton10_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	CB = true;
}

private: System::Void radioButton11_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	RB = true;
}
};
}
