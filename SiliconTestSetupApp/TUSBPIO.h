//-----------------------------------------------
//TUSB-PIO用ドライバアクセス用API
//Visual C++/CLI 用モジュール
//
//2014_02_04　株式会社タートル工業
//Copyright (C) 2013 Turtle Industry Co.,Ltd.
//-----------------------------------------------

#ifndef TUSBPIO_h
#define TUSBPIO_h

using namespace System;
using namespace System::Runtime::InteropServices;

//API関数宣言
[DllImport("TUSBPIO.DLL", CallingConvention = CallingConvention::Cdecl)]
extern short Tusbpio_Device_Open( short id);
[DllImport("TUSBPIO.DLL", CallingConvention = CallingConvention::Cdecl)]
extern void Tusbpio_Device_Close( short id);
[DllImport("TUSBPIO.DLL", CallingConvention = CallingConvention::Cdecl)]
extern short Tusbpio_Dev1_Write( short id, unsigned char addr, unsigned char dat );
[DllImport("TUSBPIO.DLL", CallingConvention = CallingConvention::Cdecl)]
extern short Tusbpio_Dev2_Write( short id, unsigned char addr, unsigned char dat );
[DllImport("TUSBPIO.DLL", CallingConvention = CallingConvention::Cdecl)]
extern short Tusbpio_Dev1_Read( short id, unsigned char addr, unsigned char *dat );
[DllImport("TUSBPIO.DLL", CallingConvention = CallingConvention::Cdecl)]
extern short Tusbpio_Dev2_Read( short id, unsigned char addr, unsigned char *dat );

// エラーメッセージ出力(オープン関数)
static String^ Tusbpio_GetErrMessage1(short retcode)
{
    switch (retcode)
    {
        case 0:
            return "正常終了しました";
        case 1:
            return "ID番号が不正です";
        case 3:
            return "すでにデバイスはオープンされています";
        case 4:
            return "接続されている台数が多すぎます";
        case 5:
            return "オープンできませんでした";
        case 6:
            return "デバイスがみつかりません";
        default:
            return "不明なエラーです";
    }
}

// エラーメッセージ出力(その他の関数)
static String^ Tusbpio_GetErrMessage2(short retcode)
{
    switch (retcode)
    {
        case 0:
            return "正常終了しました";
        case 1:
            return "オープンされていません";
        case 2:
            return "失敗しました";
        default:
            return "不明なエラーです";
    }
}

#endif
