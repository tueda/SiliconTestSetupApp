#include "pch.h"
//#include "stdafx.h"
#include "IVCVCtrl.h"
//#include "unistd.h"
#include <conio.h>

//class IVCVCtrl;

IVCVCtrl::IVCVCtrl() {

}
int IVCVCtrl::Initialize() {



	return 0;
}
int IVCVCtrl::IVCVCtrl_test() {
    serial_interface* si = new prologix_gpibusb();
    std::string device_name = "COM5";
    si->set_device_name(device_name);
    std::cout << si->get_device_name() << std::endl;
    si->initialize();
    std::cout << "initialized ... " << std::endl;
    if (!si->is_initialized()) {
        std::cout << "[ERROR] failed to create an instance of the serial interface for " << device_name << "." << std::endl;
        return -1;
    }
    else {
        std::cout << "successfully opened.." << std::endl;
    }
    
    int keithley2410_1_address = 24;
    double keithley2410_1_compliance = 10.E-6;
    //double keithley2410_1_output_voltage = 80.;
    power_supply* keithley2410_1 = new keithley2410(keithley2410_1_address);
    keithley2410_1->set_voltage(0.0); //initial voltage
    keithley2410_1->set_compliance(keithley2410_1_compliance);

    keithley2410_1->power_on(si);
    keithley2410_1->power_off(si);
    

    int keithley6517A_1_address = 29;
    double keithley6517A_1_compliance = 10.E-6;
    power_supply* keithley6517A_1 = new keithley6517A(keithley6517A_1_address);
    keithley6517A_1->set_voltage(0.0); //initial voltage
    keithley6517A_1->set_compliance(keithley6517A_1_compliance);
//    std::cout << "c1" << std::endl;
    keithley6517A_1->power_on(si);
//   std::cout << "c2" << std::endl;
    keithley6517A_1->power_off(si);
//    std::cout << "c3" << std::endl;

    delete keithley6517A_1;
    delete keithley2410_1;
    return 0;
}


std::string IVCVCtrl::get_time_str()
{
    std::string str_time;
    time_t now_t = time(NULL);
    struct tm* read_time = localtime(&now_t);
    char char_time[256];
    size_t time_result = strftime(char_time, sizeof(char_time), "%c %z", read_time);
    str_time = char_time;

    return str_time;
}
std::string IVCVCtrl::get_out_format(double voltage, double current, double compliance)
{
    std::string str_out;
    char out_format[256];
    sprintf(out_format, "%g\t%g\t%g\t%s", voltage, current, compliance, get_time_str().c_str());
    //sprintf(out_format, "%s : Voltage = %g [V], Current = %g [A], Current compliabce = %g [A]", get_time_str().c_str(), voltage, current, compliance);
    str_out = out_format;

    return str_out;
}
std::string IVCVCtrl::get_out_format_IV(double voltage, double current)
{
    std::string str_out_IV;
    char out_format_IV[256];
    sprintf(out_format_IV, "%g\t%g\t\t%s", voltage, current, get_time_str().c_str());
    str_out_IV = out_format_IV;

    return str_out_IV;
}
std::string IVCVCtrl::get_out_format_IVCV(double frequency, double voltage, double current, double capacitance, double degree)
{
    std::string str_out_IVCV;
    char out_format_IVCV[256];
    sprintf(out_format_IVCV, "%g\t%g\t%g\t%g\t%g\t%s", frequency, voltage, current, capacitance, degree, get_time_str().c_str());
    str_out_IVCV = out_format_IVCV;

    return str_out_IVCV;
}
std::string IVCVCtrl::get_out_format_freq(double frequency, double capacitance, double degree)
{
    std::string str_out_freq;
    char out_format_freq[256];
    sprintf(out_format_freq, "%g\t%g\t%g\t%s", frequency, capacitance, degree, get_time_str().c_str());
    str_out_freq = out_format_freq;

    return str_out_freq;
}
void IVCVCtrl::clearStdin()
{
    //make sure to discard stdin buffer contents before using next scanf() and fgets()
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}
/*  
int IVCVCtrl::kbhit(void)
{
    struct termios oldt, newt;
    int ch;
    int oldf;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = ioctlsocket(STDIN_FILENO, F_GETFL, 0);
    ioctlsocket(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    ch = getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    ioctlsocket(STDIN_FILENO, F_SETFL, oldf);

    if (ch != EOF) {
        ungetc(ch, stdin);
        return 1;
    }
    return 0;
}
*/

std::string IVCVCtrl::substrback(std::string str, size_t pos, size_t len)
{
    const size_t strLen = str.length();
    return str.substr(strLen - pos, len);
}
int IVCVCtrl::rename(std::string kind_of_exp, std::string filename_in)
{
    int k = 0;
    const char* filenamec = "data/nametmp.txt";
    std::string filenames = std::string(filenamec);
    std::ifstream ifs(filenames);
    if (! ifs) {
        std::cout << "Not Exist nametmp.txt file" << std::endl;
        return 0;
    }
    std::string tmpdatafilename;
    getline(ifs, tmpdatafilename);

    try {
        //    k = std::stoi(tmpdatafilename.substr(5,1));.
        k = std::stoi(substrback(tmpdatafilename, 3, 3));
    }
    catch (std::invalid_argument e) {
        std::cout << e.what() << std::endl;
    }
    k = k + 1;
    std::ofstream ofs(filenames);
    ofs << kind_of_exp << "_" << filename_in << "_" << std::setfill('0') << std::setw(3) << std::right << std::to_string(k) << std::endl;  //CV_test_012.dat
    return 0;
}
std::string IVCVCtrl::read_data_file_name()
{
    const char* filenamec = "data/nametmp.txt";
    std::string filenames = std::string(filenamec);
    std::ifstream ifs(filenames);
    std::string datafilename;
    getline(ifs, datafilename);
    std::cout << datafilename << std::endl;
    return datafilename;
}
void IVCVCtrl::ReadV(power_supply* ps, serial_interface* si)
{
    if (!ps->is_on(si)) {
        printf("Voltage and current reading can be performed only when the output is enabled. Doing nothing...\n\n");
        return;
    }
    else {
        std::cout << "power supply is on" << std::endl;
    }

    clearStdin();
    int trial = 0;
    printf("==============================================\n");
    printf("Hit any key to return to the prompt.\n");
    printf("==============================================\n");
    while (1) {
        //Reading voltage and current
        double voltage(0.);
        voltage = ps->read_voltage(si);
        //Preparing output file
        std::cout << voltage << std::endl;
        if (kbhit()) break;
        trial++;
        Sleep(1);
    }
    clearStdin();

    return;
}
void IVCVCtrl::ReadI(power_supply* ps, serial_interface* si)
{
    if (!ps->is_on(si)) {
        printf("Voltage and current reading can be performed only when the output is enabled. Doing nothing...\n\n");
        return;
    }
    else {
        std::cout << "power supply is on" << std::endl;
    }

    clearStdin();
    int trial = 0;
    printf("==============================================\n");
    printf("Hit any key to return to the prompt.\n");
    printf("==============================================\n");
    while (1) {
        //Reading voltage and current
        double current(0.);
        current = ps->read_current(si);
        //Preparing output file
        std::cout << current << std::endl;

        if (kbhit()) break;
        trial++;
        Sleep(1);
    }
    clearStdin();

    return;
}
void IVCVCtrl::ReadIV(power_supply* ps, serial_interface* si)
{
    if (!ps->is_on(si)) {
        printf("Voltage and current reading can be performed only when the output is enabled. Doing nothing...\n\n");
        return;
    }

    clearStdin();
    int trial = 0;
    printf("==============================================\n");
    printf("Hit any key to return to the prompt.\n");
    printf("==============================================\n");
    while (1) {
        //Reading voltage and current
        double voltage(0.), current(0.);
        ps->read_voltage_and_current(si, voltage, current);

  //      std::cout << " ==> " << voltage << " " << current << std::endl;
        //Preparing output file
        std::ofstream ofile_keithley_2410_1(ps->get_ofile_name(), std::ofstream::app);
        std::string out_format = get_out_format(voltage, current, ps->get_compliance());
        ofile_keithley_2410_1 << out_format << std::endl;
        std::cout << out_format << std::endl;

        if (kbhit()) break;
        trial++;
        Sleep(1);
    }
    clearStdin();

    return;
}
int IVCVCtrl::IVstepmeasure(power_supply* ps, serial_interface* si)
{
    int mNo = 99;
    double HVin = 0;
    double HVfi = -1000;
    double HVst = -10;
    double Ilim = 1e-3;
    double Tint = 3;
    double Ramp = 1;
    double Trmp = 200;
    int ram = 2;
    int flag = 1;
    std::string savepath = "C:=\\work\\Silicon\\DataOutput\\";
    std::string savename = "hoge";
    double voltage(0.), current(0.);
    double HV;

    //  std::string kind_of_exp = "IV";
    //  std::string filename_in = "test";
    //  std::string devicename = ps->get_device_type();
    //  rename(kind_of_exp, filename_in);
    //  std::string datafilename = read_data_file_name();

    while (1) {
        while (flag) {
            std::cout << " ====================================================" << std::endl;
            std::cout << " this data file : " << savepath << savename << ".dat" << std::endl;
            std::cout << " Please specify what you want to do... " << std::endl;
            std::cout << "   0: start measurement (Auto Output ON)" << std::endl;
            std::cout << "   1: set initial voltage : " << HVin << "[V]" << std::endl;
            std::cout << "   2: set final voltage   : " << HVfi << "[V]" << std::endl;
            std::cout << "   3: set step voltage    : " << HVst << "[V]" << std::endl;
            std::cout << "   4: set interval time   : " << Tint << "[s]" << std::endl;
            std::cout << "   5: set # of ramping steps : " << Ramp << "steps" << std::endl;
            std::cout << "   6: set ramping time       : " << Trmp << "[ms]" << std::endl;
            std::cout << "   7: set current limit   : " << Ilim << "[A]" << std::endl;
            std::cout << "  10: set output file directory : " << savepath << std::endl;
            std::cout << "  11: set output file name      : " << savename << "(.dat)" << std::endl;
            std::cout << "  20: Initializing Keithley2410 " << std::endl;
            std::cout << "  21: Reset Keithley2410 " << std::endl;
            std::cout << "  22: Output ON" << std::endl;
            std::cout << "  23: Output OFF" << std::endl;
            std::cout << "  99: exit " << std::endl;
            std::cout << " ====================================================" << std::endl;

            while (1) {
                mNo = 999;
                std::cout << "" << std::endl;
                std::cout << " No = ";
                scanf("%d", &mNo);
                if (mNo == 999) clearStdin();
                std::cout << "" << std::endl;
                if (mNo >= 0) break;
            }
            switch (mNo) {
            case 1:
                std::cout << " Set initial voltage [V]" << std::endl;
                std::cin >> HVin;
                if (abs(HVin) > 1100) {
                    std::cout << " Hit proper number (from -1100 to +1100)" << std::endl;
                    HVin = 0.;
                }
                clearStdin();
                break;
            case 2:
                std::cout << " Set final voltage [V]" << std::endl;
                std::cin >> HVfi;
                if (abs(HVfi) > 1100) {
                    std::cout << " Hit proper number (from -1100 to +1100)" << std::endl;
                    HVfi = 0.;
                }
                clearStdin();
                break;
            case 3:
                std::cout << " Set step voltage [V]" << std::endl;
                std::cin >> HVst;
                if (HVst == 0) {
                    std::cout << " Invalid number..." << std::endl;
                    HVst = 1.;
                }
                clearStdin();
                break;
            case 4:
                std::cout << " Set interval sleeping time [s]" << std::endl;
                std::cin >> Tint;
                if (Tint < 0) {
                    std::cout << " Invalid number..." << std::endl;
                    Tint = 1.;
                }
                clearStdin();
                break;
            case 5:
                std::cout << " Set number of ramping steps" << std::endl;
                std::cin >> Ramp;
                if (Ramp <= 0) {
                    std::cout << " Invalid number..." << std::endl;
                    Ramp = 1;
                    break;
                }
                clearStdin();
                break;
            case 6:
                std::cout << " Set ramping time [ms]" << std::endl;
                std::cin >> Trmp;
                if (Trmp < 0) {
                    std::cout << " Invalid number..." << std::endl;
                    Trmp = 0;
                    break;
                }
                clearStdin();
                break;
            case 7:
                std::cout << " Set current limit [A]" << std::endl;
                std::cin >> Ilim;
                clearStdin();
                break;
            case 10:
                std::cout << " Set output directory. add / at the back." << std::endl;
                std::cin >> savepath;
                break;
            case 11:
                std::cout << " Set output file name." << std::endl;
                std::cin >> savename;
                break;
            case 20:
                std::cout << " Configuare Keithley " << std::endl;
                ps->configure(si);
                break;
            case 21:
                std::cout << " Reset Keithley " << std::endl;
                ps->reset(si);
                break;
            case 22:
                std::cout << " Set output ON " << std::endl;
                ps->power_on(si);
                break;
            case 23:
                std::cout << " Set output OFF " << std::endl;
                ps->power_off(si);
                break;
            case 0:
                flag = 0;
                clearStdin();
                break;
            case 99:
                return 0;
            default:
                printf(" not defined. doing nothing...\n");
                break;
            }
        }

        ps->power_on(si);
        std::cout << " ===== Set output ON ===== " << std::endl;
        ps->configure(si);
        std::cout << " ===== Configure Keithley ===== " << std::endl;
        // std::cout<<"=====Reset "<< devicename <<"====="<<std::endl;
        // ps->reset(si);
        std::ofstream ofile_IV(savepath + savename + ".dat");
        std::string out_format_IV;
        HV = HVin;
        if (HVst > 0)
        {
            while (HV <= HVfi) {
                if (HV == HVin) ps->set_voltage(HVin, HVin);
                else ps->set_voltage(HV, HVst);
                ps->set_sweep_steps(Ramp);
                ps->set_sweep_sleep_in_ms(Trmp);
                ps->voltage_sweep(si);
                //	ps->config_voltage(si);
                Sleep(Tint);
                ps->read_voltage_and_current(si, voltage, current);
                out_format_IV = get_out_format_IV(HV, current);
                ofile_IV << out_format_IV << std::endl;
                if (abs(current) > abs(Ilim)) break;
                if (HV == HVin) std::cout << "  Voltage\tCurrent\t\tDate/Time" << std::endl;
                std::cout << "  " << out_format_IV << std::endl;
                if (kbhit()) break;
                HV = HV + HVst;
            }
        }
        else {
            while (HV >= HVfi) {
                if (HV == HVin) ps->set_voltage(HVin, HVin);
                else ps->set_voltage(HV, HVst);
                ps->set_sweep_steps(Ramp);
                ps->set_sweep_sleep_in_ms(Trmp);
                ps->voltage_sweep(si);
                //      ps->config_voltage(si);
                Sleep(Tint);
                ps->read_voltage_and_current(si, voltage, current);
                out_format_IV = get_out_format_IV(HV, current);
                ofile_IV << out_format_IV << std::endl;
                if (abs(current) > abs(Ilim)) break;
                if (HV == HVin) std::cout << "  Voltage\tCurrent\tDate/Time" << std::endl;
                std::cout << " " << out_format_IV << std::endl;
                if (kbhit()) break;
                HV = HV + HVst;
            }
        }
        //---supplied voltage down---
        if (abs(HV) / 5 < 1) ram = 1;
        else ram = abs(HV) / 5;
        ps->set_sweep_steps(ram);
        ps->set_sweep_sleep_in_ms(200.);
        ps->set_voltage(0, -HV);
        ps->voltage_sweep(si);

        ofile_IV.close();
        flag = 1;
    }
    return 0;
    /*
    int mNo = 99;
    float HVin = -5;
    float HVfi = 5;
    float HVst = 2;
    float curlim = 1e-3;
    float TMsl = 1.;
    float Ramp = 1.;
    float TMsw = 500.;
    int flag = 1;
    //  std::string filedir = "katsuya/20190120/hp4284A/series_circuit";
    std::string filedir = "katsuya/20190131/Rinp";
    std::string kind_of_exp = "IV";
    std::string filename_in = "test";
    std::string devicename = ps->get_device_type();
    std::string datafilename = "IVtest";
    //  rename(kind_of_exp, filename_in);
    //  std::string datafilename = read_data_file_name();

    //  std::cout<<"this data file name is "<<datafilename<<" in data/"<<filedir<<"/"<<std::endl;

    while (flag) {
        std::cout << "this data file name is " << datafilename << " in data/" << filedir << "/" << std::endl;

        std::cout << "====================================================" << std::endl;
        std::cout << " current limit : " << curlim << "[A]" << std::endl;

        std::cout << "Please specify what you want to do... " << std::endl;
        std::cout << "  1: set initial voltage : " << HVin << "[V]" << std::endl;
        std::cout << "  2: set final voltage : " << HVfi << "[V]" << std::endl;
        std::cout << "  3: set step voltage : " << HVst << "[V]" << std::endl;
        std::cout << "  4: set interval time : " << TMsl << "[ms]" << std::endl;
        std::cout << "  5: set # of ramping steps : " << Ramp << "steps" << std::endl;
        std::cout << "100: set output file directory : " << filedir << std::endl;
        std::cout << "101: set output file name : " << filename_in << std::endl;
        //std::cout<<"200: Initializing Keithley2410 "<<std::endl;
        //std::cout<<"201: Reset Keithley2410 "<<std::endl;
        //std::cout<<"202: Output ON"<<std::endl;
        std::cout << "203: Output OFF" << std::endl;
        std::cout << "  0: start measurement (Auto Output ON)" << std::endl;
        std::cout << " 99: exit " << std::endl;

        while (1) {
            mNo = 999;
            std::cout << "" << std::endl;
            std::cout << "No = ";
            scanf("%d", &mNo);
            if (mNo == 999) clearStdin();
            std::cout << "" << std::endl;
            if (mNo >= 0) break;
        }

        switch (mNo) {
        case 1:
            std::cout << "set initial voltage [V]" << std::endl;
            std::cin >> HVin;
            clearStdin();
            break;
        case 2:
            std::cout << "set final voltage[V]" << std::endl;
            std::cin >> HVfi;
            if (abs(HVfi) > 1100) { //Fail-safe for someone who hit non-numeric value
                std::cout << "Hit proper numble (from -1100 to +1100)" << std::endl;
                HVfi = 0.;
            }
            clearStdin();
            break;
        case 3:
            std::cout << "set step voltage[V]" << std::endl;
            std::cin >> HVst;
            if (HVst == 0) { //Fail-safe for someone who hit non-numeric value
                std::cout << "Invalid number..." << std::endl;
                HVst = 1.;
            }
            clearStdin();
            break;
        case 4:
            std::cout << "set interval sleep time[ms]" << std::endl;
            std::cin >> TMsl;
            if (TMsl < 0 || TMsl == 0) { //Fail-safe for someone who hit non-numeric value
                std::cout << "Invalid number..." << std::endl;
                TMsl = 1.;
            }
            clearStdin();
            break;
        case 5:
            std::cout << "set # of ramping steps" << std::endl;
            std::cin >> Ramp;
            if (Ramp < 0) { //Fail-safe for someone who hit non-numeric value
                std::cout << "Invalid number..." << std::endl;
                clearStdin();
                break;
            }
            break;
        case 100:
            std::cout << "set output file directory" << std::endl;
            std::cin >> filedir;
            break;
        case 101:
            std::cout << "set output file name" << std::endl;
            //      std::cin>>filename_in;
            std::cin >> datafilename;
            break;

            //case 200:
            //printf("=== Configuare Keithley2410.\n");
            //ps->configure(si);
            //break;
            // case 201:
            //printf("=== Reset Keithley2410.\n");
            //ps->reset(si);
            //break;
            //case 202:
            //printf("=== Set output ON.\n");
            //ps->power_on(si); //Output ON with initial voltage
            //break;
        case 203:
            printf("=== Set output OFF.\n");
            ps->power_off(si); //Output ON with initial voltage
            break;
        case 0:
            flag = 0;
            clearStdin();
            break;


        case 99:
            return 0;

        default:
            printf("not defined. doing nothing...\n");
            break;
        }
    }

    printf("=== Set output ON.\n");
    ps->power_on(si); //Output ON with initial voltag;    
    std::cout << "=====Configure " << devicename << "=====" << std::endl;
    ps->configure(si);
    // std::cout<<"=====Reset "<< devicename <<"====="<<std::endl;
    // ps->reset(si);

    std::cout << "==========complete settings==========" << std::endl;

    std::ofstream ofile_IV("data/" + filedir + "/" + datafilename + ".dat", std::ofstream::app);
    std::string out_format_IV;
    double voltage(0.), current(0.);
    float HV = HVin;

    if (HVst > 0)
    {
        while (HV <= HVfi) {
            //	ps->set_sweep_steps(Ramp);
            //	ps->set_sweep_sleep_in_ms(TMsl);
            ps->set_voltage(HV);
            //	ps->voltage_sweep(si);
            ps->config_voltage(si);
            Sleep(2);
            // if(HV>300) sleep(5);

            //	for(int i=0;i<10;i++){
            ps->read_voltage_and_current(si, voltage, current);
            out_format_IV = get_out_format_IV(HV, current);
            ofile_IV << out_format_IV << std::endl;
            if (fabs(current) > curlim) break;
            if (HV == HVin) std::cout << "Voltage\tCurrent\tDate/Time" << std::endl;
            std::cout << out_format_IV << std::endl;
            //      }
            if (kbhit()) break;
            HV = HV + HVst;
            Sleep(0.1);
        }
    }
    else {
        while (HV >= HVfi) {
            //      ps->set_sweep_steps(Ramp);
            //      ps->set_sweep_sleep_in_ms(TMsl);
            ps->set_voltage(HV);
            //      ps->voltage_sweep(si);
            ps->config_voltage(si);
            Sleep(2);
            // if(fabs(HV)>300) sleep(5);
            // if(fabs(HV)>600) sleep(8);
            //      for(int i=0;i<10;i++){
            ps->read_voltage_and_current(si, voltage, current);
            out_format_IV = get_out_format_IV(HV, current);
            ofile_IV << out_format_IV << std::endl;
            if (fabs(current) > curlim) break;
            if (HV == HVin) std::cout << "Voltage\tCurrent\tDate/Time" << std::endl;
            std::cout << out_format_IV << std::endl;
            //    }

            if (kbhit()) break;
            HV = HV + HVst;
            Sleep(0.1);
        }
    }
    
//    int ram =abs(ps->read_voltage(si))/10;
//    ps->set_sweep_steps(ram);
//    ps->set_sweep_sleep_in_ms(100.);
//    ps->set_voltage(0);
//    ps->voltage_sweep(si);
    
    while (HV != 0) {
        if (fabs(HV) <= fabs(HVst)) {
            ps->set_voltage(0);
            ps->config_voltage(si);
            std::cout << "applied voltage : " << HV << " [V]" << std::endl;
            break;
        }
        HV = HV - HVst;
        ps->set_voltage(HV);
        //      sleep(1);
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        ps->config_voltage(si);
        std::cout << "applied voltage : " << HV << " [V]" << std::endl;
    }
    ofile_IV.close();

    return 1;
    */
}
void IVCVCtrl::SetFreq(LCR_meter* lcr, serial_interface* si) {
    double freqin = 2000;

    std::cout << " Set frequency [Hz]" << std::endl;
    std::cin >> freqin;
    if (freqin >= 5 && freqin <= 13000000) {
        lcr->set_frequency(freqin);
        lcr->config_frequency(si);
    }
    else {
        std::cout << " Type available number..." << std::endl;
        freqin = 1000;
    }
    std::cout << "freq : " << freqin << std::endl;
    clearStdin();
}
void IVCVCtrl::ReadC(LCR_meter* lcr, serial_interface* si)
{
    /*
    lcr->configure(si);

    double freqin = 2000;


    std::cout << " Set frequency [Hz]" << std::endl;
    std::cin >> freqin;
    if (freqin >= 5 && freqin <= 13000000) {
        lcr->set_frequency(freqin);
        lcr->config_frequency(si);
    }
    else {
        std::cout << " Type available number..." << std::endl;
        freqin = 1000;
    }
    std::cout << "freq : " << freqin << std::endl;
    clearStdin();
//    lcr->configure(si);

*/
//    double deg = 0;
//    double cap = 0;
//    Sleep(1000); std::cout << "reading impedance" << std::endl;
//    lcr->read_capacitance_and_degree(si, cap, deg);


    clearStdin();
    int trial = 0;
    printf("==============================================\n");
    printf("Hit any key to return to the prompt.\n");
    printf("==============================================\n");
    while (1) {
        //Reading voltage and current
        double voltage(0.);
        //Preparing output file
        double deg = 0;
        double cap = 0;
        //    Sleep(1000); std::cout << "reading impedance" << std::endl;
        lcr->read_capacitance_and_degree(si, cap, deg);
        std::cout << cap << " " << deg << std::endl;
        if (kbhit()) break;
        trial++;
        Sleep(1);
    }
    clearStdin();




}
int IVCVCtrl::IVCVread(power_supply* ps, LCR_meter* lcr, serial_interface* si)
{
    int mNo = 99;
    int kNo = 1;
    int flag = 1;
    int flagw;
    int ram = 2;
    double freqin = 2000;
    double freqfi = 5000;
    double freqst = 100;
    double deg = 0;
    double cap = 0;
    double HVin = 0;
    double HVfi = -500;
    double HVst = -10;
    double Tint = 3;
    double Ramp = 1;
    double Trmp = 500.;
    double Ilim = 1e-3;
    double D = 0.02;//Damping factor
    double HV;
    double freq = freqin;
    double voltage(0.), current(0.);
    int flag1 = 0;
    int flag2 = 0;
    double tmpdeg = 100;
    std::string savepath = "C:\\work\\Silicon\\DataOutput\\";
    //    std::string kind_of_exp = "CV_17LS_1e14";
    std::string savename = "hoge";
    //  std::string filename_in = "test";
    //  std::string devicename = ps->get_device_type();
    //  std::string datafilename = read_data_file_name();
    //  rename(kind_of_exp, filename_in);
    //  std::cout<<"this data file name is "<<datafilename<<" in data/"<<filedir<<"/"<<std::endl;

    while (1) {
        while (flag) {
            freqfi = freqin;
            int frei = freqin;
            std::cout << "" << std::endl;
            std::cout << " ====================================================" << std::endl;
            std::cout << " this data file : " << savepath << savename << ".dat" << std::endl;
            std::cout << " Please specify what you want to do... " << std::endl;
            std::cout << "   0: start measurement (Auto Output ON)" << std::endl;
            std::cout << "   1: set initial voltage : " << HVin << "[V]" << std::endl;
            std::cout << "   2: set final voltage   : " << HVfi << "[V]" << std::endl;
            std::cout << "   3: set step voltage    : " << HVst << "[V]" << std::endl;
            std::cout << "   4: set interval time   : " << Tint << "[s]" << std::endl;
            std::cout << "   5: set # of ramping steps : " << Ramp << "steps" << std::endl;
            std::cout << "   6: set ramping time       : " << Trmp << "[ms]" << std::endl;
            std::cout << "   7: set current limit   : " << Ilim << "[A]" << std::endl;
            std::cout << "  10: set frequency : " << freqin << "[Hz]" << std::endl;
            std::cout << "  11: zero open calibration  " << std::endl;
            std::cout << "  12: zero short calibration " << std::endl;
            std::cout << "  13: configure LCRmeter " << std::endl;
            std::cout << "  13: set damping factor : " << D << std::endl;
            // std::cout<<"  13: set frequency step :"<<freqst<<"[Hz]"<<std::endl;
            std::cout << "  20: Initializing Keithley2410 " << std::endl;
            std::cout << "  21: Reset Keithley2410 " << std::endl;
            std::cout << "  22: Output ON" << std::endl;
            std::cout << "  23: Output OFF" << std::endl;
            std::cout << "  90: set output file directory : " << savepath << std::endl;
            std::cout << "  91: set output file name      : " << savename << "(.dat)" << std::endl;
            std::cout << "  99: exit " << std::endl;
            std::cout << " ====================================================" << std::endl;

            while (1) {
                mNo = 999;
                std::cout << "" << std::endl;
                std::cout << " No = ";
                scanf("%d", &mNo);
                if (mNo == 999) clearStdin();
                std::cout << "" << std::endl;
                if (mNo >= 0) break;
            }


            switch (mNo) {
            case 1:
                std::cout << " Set initial voltage [V]" << std::endl;
                std::cin >> HVin;
                if (abs(HVin) > 1100) {
                    std::cout << " Hit proper number (from -1100 to +1100)" << std::endl;
                    HVin = 0.;
                }
                clearStdin();
                break;
            case 2:
                std::cout << " Set final voltage [V]" << std::endl;
                std::cin >> HVfi;
                if (abs(HVfi) > 1100) {
                    std::cout << " Hit proper number (from -1100 to +1100)" << std::endl;
                    HVfi = 0.;
                }
                clearStdin();
                break;
            case 3:
                std::cout << " Set step voltage [V]" << std::endl;
                std::cin >> HVst;
                if (HVst == 0) {
                    std::cout << " Invalid number..." << std::endl;
                    HVst = 1.;
                }
                clearStdin();
                break;
            case 4:
                std::cout << " Set interval sleeping time [s]" << std::endl;
                std::cin >> Tint;
                if (Tint < 0) {
                    std::cout << " Invalid number..." << std::endl;
                    Tint = 1.;
                }
                clearStdin();
                break;
            case 5:
                std::cout << " Set number of ramping steps" << std::endl;
                std::cin >> Ramp;
                if (Ramp <= 0) {
                    std::cout << " Invalid number..." << std::endl;
                    Ramp = 1;
                    break;
                }
                clearStdin();
                break;
            case 6:
                std::cout << " Set ramping time [ms]" << std::endl;
                std::cin >> Trmp;
                if (Trmp < 0) {
                    std::cout << " Invalid number..." << std::endl;
                    Trmp = 0;
                    break;
                }
                clearStdin();
                break;
            case 7:
                std::cout << " Set current limit [A]" << std::endl;
                std::cin >> Ilim;
                clearStdin();
                break;
            case 10:
                std::cout << " Set frequency [Hz]" << std::endl;
                std::cin >> freqin;
                if (freqin >= 5 && freqin <= 13000000) {
                    lcr->set_frequency(freqin);
                    lcr->config_frequency(si);
                }
                else {
                    std::cout << " Type available number..." << std::endl;
                    freqin = 1000;
                }
                clearStdin();
                break;
            case 11:
                std::cout << " Calibration by Zero Open" << std::endl;
                std::cout << " If you complete set, enter 0. " << std::endl;
                std::cin >> kNo;
                if (kNo == 0) {
                    lcr->zeroopen(si);
                    std::cout << " Zero Open done" << std::endl;
                }
                break;
            case 12:
                std::cout << " Calibration by Zero Short" << std::endl;
                std::cout << " If you complete set, enter 0. " << std::endl;
                std::cin >> kNo;
                if (kNo == 0) {
                    lcr->zeroshort(si);
                    std::cout << " Zero Short done" << std::endl;
                }
                break;
            case 13:
                std::cout << " Set Damping factor" << std::endl;
                std::cin >> D;
                break;
            case 20:
                std::cout << " Configuare Keithley " << std::endl;
                ps->configure(si);
                break;
            case 21:
                std::cout << " Reset Keithley " << std::endl;
                ps->reset(si);
                break;
            case 22:
                std::cout << " Set output ON " << std::endl;
                ps->power_on(si);
                break;
            case 23:
                std::cout << " Set output OFF " << std::endl;
                ps->power_off(si);
                break;
            case 90:
                std::cout << " Set output directory. add / at the back." << std::endl;
                std::cin >> savepath;
                break;
            case 91:
                std::cout << " Set output file name." << std::endl;
                std::cin >> savename;
                break;
            case 0:
                flag = 0;
                clearStdin();
                break;
            case 99:
                return 0;
            default:
                printf(" not defined. doing nothing...\n");
                break;
            }
        }

        std::ofstream ofile_IVCV(savepath + savename + ".dat");
        std::string out_format_IVCV;
        lcr->set_frequency(freqin);
        lcr->config_frequency(si);
        lcr->configure(si);
        printf(" ===== Configuare LCRmeter ===== \n");
        ps->power_on(si);
        printf(" ===== Set output ON ===== \n");
        ps->configure(si);
        std::cout << " ===== Configure Keithley =====  " << std::endl;
        std::cout << " ========== Complete settings ========== " << std::endl;
        HV = HVin;
        flagw = 1;
        if (HVst > 0) {
            if (flagw == 0) break;
            while (HV <= HVfi && flagw != 0) {
                if (HV == HVin) ps->set_voltage(HVin, HVin);
                else ps->set_voltage(HV, HVst);
                ps->set_sweep_steps(Ramp);
                ps->set_sweep_sleep_in_ms(Trmp);
                ps->voltage_sweep(si);
                //	ps->config_voltage(si);
                Sleep(Tint);
                lcr->read_capacitance_and_degree(si, cap, deg);
                ps->read_voltage_and_current(si, voltage, current);
                //	  std::cout<<"voltage :\t"<<voltage<<std::endl;
                std::cout << "voltage :\t" << HV << std::endl;
                std::cout << "current :\t" << current << std::endl;
                std::cout << "frequency :\t" << freq << std::endl;
                std::cout << "capacitance :\t" << cap << std::endl;
                std::cout << "degree :\t" << deg << std::endl;
                std::cout << "" << std::endl;
                out_format_IVCV = get_out_format_IVCV(HV, current, freq * 1000, cap, deg);
                ofile_IVCV << out_format_IVCV << std::endl;
                if (kbhit()) { flagw = 0; break; }
                if (fabs(current) > Ilim) { flagw = 0; break; }
                HV = HV + HVst;
            }
        }

        else {
            if (flagw == 0) break;
            freq = freqin;
            while (HV >= HVfi && flagw != 0) {
                if (HV == HVin) ps->set_voltage(HVin, HVin);
                else ps->set_voltage(HV, HVst);
                ps->set_sweep_steps(Ramp);
                ps->set_sweep_sleep_in_ms(Trmp);
                ps->voltage_sweep(si);
                //	ps->config_voltage(si);
                Sleep(Tint);

                ////////////////////////////////////////////////////////////////////////////
                //Cbulk
                while (freq < 1e3) {
                    lcr->set_frequency(freq);
                    lcr->config_frequency(si);
                    Sleep(0.1);
                    lcr->read_capacitance_and_degree(si, cap, deg);
                    if (fabs(deg) <= D) { flag1 = 0; flag2 = 0; break; }
                    if (deg < -D) { freq = freq + 10; flag1 = 1; }
                    if (deg > D) { freq = freq - 10; flag2 = 1; }
                    if (flag1 == 1 && flag2 == 1) { flag1 = 0; flag2 = 0; break; }
                    if (kbhit()) { flagw = 0; break; }
                }
                while (freq >= 1e3 && freq < 1e4) {
                    lcr->set_frequency(freq);
                    lcr->config_frequency(si);
                    Sleep(0.1);
                    lcr->read_capacitance_and_degree(si, cap, deg);
                    if (fabs(deg) <= D) { flag1 = 0; flag2 = 0; break; }
                    if (deg < -D) { freq = freq + 1e2; flag1 = 1; }
                    if (deg > D) { freq = freq - 1e2; flag2 = 1; }
                    if (flag1 == 1 && flag2 == 1) { flag1 = 0; flag2 = 0; break; }
                    if (kbhit()) { flagw = 0; break; }
                }
                while (freq >= 1e4 && freq < 1e5) {
                    lcr->set_frequency(freq);
                    lcr->config_frequency(si);
                    Sleep(0.1);
                    lcr->read_capacitance_and_degree(si, cap, deg);
                    if (fabs(deg) <= D) { flag1 = 0; flag2 = 0; break; }
                    if (deg < -D) { freq = freq + 1e3; flag1 = 1; }
                    if (deg > D) { freq = freq - 1e3; flag2 = 1; }
                    if (flag1 == 1 && flag2 == 1) { flag1 = 0; flag2 = 0; break; }
                    if (kbhit()) { flagw = 0; break; }
                }
                while (freq >= 1e5 && freq < 1e6) {
                    lcr->set_frequency(freq);
                    lcr->config_frequency(si);
                    Sleep(0.1);
                    lcr->read_capacitance_and_degree(si, cap, deg);
                    if (fabs(deg) <= D) { flag1 = 0; flag2 = 0; break; }
                    if (deg < -D) { freq = freq + 1e4; flag1 = 1; }
                    if (deg > D) { freq = freq - 1e4; flag2 = 1; }
                    if (flag1 == 1 && flag2 == 1) { flag1 = 0; flag2 = 0; break; }
                    if (kbhit()) { flagw = 0; break; }
                }

                ////////////////////////////////////////////////////////////////////////////

                lcr->read_capacitance_and_degree(si, cap, deg);
                ps->read_voltage_and_current(si, voltage, current);
                //	  std::cout<<"voltage :\t"<<voltage<<std::endl;
                std::cout << "voltage :\t" << HV << std::endl;
                std::cout << "current :\t" << current << std::endl;
                std::cout << "frequency :\t" << freq << std::endl;
                std::cout << "capacitance :\t" << cap << std::endl;
                std::cout << "degree :\t" << deg << std::endl;
                std::cout << "" << std::endl;
                out_format_IVCV = get_out_format_IVCV(HV, current, freq * 1000, cap, deg);
                ofile_IVCV << out_format_IVCV << std::endl;
                //	  std::cout<<out_format_IVCV<<std::endl; //confirmation
                //	  freq = freq + freqst;
                if (kbhit()) { flagw = 0; break; }
                if (fabs(current) > Ilim) { flagw = 0; break; }
                //	}	
                HV = HV + HVst;
                //	freq = freqin;
            }
        }

        //---supplied voltage down---
        if (abs(HV) / 5 < 1) ram = 1;
        else ram = abs(HV) / 5;
        ps->set_sweep_steps(ram);
        ps->set_sweep_sleep_in_ms(200.);
        ps->set_voltage(0, -HV);
        ps->voltage_sweep(si);

        ofile_IVCV.close();
        flag = 1;
    }
    return 0;
    /*
    int mNo = 99;
    int kNo = 1;
    int flag = 1;
    double freqin = 100;
    double freqfi = 5000;
    double freqst = 100;
    double deg = 0;
    double cap = 0;
    float HVin = 0;
    float HVfi = -500;
    float HVst = -10;
    float TMsl = 3.;
    float Ramp = 1.;
    float TMsw = 500.;
    double curlim = 200e-6;
    std::string filedir = "katsuya/20190120/hp4284A/series_circuit";
    //  std::string filedir = "katsuya/MD/test3";
    //    std::string kind_of_exp = "CV_17LS_1e14";
    std::string datafilename = "CVtest";
    std::string filename_in = "test";
    std::string devicename = ps->get_device_type();
    //  std::string datafilename = read_data_file_name();

    //  rename(kind_of_exp, filename_in);
    //  std::cout<<"this data file name is "<<datafilename<<" in data/"<<filedir<<"/"<<std::endl;

    while (1) {

        while (flag) {
            freqfi = freqin;
            int frei = freqin;
            // std::string filename_in = std::to_string(frei);
            // rename(kind_of_exp, filename_in);
            std::cout << "this data file name is " << datafilename << " in data/" << filedir << "/" << std::endl;
            std::cout << "  1: set frequency : " << freqin << "[Hz]" << std::endl;
            // std::cout<<"  1: set initial frequency : "<<freqin<<"[Hz]"<<std::endl;
            // std::cout<<"  2: set final frequency :"<<freqfi<<"[Hz]"<<std::endl;
            // std::cout<<"  3: set frequency step :"<<freqst<<"[Hz]"<<std::endl;
            std::cout << "  4: set initial voltage :" << HVin << "[V]" << std::endl;
            std::cout << "  5: set final voltage :" << HVfi << "[V]" << std::endl;
            std::cout << "  6: set step voltage :" << HVst << "[V]" << std::endl;
            std::cout << "  7: set interval time : " << TMsl << "[s]" << std::endl;
            //      std::cout<<"  8: set # of ramping steps : "<<Ramp<<"steps"<<std::endl;
            std::cout << "  9: set current compliance : " << curlim << "[A]" << std::endl;
            std::cout << "  10: hp4192A calibration zero open " << std::endl;
            std::cout << "  11: hp4192A calibration zero short" << std::endl;
            std::cout << "  12: hp4192A self test" << std::endl;
            std::cout << "  20: filename set" << std::endl;
            std::cout << "  111: config lcrmeter" << std::endl;
            std::cout << "  0: start measurement " << std::endl;
            std::cout << "  99: exit " << std::endl;

            while (1) {
                mNo = 999;
                std::cout << "" << std::endl;
                std::cout << "No = ";
                scanf("%d", &mNo);
                if (mNo == 999) clearStdin();
                std::cout << "" << std::endl;
                if (mNo >= 0) break;
            }

            switch (mNo) {
            case 1:
                std::cout << "set initial frequency [Hz]" << std::endl;
                std::cout << "available frequency range:5Hz~13MHz" << std::endl;
                std::cin >> freqin;
                if (freqin >= 5 && freqin <= 13000000) {
                    lcr->set_frequency(freqin);
                    lcr->config_frequency(si);
                }
                else {
                    std::cout << "type available number..." << std::endl;
                    freqin = 1000;
                }
                clearStdin();
                break;
            case 2:
                std::cout << "set final frequency [Hz]" << std::endl;
                std::cout << "available frequency range:5Hz~13MHz" << std::endl;
                std::cin >> freqfi;
                if (freqfi >= 5 && freqfi <= 13000000) {
                    //lcr->set_frequency(freqfi);
                    //lcr->config_frequency(si);
                }
                else {
                    std::cout << "type available number..." << std::endl;
                    freqfi = 1000;
                }
                clearStdin();
                break;
            case 3:
                std::cout << "enter step frequency > 0" << std::endl;
                std::cin >> freqst;
                if (freqst <= 0) {
                    std::cout << "enter available number..." << std::endl;
                    freqst = 1000;
                }
                clearStdin();
                break;
            case 4:
                std::cout << "set initial voltage [V]" << std::endl;
                std::cin >> HVin;
                clearStdin();
                break;
            case 5:
                std::cout << "set final voltage[V]" << std::endl;
                std::cin >> HVfi;
                if (abs(HVfi) > 1100) { //Fail-safe for someone who hit non-numeric value
                    std::cout << "Hit proper numble (from -1100 to +1100)" << std::endl;
                    HVfi = 0.;
                }
                clearStdin();
                break;
            case 6:
                std::cout << "set step voltage[V]" << std::endl;
                std::cin >> HVst;
                if (HVst == 0) { //Fail-safe for someone who hit non-numeric value
                    std::cout << "Invalid number..." << std::endl;
                    HVst = 10.;
                }
                clearStdin();
                break;
            case 7:
                std::cout << "set interval sleep time[ms]" << std::endl;
                std::cin >> TMsl;
                if (TMsl < 0 || TMsl == 0) { //Fail-safe for someone who hit non-numeric value
                    std::cout << "Invalid number..." << std::endl;
                    TMsl = 1.;
                }
                clearStdin();
                break;
                // case 8:
                // 	std::cout<<"set # of ramping steps"<<std::endl;
                // 	std::cin>>Ramp;
                // 	if(Ramp<0){ //Fail-safe for someone who hit non-numeric value
                // 	  std::cout<<"Invalid number..."<<std::endl;
                // 	  clearStdin();
                // 	  break;
                // 	}
                // 	break;
            case 9:
                std::cout << "set currrent compliance [A]" << std::endl;
                std::cin >> curlim;
                clearStdin();
                break;
            case 10:
                std::cout << "Please do not connect to sample (open)..." << std::endl;
                std::cout << "if you complete set, enter 0. " << std::endl;
                std::cin >> kNo;
                if (kNo == 0) {
                    lcr->zeroopen(si);
                    std::cout << "Zero Open done" << std::endl;
                }
                else break;
                break;
            case 11:
                std::cout << "Please connect to short circuiting plate (short)..." << std::endl;
                std::cout << "if you complete set, enter 0. " << std::endl;
                std::cin >> kNo;
                if (kNo == 0) {
                    lcr->zeroshort(si);
                    std::cout << "Zero Short done" << std::endl;
                }
                else break;
                break;

            case 20:
                std::cout << "set output file name" << std::endl;
                std::cin >> datafilename;
                break;
            case 0:
                flag = 0;
                clearStdin();
                break;
            case 99:
                return 0;
            case 111:
                lcr->configure(si);
                break;
            }
        }

        lcr->set_frequency(freqin);
        lcr->config_frequency(si);
        lcr->configure(si);
        printf("Configuare hp4192A...\n");
        ps->power_on(si);
        printf("Set output ON.\n");
        ps->configure(si);
        std::cout << "Configure " << devicename << std::endl;
        std::cout << "==========complete settings==========" << std::endl;
        std::ofstream ofile_IVCV("data/" + filedir + "/" + datafilename + ".dat", std::ofstream::app);
        std::string out_format_IVCV;
        float HV = HVin;
        double freq = freqin;
        double voltage(0.), current(0.);
        int flagw = 1;
        if (HVst > 0) {
            if (flagw == 0) break;
            while (HV <= HVfi && flagw != 0) {
                //	if(flagw==0) break;
                //	ps->set_sweep_steps(Ramp);
                //	ps->set_sweep_sleep_in_ms(TMsl);
                ps->set_voltage(HV);
                //	ps->voltage_sweep(si);
                ps->config_voltage(si);
                //	while(freq<=freqfi){
                Sleep(TMsl);
                // lcr->set_frequency(freq);
                // lcr->config_frequency(si);
                lcr->read_capacitance_and_degree(si, cap, deg);
                ps->read_voltage_and_current(si, voltage, current);
                //	  std::cout<<"voltage :\t"<<voltage<<std::endl;
                std::cout << "voltage :\t" << HV << std::endl;
                std::cout << "current :\t" << current << std::endl;
                std::cout << "frequency :\t" << freq << std::endl;
                std::cout << "capacitance :\t" << cap << std::endl;
                std::cout << "degree :\t" << deg << std::endl;
                std::cout << "" << std::endl;
                out_format_IVCV = get_out_format_IVCV(HV, current, freq * 1000, cap, deg);
                ofile_IVCV << out_format_IVCV << std::endl;
                //	  std::cout<<out_format_IVCV<<std::endl; //confirmation
                //	  freq = freq + freqst;
                if (kbhit()) { flagw = 0; break; }
                if (fabs(current) > curlim) { flagw = 0; break; }
                //	}	
                HV = HV + HVst;
                //	freq = freqin;
            }
        }

        else {
            if (flagw == 0) break;
            while (HV >= HVfi && flagw != 0) {
                //	if(flagw==0) break;
                //	ps->set_sweep_steps(Ramp);
                //	ps->set_sweep_sleep_in_ms(TMsl);
                ps->set_voltage(HV);
                //	ps->voltage_sweep(si);
                ps->config_voltage(si);
                //	while(freq<=freqfi){
                Sleep(TMsl);
                // lcr->set_frequency(freq);
                // lcr->config_frequency(si);

              ////////////////////////////////////////////////////////////////////////////
                int flag1 = 0;
                int flag2 = 0;
                double tmpdeg = 100;
                //Cbulk
                while (freq <= 1e3) {
                    lcr->set_frequency(freq);
                    lcr->config_frequency(si);
                    Sleep(0.1);
                    lcr->read_capacitance_and_degree(si, cap, deg);
                    if (fabs(deg) <= 0.05) break;
                    if (deg < -0.05) { freq = freq + 10; flag1 = 1; }
                    if (deg > 0.05) { freq = freq - 10; flag2 = 2; }
                    if (flag1 == 1 && flag2 == 1) { break; flag1 = 0, flag2 = 0; }
                    if (kbhit()) { flagw = 0; break; }
                }
                while (freq >= 1e3 && freq <= 1e4) {
                    lcr->set_frequency(freq);
                    lcr->config_frequency(si);
                    Sleep(0.1);
                    lcr->read_capacitance_and_degree(si, cap, deg);
                    if (fabs(deg) <= 0.05) break;
                    if (deg < -0.05) { freq = freq + 50; flag1 = 1; }
                    if (deg > 0.05) { freq = freq - 50; flag2 = 2; }
                    if (flag1 == 1 && flag2 == 1) { break; flag1 = 0, flag2 = 0; }
                    if (kbhit()) { flagw = 0; break; }
                }
                while (freq >= 1e4 && freq <= 1e5) {
                    lcr->set_frequency(freq);
                    lcr->config_frequency(si);
                    Sleep(0.1);
                    lcr->read_capacitance_and_degree(si, cap, deg);
                    if (fabs(deg) <= 0.05) break;
                    if (deg < -0.05) { freq = freq + 250; flag1 = 1; }
                    if (deg > 0.05) { freq = freq - 250; flag2 = 2; }
                    if (flag1 == 1 && flag2 == 1) { break; flag1 = 0, flag2 = 0; }
                    if (kbhit()) { flagw = 0; break; }
                }
                while (freq >= 1e5 && freq <= 1e6) {
                    lcr->set_frequency(freq);
                    lcr->config_frequency(si);
                    Sleep(0.1);
                    lcr->read_capacitance_and_degree(si, cap, deg);
                    if (fabs(deg) <= 0.05) break;
                    if (deg < -0.05) { freq = freq + 2500; flag1 = 1; }
                    if (deg > 0.05) { freq = freq - 2500; flag2 = 2; }
                    if (flag1 == 1 && flag2 == 1) { break; flag1 = 0, flag2 = 0; }
                    if (kbhit()) { flagw = 0; break; }
                }

                //Cint

                // while(freq>=1e4){
                //   lcr->set_frequency(freq);
                //   lcr->config_frequency(si);
                //   sleep(0.1);
                //   lcr->read_capacitance_and_degree(si, cap, deg);
                //   if(fabs(deg)<=0.02) break;
                //   if(deg>0.02) { freq = freq + 1e3; flag1=1; }
                //   if(deg<-0.02)  { freq = freq - 1e3; flag2=2; }
                //   if(flag1==1 && flag2==1) { flag1=0, flag2=0; break; }
                //   if(kbhit()) { flagw=0; break; }
                // }
                // while(freq>=1e3 && freq<=1e4){
                //   lcr->set_frequency(freq);
                //   lcr->config_frequency(si);
                //   sleep(0.1);
                //   lcr->read_capacitance_and_degree(si, cap, deg);
                //   if(fabs(deg)<=0.02) break;
                //   if(deg<-0.02) { freq = freq + 1e4; flag1=1; }
                //   if(deg>0.02)  { freq = freq - 1e4; flag2=2; }
                //   if(flag1==1 && flag2==1) { flag1=0, flag2=0; break; }
                //   if(kbhit()) { flagw=0; break; }
                // }
                ////////////////////////////////////////////////////////////////////////////

                lcr->read_capacitance_and_degree(si, cap, deg);
                ps->read_voltage_and_current(si, voltage, current);
                //	  std::cout<<"voltage :\t"<<voltage<<std::endl;
                std::cout << "voltage :\t" << HV << std::endl;
                std::cout << "current :\t" << current << std::endl;
                std::cout << "frequency :\t" << freq << std::endl;
                std::cout << "capacitance :\t" << cap << std::endl;
                std::cout << "degree :\t" << deg << std::endl;
                std::cout << "" << std::endl;
                out_format_IVCV = get_out_format_IVCV(HV, current, freq * 1000, cap, deg);
                ofile_IVCV << out_format_IVCV << std::endl;
                //	  std::cout<<out_format_IVCV<<std::endl; //confirmation
                //	  freq = freq + freqst;
                if (kbhit()) { flagw = 0; break; }
                if (fabs(current) > curlim) { flagw = 0; break; }
                //	}	
                HV = HV + HVst;
                //	freq = freqin;
            }
        }
        while (HV != 0) {
            HV = HV - HVst;
            ps->set_voltage(HV);
            //      sleep(1);
            std::this_thread::sleep_for(std::chrono::milliseconds(300));
            if (abs(HV) < abs(HVst)) {
                ps->set_voltage(0);
            }
            ps->config_voltage(si);
            std::cout << "applied voltage : " << HV << " [V]" << std::endl;
        }
        
 //         int ram =abs(ps->read_voltage(si))/10;
 //         ps->set_sweep_steps(ram);
 //         ps->set_sweep_sleep_in_ms(100.);
 //         ps->set_voltage(0);
 //         ps->voltage_sweep(si);
        
        ofile_IVCV.close();
        flag = 1;
    }
    
    return 0;
    */
}

int IVCVCtrl::sweepfreq(power_supply* ps, LCR_meter* lcr, serial_interface* si) {
    int mNo = 99;
    int kNo = 1;
    int flag = 1;
    const char* dummy;
    double freq = 0;
    double cap = 0;
    double deg = 0;
    double sfreq = 500; //step freq
    double tfreq = 500; //start freq
    double pfreq = 100000; //stop freq
    //  double HVfi=0;
    int nos = (pfreq - tfreq) / sfreq;
    std::string buffer;
    std::string filedir = "C:\\work\\Silicon\\DataOutput\\";
    std::string datafilename = "test";

    while (1) {
        while (flag) {
            std::cout << "this data file name is " <<  filedir << "/" << datafilename << std::endl;
            std::cout << "  1: set start frequency : " << tfreq << "[Hz]" << std::endl;
            std::cout << "  2: set stop frequency :" << pfreq << "[Hz]" << std::endl;
            std::cout << "  3: set step frequency :" << sfreq << "[Hz]" << std::endl;
            //      std::cout<<"  4: set initial voltage :"<<HVin<<"[V]"<<std::endl;
            //      std::cout<<"  5: set voltage :"<<HVfi<<"[V]"<<std::endl;
            //      std::cout<<"  6: set step voltage :"<<HVst<<"[V]"<<std::endl;
            //      std::cout<<"  7: set interval time : "<<TMsl<<"[s]"<<std::endl;
            //      std::cout<<"  8: set # of ramping steps : "<<Ramp<<"steps"<<std::endl;
            //      std::cout<<"  9: set current compliance : "<<curlim<<"[A]"<<std::endl;
            std::cout << "  10: hp4192A calibration zero open " << std::endl;
            std::cout << "  11: hp4192A calibration zero short" << std::endl;
            std::cout << "  12: hp4192A self test" << std::endl;
            std::cout << "  20: filename set" << std::endl;
            std::cout << "  0: start measurement " << std::endl;
            std::cout << "  99: exit " << std::endl;
            nos = (pfreq - tfreq) / sfreq;

            while (1) {
                mNo = 999;
                std::cout << "" << std::endl;
                std::cout << "No = ";
                scanf("%d", &mNo);
                if (mNo == 999) clearStdin();
                std::cout << "" << std::endl;
                if (mNo >= 0) break;
            }

            switch (mNo) {
            case 1:
                std::cout << "set start frequency [Hz]" << std::endl;
                std::cout << "available frequency range:5Hz~13MHz" << std::endl;
                std::cin >> tfreq;
                if (tfreq >= 5 && tfreq <= 13000000) {
                    lcr->set_frequency(tfreq);
                    lcr->config_frequency(si);
                }
                else {
                    std::cout << "type available number..." << std::endl;
                    tfreq = 10000;
                }
                clearStdin();
                break;
            case 2:
                std::cout << "set stop frequency [Hz]" << std::endl;
                std::cout << "available frequency range:5Hz~13MHz" << std::endl;
                std::cin >> pfreq;
                if (pfreq < 5 || pfreq>13000000) {
                    std::cout << "type available number..." << std::endl;
                    pfreq = 10000;
                }
                clearStdin();
                break;
            case 3:
                std::cout << "set step frequency > 0" << std::endl;
                std::cin >> sfreq;
                if (sfreq <= 0) {
                    std::cout << "enter available number..." << std::endl;
                    sfreq = 10000;
                }
                clearStdin();
                break;
                // case 4:
                // 	std::cout<<"set initial voltage [V]"<<std::endl;
                // 	std::cin>>HVin;
                // 	clearStdin();
                // 	break;
                // case 5:
                // 	std::cout<<"set voltage[V]"<<std::endl;
                // 	std::cin>>HVfi;
                // 	if(abs(HVfi)>1100){ //Fail-safe for someone who hit non-numeric value
                // 	  std::cout<<"Hit proper numble (from -1100 to +1100)"<<std::endl;
                // 	  HVfi = 0.;
                // 	}
                // 	clearStdin();
                // 	break;
                // case 6:
                // 	std::cout<<"set step voltage[V]"<<std::endl;
                // 	std::cin>>HVst;
                // 	if(HVst==0){ //Fail-safe for someone who hit non-numeric value
                // 	  std::cout<<"Invalid number..."<<std::endl;
                // 	  HVst = 10.;
                // 	}
                // 	clearStdin();
                // 	break;
                // case 7:
                // 	std::cout<<"set interval sleep time[ms]"<<std::endl;
                // 	std::cin>>TMsl;
                // 	if(TMsl<0 || TMsl==0){ //Fail-safe for someone who hit non-numeric value
                // 	  std::cout<<"Invalid number..."<<std::endl;
                // 	  TMsl = 1.;
                // 	}
                // 	clearStdin();
                // 	break;
                // case 8:
                // 	std::cout<<"set # of ramping steps"<<std::endl;
                // 	std::cin>>Ramp;
                // 	if(Ramp<0){ //Fail-safe for someone who hit non-numeric value
                // 	  std::cout<<"Invalid number..."<<std::endl;
                // 	  clearStdin();
                // 	  break;
                // 	}
                // 	break;
                // case 9:
                // 	std::cout<<"set currrent compliance [A]"<<std::endl;
                // 	std::cin>>curlim;
                // 	clearStdin();
                // 	break;
            case 10:
                std::cout << "Open zero caliblation" << std::endl;
                std::cout << "if you complete set, enter 0. " << std::endl;
                std::cin >> kNo;
                if (kNo == 0) {
                    lcr->zeroopen(si);
                    std::cout << "Zero Open done" << std::endl;
                }
                else {
                    std::cout << "Zero Open caliblation NOT done" << std::endl;
                }
                break;
            case 11:
                std::cout << "Zero short caliblation" << std::endl;
                std::cout << "if you complete set, enter 0. " << std::endl;
                std::cin >> kNo;
                if (kNo == 0) {
                    lcr->zeroshort(si);
                    std::cout << "Zero Short done" << std::endl;
                }
                else {
                    std::cout << "Zero short caliblation NOT done" << std::endl;
                }
                break;

            case 20:
                std::cout << "set output file name" << std::endl;
                std::cin >> datafilename;
                break;
            case 0:
                flag = 0;
                clearStdin();
                break;
            case 99:
                return 0;
            }
        }

        std::ofstream ofile_freq(filedir + "/" + datafilename + ".dat", std::ofstream::app);
        std::string out_format_freq;

        lcr->set_frequency(tfreq);
        lcr->config_frequency(si);
        lcr->set_sfrequency(sfreq);
        lcr->set_tfrequency(tfreq);
        lcr->set_pfrequency(pfreq);
        lcr->sweep_frequency(si, cap, deg, freq);
        for (int ii = 0; ii < nos + 1; ii++) {
            si->write("EX");
            si->write(":READ?\r\n");
            si->write("++read\r\n");
            Sleep(100);

            si->make_talker();
            si->read(buffer);
            std::cout << buffer << std::endl;
            sscanf(buffer.c_str(), "NCPN%lf,%2sFN%lf,K%lf", &cap, &dummy, &deg, &freq);
            if (ii == 0) std::cout << "freq[Hz]" << "\t" << "capacitance[F]" << "\t" << "degree[deg]" << "\t" << std::endl;
            std::cout << freq * 1000 << "\t\t" << cap << "\t\t" << deg << "\t" << std::endl;
            out_format_freq = get_out_format_freq(freq * 1000, cap, deg);
            ofile_freq << out_format_freq << std::endl;
            si->make_listener();
            if (kbhit()) { break; }
        }
        ofile_freq.close();
        flag = 1;
        return 0;
    }
}
int IVCVCtrl::sweepfreq2(power_supply* ps, LCR_meter* lcr, serial_interface* si) {
    int flag = 1;
    int mNo, kNo;
    double freqin = 2e2;
    double freqfi = 1e6;
    double Tint = 0.5;
    double cap, deg, freq, fst;
    std::string savepath = "C:\\work\\Silicon\\DataOutpu\\";
    std::string savename = "hogeswfreq";

    while (1) {
        while (flag) {
            std::cout << "" << std::endl;
            std::cout << " ====================================================" << std::endl;
            std::cout << " this data file : " << savepath << savename << ".dat" << std::endl;
            std::cout << " Please specify what you want to do... " << std::endl;
            std::cout << "   0: start measurement (Auto Output ON)" << std::endl;
            std::cout << "   1: set initial frequency : " << freqin << "[Hz]" << std::endl;
            std::cout << "   2: set final frequency   :" << freqfi << "[Hz]" << std::endl;
            std::cout << "   3: set interval time   : " << Tint << "[s]" << std::endl;
            std::cout << "  11: zero open calibration  " << std::endl;
            std::cout << "  12: zero short calibration " << std::endl;
            std::cout << "  13: configure LCRmeter " << std::endl;
            std::cout << "  90: set output file directory : " << savepath << std::endl;
            std::cout << "  91: set output file name      : " << savename << "(.dat)" << std::endl;
            std::cout << "  99: exit " << std::endl;
            std::cout << " ====================================================" << std::endl;

            while (1) {
                mNo = 999;
                std::cout << "" << std::endl;
                std::cout << " No = ";
                scanf("%d", &mNo);
                if (mNo == 999) clearStdin();
                std::cout << "" << std::endl;
                if (mNo >= 0) break;
            }

            switch (mNo) {
            case 1:
                std::cout << " Set initial frequency [Hz]" << std::endl;
                std::cin >> freqin;
                if (freqin >= 5 && freqin <= 13000000) {
                    lcr->set_frequency(freqin);
                    lcr->config_frequency(si);
                }
                else {
                    std::cout << " Type available number..." << std::endl;
                    freqin = 1000;
                }
                clearStdin();
                break;
            case 2:
                std::cout << " Set final frequency [Hz]" << std::endl;
                std::cin >> freqfi;
                if (freqin < 5 || freqin>13000000) {
                    std::cout << " Type available number..." << std::endl;
                    freqfi = 1000;
                }
                clearStdin();
                break;
            case 3:
                std::cout << " Set interval time [s]" << std::endl;
                std::cin >> Tint;
                if (Tint < 0) {
                    std::cout << " Type available number..." << std::endl;
                    Tint = 0;
                }
                clearStdin();
                break;
            case 11:
                std::cout << " Calibration by Zero Open" << std::endl;
                std::cout << " If you complete set, enter 0. " << std::endl;
                std::cin >> kNo;
                if (kNo == 0) {
                    lcr->zeroopen(si);
                    std::cout << " Zero Open done" << std::endl;
                }
                else break;
                break;
            case 12:
                std::cout << " Calibration by Zero Short" << std::endl;
                std::cout << " If you complete set, enter 0. " << std::endl;
                std::cin >> kNo;
                if (kNo == 0) {
                    lcr->zeroshort(si);
                    std::cout << " Zero Short done" << std::endl;
                }
                else break;
                break;
            case 90:
                std::cout << " Set output directory. add / at the back." << std::endl;
                std::cin >> savepath;
                break;
            case 91:
                std::cout << " Set output file name." << std::endl;
                std::cin >> savename;
                break;
            case 0:
                flag = 0;
                clearStdin();
                break;
            case 99:
                return 0;
            default:
                printf(" not defined. doing nothing...\n");
                break;
            }
        }
        std::ofstream ofile_freq( savepath + savename + ".dat");
        std::string out_format_freq;
        freq = freqin;
        lcr->configure(si);
        while (freq <= freqfi) {
            if (freq > 0 && freq < 1e2) fst = 5;
            else if (freq >= 1e2 && freq < 1e3) fst = 100;
            else if (freq >= 1e3 && freq < 1e4) fst = 1000;
            else if (freq >= 1e4 && freq < 1e5) fst = 10000;
            else if (freq >= 1e5 && freq <= 1e6) fst = 100000;
            else if (freq > 1e6) break;
            lcr->set_frequency(freq);
            lcr->config_frequency(si);
            Sleep(Tint);
            lcr->read_capacitance_and_degree(si, cap, deg);
            out_format_freq = get_out_format_freq(freq, cap, deg);
            ofile_freq << out_format_freq << std::endl;
            freq = freq + fst;
            if (kbhit()) break;
        }
        ofile_freq.close();
        flag = 1;
    }
    return 0;
}
void IVCVCtrl:: SelectFunction() {
    printf(" ====================================================\n");
    printf(" Please specify what you want to do... \n");
    printf(" 0: Initializing Keithley \n");
    printf(" 1: Reset Keithley \n");
    printf(" 2: Configure HV value\n");
    printf(" 3: Sweep HV value to the target\n");
    printf(" 4: Output ON\n");
    printf(" 5: Output OFF\n");
    printf(" 6: Read voltage\n");
    printf(" 7: Read current\n");
    printf(" 8: Read voltage/current\n");
    printf(" 9: Set compliance\n");
    printf(" 10: Initilize LCR meter\n");
    printf(" 11: Set Frequency\n");
    printf(" 12: Read capacitance\n");
//    printf(" 20: Read voltage/capacitance\n");
//    printf(" 21: sweep frequency\n");
    printf(" 30: Change #sweep steps\n");
    printf(" 31: Change sweep interval\n");
    //  printf(" 7: Read voltage/current for all working devices\n");
    //  printf("20: IV measurement\n");
    printf(" 101: IV measurement \n");
 //   printf(" 102: Read capacitance \n");
    printf(" 103: IV&CV current & capacitance \n");
    printf(" 104: Sweep frequency \n");
    printf(" 105: Sweep frequency2 \n");
//    printf(" 110: Output ON\n");
//    printf(" 111: Output OFF\n");
//    printf(" 112: Configure Keithley \n");
    printf("  99: exit \n");
    printf(" ====================================================\n");

    while (1) {
        nNo = 999;
        printf("\n");
        printf("No = ");
        scanf("%d", &nNo);
        if (nNo == 999) clearStdin();
        printf("\n");
        if (nNo >= 0) break;
    }
    /*
    printf("====================================================\n");
    printf("Please specify what you want to do... \n");
    printf("99: exit \n");

    while (1) {
        nNo = 999;
        printf("\n");
        printf("No = ");
        scanf("%d", &nNo);
        if (nNo == 999) clearStdin();
        printf("\n");
        if (nNo >= 0) break;
    }
*/
    return;
}
int IVCVCtrl::CallFunction(power_supply* ps, serial_interface* si, LCR_meter* lcr) {
    int id = 999;
    int addr = 0;
    float HV = 10000;
    int para = -1;
    int flag = 1;
    float comp = 0.001;

    std::string kname = ps->get_device_type();
    std::string lname = lcr->get_device_type();


    std::string devicename = ps->get_device_type();

    switch (nNo) {
    case 0:
        //printf("=== Configuare Keithley2410.\n");
        std::cout << "=====Configure " << devicename << "=====" << std::endl;
        ps->configure(si);
        break;
    case 1:
        //    printf("=== Reset Keithley2410.\n");
        std::cout << "=====Reset " << devicename << "=====" << std::endl;
        ps->reset(si);
        break;
    case 2:
        printf("=== Set HV value.\n");
        printf("Hit desired voltage [V]: ");
        scanf("%f", &HV);
        if (HV == 10000) { //Fail-safe for someone who hit non-numeric value
            printf("Hit a number...");
            clearStdin();
            break;
        }
        ps->set_voltage(HV);
        ps->config_voltage(si);
        break;
    case 3:
        if (!ps->is_on(si)) {
            printf("Voltage sweep is only available with output ON. Doing nothing...\n\n");
            break;
        }
        printf("=== Voltage sweep to the target value.\n");
        printf("Hit desired voltage [V]: ");
        scanf("%f", &HV);
        if (HV == 10000) { //Fail-safe for someone who hit non-numeric value
            printf("Hit a number...");
            clearStdin();
            break;
        }
        std::cout << "Configuring Voltage to " << HV << " [V]" << std::endl;
        ps->set_voltage(HV);
        ps->voltage_sweep(si);
        break;
    case 4:
        printf("=== Set output ON.\n");
        ps->power_on(si); //Output ON with initial voltage
        break;
    case 5:
        printf("=== Set output OFF.\n");
        ps->power_off(si); //Output ON with initial voltage
        break;
    case 6:
        printf("=== Read voltage.\n");
        ReadV(ps, si);
        break;
    case 7:
        printf("=== Read current.\n");
        ReadI(ps, si);
        break;
    case 8:
        printf("=== Read voltage and current.\n");
        ReadIV(ps, si);
        break;
    case 9:
        printf("=== set compliance.\n");
        printf("Hit compliance [A]: ");
        scanf("%f", &comp);
        std::cout << "Configuring Compliance to " << comp << " [A]" << std::endl;
        ps->set_compliance(comp);
        ps->config_compliance(si);
       break;
    case 10:
        //printf("=== Configuare Keithley2410.\n");
        std::cout << "=====Configure LCR meter =====" << std::endl;
        lcr->reset(si);
        lcr->configure(si);
        break;

    case 11:
        std::cout << " -----Set Frequency -----" << std::endl;
        SetFreq(lcr, si);
        break;

    case 12:
        std::cout << " -----Read capacitance-----" << std::endl;
        ReadC(lcr, si);
        //   while(flag){flag=Cread(lcr,si);}
        break;
//    case 20:
//        printf("=== Read voltage/capacitance.\n");
//        while (flag) { flag = IVCVread(ps, lcr, si); }
//        break;
//    case 21:
//        printf("=== Sweep frequency.\n");
//        while (flag) { flag = sweepfreq(ps, lcr, si); }
//        break;
//    case 22:
//        printf("=== Sweep frequency2.\n");
//        while (flag) { flag = sweepfreq2(ps, lcr, si); }
//        break;

    case 30:
        printf("=== Change #sweep steps.\n");
        printf("Hit desired steps: ");
        scanf("%d", &para);
        if (para < 0) { //Fail-safe for someone who hit non-numeric value
            printf("Invalid number...");
            clearStdin();
            break;
        }
        std::cout << "Configuring #sweep steps to " << para << std::endl;
        ps->set_sweep_steps(para);
        break;
    case 31:
        printf("=== Change #sweep sleep time.\n");
        printf("Hit desired sleep time [ms]: ");
        scanf("%d", &para);
        if (para < 0) { //Fail-safe for someone who hit non-numeric value
            printf("Invalid number...");
            clearStdin();
            break;
        }
        std::cout << "Configuring sweep sleep time to " << para << std::endl;
        ps->set_sweep_sleep_in_ms(para);
        break;
        // case 7:
        //   printf("=== Monitoring voltage and current for all working keithleys.\n");
        //   ReadIVAll();
        //   break;
        // case 20:
        //   printf("=== IV measurement.\n");
        //   printf("Hit address:");
        //   scanf("%d",&addr);
        //   if(addr<1 || 15<=addr){
        //     printf("Please specify address in range (2-15).");
        //   }else{
        //     char tmpaddr[5];
        //     sprintf(tmpaddr, "%d", addr);
        //     IVMeasure(tmpaddr);
        //   }
        //   break;
    case 101:
        std::cout << " -----IV : Read current-----" << std::endl;
        while (flag) { flag = IVstepmeasure(ps, si); }
        break;

    case 103:
        std::cout << " -----IV & CV : Read current & capacitance-----" << std::endl;
        while (flag) { flag = IVCVread(ps, lcr, si); }
        break;
    case 104:
        std::cout << " -----Sweep frequency-----" << std::endl;
        while (flag) { flag = sweepfreq(ps, lcr, si); }
        break;
    case 105:
        std::cout << " -----Sweep freqency2-----" << std::endl;
        while (flag) { flag = sweepfreq2(ps, lcr, si); }
        break;
    case 99:
        return 1;
    default:
        printf(" not defined. doing nothing...\n");
        break;
    }
    printf("\n");

    return 0;
    /*

    switch (nNo) {
    case 99:
        return 1;
    default:
        printf("not defined. doing nothing...\n");
        break;
    }
    printf("\n");

    return 0;
    */
}

int IVCVCtrl::main_test(serial_interface* si)
{
    std::cout << " ----- Started PowerSupply control application ----- " << std::endl;


    //---Making devices to control---
    int keithley2410_1_address = 24; //GPIB address
    //  double keithley2410_compliance = 10.E-6;
    power_supply* keithley2410_1 = new keithley2410(keithley2410_1_address);
    keithley2410_1->set_voltage(0, 0); //initial voltage
    //  keithley2410_1->set_compliance(keithley2410_compliance);

    int keithley6517A_1_address = 24; //GPIB adress
    power_supply* keithley6517A_1 = new keithley6517A(keithley6517A_1_address);
    keithley6517A_1->set_voltage(0, 0); //initial voltage

    int keithley6517A_2_address = 29; //GPIB adress
    power_supply* keithley6517A_2 = new keithley6517A(keithley6517A_2_address);
    keithley6517A_2->set_voltage(0, 0); //initial voltage

    int hp4192A_1_address = 2;
    LCR_meter* hp4192A_1 = new hp4192A(hp4192A_1_address);

    int hp4284A_1_address = 3;
    LCR_meter* hp4284A_1 = new hp4284A(hp4284A_1_address);

    //---select device---
    power_supply* keithley = keithley6517A_2;
    LCR_meter* lcr = hp4284A_1;
    int kNo = 100;
    int lNo = 100;
    while (kNo == 100) {
        std::cout << " Select keithley" << std::endl;
        std::cout << "   keithley2410 (addrss:" << keithley2410_1_address << ") -> 1" << std::endl;
        std::cout << "   keithley6517A(addrss:" << keithley6517A_1_address << ") -> 2" << std::endl;
        std::cout << "   keithley6517A(addrss:" << keithley6517A_2_address << ") -> 3" << std::endl;
        std::cout << "   Exit -> 99" << std::endl;
        std::cout << " No = ";
        std::cin >> kNo;
        if (kNo != 1 && kNo != 2 && kNo != 3 && kNo != 99) {
            std::cout << " Enter correct number..." << std::endl;
            kNo = 100;
        }
        if (kNo == 99) { lNo = 99; break; }
    }
    if (kNo == 1) keithley = keithley2410_1;
    if (kNo == 2) keithley = keithley6517A_1;
    if (kNo == 3) keithley = keithley6517A_2;

    while (lNo == 100) {
        std::cout << " Select LCRmeter" << std::endl;
        std::cout << "   hp4192A(addrss:" << hp4192A_1_address << ") -> 1" << std::endl;
        std::cout << "   hp4284A(addrss:" << hp4284A_1_address << ") -> 2" << std::endl;
        std::cout << "   Exit -> 99" << std::endl;
        std::cout << " No = ";
        std::cin >> lNo;
        if (lNo != 1 && lNo != 2 && lNo != 99)
        {
            std::cout << " Enter correct number..." << std::endl;
            lNo = 100;
        }
        if (lNo == 99) { kNo = 99; break; }
    }
    if (lNo == 1) lcr = hp4192A_1;
    if (lNo == 2) lcr = hp4284A_1;

    while (kNo != 99 && lNo != 99) {
        std::cout << " Select " << keithley->get_device_type() << std::endl;
        std::cout << " Select " << lcr->get_device_type() << std::endl;
        SelectFunction();
        int ret = CallFunction(keithley, si, lcr);
        if (ret != 0) break;
    }

    delete keithley2410_1;
    delete keithley6517A_1;
    delete keithley6517A_2;
    //  delete keithley6517B_1;
    delete hp4192A_1;
    delete hp4284A_1;

    std::cout << " ----- Closing application -----" << std::endl;

    return 0;


    /*
    std::cout << "Started PowerSupply control application." << std::endl;

    //Making serial interface for devices
    serial_interface* serial_interface = new prologix_gpibusb();
    std::string device_name = "COM5";
    serial_interface->set_device_name(device_name);
    serial_interface->initialize();
    if (! serial_interface->is_initialized()) {
        std::cout << "[ERROR] failed to create an instance of the serial interface for " << device_name << "." << std::endl;
        return -1;
    }
    //Making devices to control

    int keithley2410_1_address = 24;
    double keithley2410_1_compliance = 10.E-6;
    //double keithley2410_1_output_voltage = 80.;
    power_supply* keithley2410_1 = new keithley2410(keithley2410_1_address);
    keithley2410_1->set_voltage(0.0); //initial voltage
    keithley2410_1->set_compliance(keithley2410_1_compliance);
    //keithley2410_1->set_sweep_steps(20);

    int keithley6517A_1_address = 29;
    double keithley6517A_1_compliance = 10.E-6;
    power_supply* keithley6517A_1 = new keithley6517A(keithley6517A_1_address);
    keithley6517A_1->set_voltage(0.0); //initial voltage
    keithley6517A_1->set_compliance(keithley6517A_1_compliance);


    int hp4192A_1_address = 2;
    LCR_meter* hp4192A_1 = new hp4192A(hp4192A_1_address);

    int hp4284A_1_address = 3;
    LCR_meter* hp4284A_1 = new hp4284A(hp4284A_1_address);

    // //Voltage sweep to the config voltage
    // keithley2410_1->set_voltage(keithley2410_1_output_voltage);
    // keithley2410_1->voltage_sweep(serial_interface);
    // std::this_thread::sleep_for(std::chrono::milliseconds(500));

    // //Preparing output file
    // std::ofstream ofile_keithley_2410_1(keithley2410_1->get_ofile_name(), std::ofstream::app);

    // //Reading voltage and current
    // double voltage(0.), current(0.);
    // keithley2410_1->read_voltage_and_current(serial_interface, voltage, current);

    // //Output information
    // std::string out_format = get_out_format(voltage, current, keithley2410_1_compliance);
    // ofile_keithley_2410_1<<out_format<<std::endl;
    // std::cout<<out_format<<std::endl;

    // //Finalizing operation
    // keithley2410_1->set_voltage(0.);
    // keithley2410_1->voltage_sweep(serial_interface);
    // keithley2410_1->power_off(serial_interface);


    int keithleyNo = 9;
    while (keithleyNo == 9) {
        std::cout << "which keithley do you use? Select [2410]=1,  [6517A]=2, or Exit=99" << std::endl;
        std::cin >> keithleyNo;
        if (keithleyNo == 1) {
            while (1) {
                std::cout << "select " << keithley2410_1->get_device_type() << std::endl;
                SelectFunction();
                int ret = CallFunction(keithley2410_1, serial_interface, hp4192A_1);
                //	int ret = CallFunction(keithley2410_1, serial_interface, hp4284A_1);
                if (ret != 0) break;
            }
        }
        else if (keithleyNo == 2) {
            while (1) {
                std::cout << "select " << keithley6517A_1->get_device_type() << std::endl;
                SelectFunction();
                //			int ret = CallFunction(keithley6517A_1, serial_interface, hp4192A_1);
                int ret = CallFunction(keithley6517A_1, serial_interface, hp4284A_1);
                if (ret != 0) break;
            }
        }
        else if (keithleyNo == 99) break;
        else {
            std::cout << "Enter correct number..." << std::endl;
            keithleyNo = 9;
        }
    }
    delete serial_interface;
    delete keithley2410_1;
    delete keithley6517A_1;
    //  delete keithley6517B_1;
    delete hp4192A_1;
    delete hp4284A_1;

    std::cout << "Closing application..." << std::endl;

    return 0;
    */
}
int IVCVCtrl::test(serial_interface* si)
{
    std::string buf;

 
    for (int ii = 0; ii < 30;ii++) si->manual_send_command();
    return 0;
    si->set_address(29);
    si->write("++auto 0\r\n");
    si->read(buf); 
    std::cout << buf << std::endl;
    si->write("++auto 1\r\n");

    si->write(":SOUR:VOLT:MCON ON\r\n");
    si->write(":SOUR:VOLT:RANGE 1000\r\n");
    si->write(":SENSE:FUNCTION \'CURRENT\'\r\n");
    //  si->write(":SENSE:CURRENT:RANGE 2e-5\r\n");
    //  si->write(":SENSE:CURRENT:RANGE:AUTO ON\r\n");
    si->write(":SYST:ZCHECK ON\r\n");
    Sleep(1000);
    si->write(":SYST:ZCORRECT ON\r\n");
    si->write(":SYST:ZCHECK OFF\r\n");
    si->write("++auto 0\r\n");

    si->read(buf);


    //    int keithley6517A_2_address = 29; //GPIB adress
    //    power_supply* keithley6517A_2 = new keithley6517A(keithley6517A_2_address);
    //    keithley6517A_2->set_voltage(0, 0); //initial voltage
    //    power_supply* keithley = keithley6517A_2;
}