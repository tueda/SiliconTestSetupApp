// power_supply.h
#ifndef INCLUDED_POWER_SUPPLY
#define INCLUDED_POWER_SUPPLY
#include <string>

class serial_interface;

class power_supply
{
 public:
  power_supply(){};
  virtual ~power_supply(){};

  virtual int initialize() = 0;
  virtual int finalize() = 0;
  virtual int reset(serial_interface *si) = 0;
  virtual int configure(serial_interface* si) = 0;
  virtual void setvoltrange(serial_interface* si, double range) = 0;
  virtual int power_on(serial_interface *si) = 0;
  virtual int power_off(serial_interface *si) = 0;
  virtual int config_voltage(serial_interface *si) = 0;
  virtual int config_compliance(serial_interface *si) = 0;
  virtual void read_voltage_and_current(serial_interface *si, double &voltage, double &current) = 0;
  virtual void read_voltage_and_current2(serial_interface *si, double &voltage, double &current) = 0;
  virtual double read_voltage(serial_interface *si) = 0;
  virtual double read_current(serial_interface *si) = 0;

  virtual bool is_on(serial_interface *si) = 0;
  virtual bool is_off(serial_interface *si) = 0;

  virtual int voltage_sweep(serial_interface* si) { return 0; };

  int get_address(){return m_address;};
  double get_voltage(){return m_voltage;};
  double get_compliance(){return m_compliance;};

  virtual void set_address(int address){m_address = address;}; //needed for GPIB devices;
  virtual void set_voltage(double voltage) { m_voltage = voltage;};
  virtual void set_voltage(double voltage, double step_voltage){m_voltage = voltage; m_step_voltage = step_voltage;};
  virtual void set_compliance(double compliance){m_compliance = compliance;};

  void set_sweep_steps(int sweep_steps){m_sweep_steps = sweep_steps;};
  void set_sweep_sleep_in_ms(int sweep_sleep_in_ms){m_sweep_sleep_in_ms = sweep_sleep_in_ms;};

  virtual std::string get_device_type(){return "UNDEFINED";};
  virtual std::string get_ofile_name(){return "data/IV_"+this->get_device_type()+"_gpib"+std::to_string(m_address)+".dat";};
  virtual std::string get_ofile_name_IV(std::string filedir, std::string filename){return "data/"+filedir+"/IV_"+filename+".dat";};

 protected:
  int m_address = -1;
  double m_voltage = 0.;
  double m_step_voltage = 0.;
  double m_compliance = 0.;

  int m_sweep_steps = 50;
  int m_sweep_sleep_in_ms = 0;

};

#endif
