#include "pch.h"
#include "SiliconTestSetup.h"
#include <msclr/marshal_cppstd.h>

SiliconTestSetup::SiliconTestSetup() {
	isGPIBOpen = false;
	vol1 = new std::vector<double>();
	curr = new std::vector<double>();
	vol2 = new std::vector<double>();
	cap = new std::vector<double>();
	deg = new std::vector<double>();
	freq = new std::vector<double>();
	activepsname = new std::string();
	activelcrname = new std::string();
	activepsVtestname = new std::string();
	activepsHVname = new std::string();
	activelcr1name = new std::string();
	activelcr2name = new std::string();
}
int SiliconTestSetup::initialize() {
	isGPIBOpen = false;
	//---Making serial interface for devices---
	si = new prologix_gpibusb();
	std::string device_name = "COM5";
	si->set_device_name(device_name);
	si->initialize();
	if (!si->is_initialized()) {
		std::cout << " [ERROR] failed to create an instance of the serial interface for " << device_name << "." << std::endl;
		return -1;
	}
	isGPIBOpen = true;
	dh = new DataHandler();

//	tpc = new TeslaProbeCtrl();
//	tpc->Initialize(22);
//	tpc->SetSerialInterface(si);

	usbpio = new TUSBPIOCtrl(0);
	ivcv = new IVCVCtrl();
	psl = new std::map<power_supply*, std::string>();
	lcrl = new std::map<LCR_meter*, std::string>();
	psl->clear();
	lcrl->clear();
	runIV = false;
	runCV = false;
	runCF = false;

	return 0;
}
std::string SiliconTestSetup::getNow() {
	DateTime dt = DateTime::Now;
//	return msclr::interop::marshal_as<std::string>(dt.ToString(L"r"));
	return msclr::interop::marshal_as<std::string>(dt.ToString(L"dd-MM-yyyy HH:mm:ss"));
}

/*
void SiliconTestSetup::SetTestV(std::string psname) {
	std::string firstps;
	int secondps;
	firstps = psname.substr(0, psname.find(":"));
	secondps = atoi((psname.substr(psname.find(":") + 1)).c_str());

	//secondps->get_address();
//	int secondpsnum = static_cast<int>(secondps);
	std::cout << firstps << std::endl;
	std::cout << secondps << std::endl;
	
	if (firstps == "Keithley6517A") { activeps = new keithley6517A(secondps); }
	else if (firstps == "Keithley2410") { activeps = new keithley2410(secondps); }
	else { std::cout << "Can't set TestV!!" << std::endl; }
//	si = new serial_interface;
	*activepsname = firstps;
}
void SiliconTestSetup::SetHV(std::string psname) {
	std::string firstps;
	int secondps;
	firstps = psname.substr(0, psname.find(":"));
	secondps = atoi((psname.substr(psname.find(":") + 1)).c_str());

	//secondps->get_address();
//	int secondpsnum = static_cast<int>(secondps);
	std::cout << firstps << std::endl;
	std::cout << secondps << std::endl;
	if (firstps == "Keithley6517A") {activeps = new keithley6517A(secondps); }
	else if (firstps == "Keithley2410") { activeps = new keithley2410(secondps); }
	else { std::cout << "Can't set TestV!!" << std::endl; }
//	si = new serial_interface;
	*activepsname = firstps;
}
void SiliconTestSetup::SetCint(std::string lcrname) {
	std::string firstps;
	int secondps;
	firstps = lcrname.substr(0, lcrname.find(":"));
	secondps = atoi((lcrname.substr(lcrname.find(":") + 1)).c_str());

	//secondps->get_address();
//	int secondpsnum = static_cast<int>(secondps);
	std::cout << firstps << std::endl;
	std::cout << secondps << std::endl;
	if (firstps == "hp4192A") { activelcr = new hp4192A(secondps); }
	if (firstps == "hp4284A") { activelcr = new hp4284A(secondps); }
	*activepsname = firstps;
}
void SiliconTestSetup::SetCcp(std::string lcrname) {
	std::string firstps;
	int secondps;
	firstps = lcrname.substr(0, lcrname.find(":"));
	secondps = atoi((lcrname.substr(lcrname.find(":") + 1)).c_str());

	//secondps->get_address();
//	int secondpsnum = static_cast<int>(secondps);
	std::cout << firstps << std::endl;
	std::cout << secondps << std::endl;
	if (firstps == "hp4192A") { activelcr = new hp4192A(secondps); }
	if (firstps == "hp4284A") { activelcr = new hp4284A(secondps); }
	*activepsname = firstps;
}
*/
void SiliconTestSetup::setCurrentActiveDeviceQA(std::string psHVname, std::string psVtestname, std::string lcr1name, std::string lcr2name) {
	std::cout << "setting device for QA now..." << std::endl;
	for (auto xx : *psl) {
		if (psHVname.substr(0, psHVname.find(":")) == xx.second && atoi((psHVname.substr(psHVname.find(":") + 1)).c_str()) == xx.first->get_address()) {
			std::cout << "use device HV: " << xx.first->get_address() << " " << xx.second << std::endl;
			activepsHV = xx.first;
			*activepsHVname = xx.second;
			break;
		}
		if (psVtestname.substr(0, psVtestname.find(":")) == xx.second && atoi((psVtestname.substr(psVtestname.find(":") + 1)).c_str()) == xx.first->get_address()) {
			std::cout << "use device Vtest: " << xx.first->get_address() << " " << xx.second << std::endl;
			activepsVtest = xx.first;
			*activepsVtestname = xx.second;
			break;
		}
	}
	for (auto xx : *lcrl) {
		if (lcr1name.substr(0, lcr1name.find(":")) == xx.second && atoi((lcr1name.substr(lcr1name.find(":") + 1)).c_str()) == xx.first->get_address()) {
			std::cout << "use device : " << xx.first->get_address() << " " << xx.second << std::endl;
			activelcr1 = xx.first;
			*activelcr1name = xx.second;
			break;
		}
		if (lcr2name.substr(0, lcr2name.find(":")) == xx.second && atoi((lcr2name.substr(lcr2name.find(":") + 1)).c_str()) == xx.first->get_address()) {
			std::cout << "use device : " << xx.first->get_address() << " " << xx.second << std::endl;
			activelcr2 = xx.first;
			*activelcr2name = xx.second;
			break;
		}
	}


}

void SiliconTestSetup::setCurrentActiveDevice(std::string psname, std::string lcrname) {
	std::cout << psname<<"setting now..." << std::endl;
	for (auto xx : *psl) {
		if (psname.substr(0, psname.find(":")) == xx.second && atoi((psname.substr(psname.find(":") + 1)).c_str()) == xx.first->get_address()) {
			std::cout << "use device : " << xx.first->get_address() << " " << xx.second << std::endl;
			activeps = xx.first;
			*activepsname = xx.second;
			break;
		}
	}
	for (auto xx : *lcrl) {
		if (lcrname.substr(0, lcrname.find(":")) == xx.second && atoi((lcrname.substr(lcrname.find(":") + 1)).c_str()) == xx.first->get_address()) {
			std::cout << "use device : " << xx.first->get_address() << " " << xx.second << std::endl;
			activelcr = xx.first;
			*activelcrname = xx.second; 
			break;
		}
	}
}

void SiliconTestSetup::ClearData() {
	vol1->clear();
	curr->clear();
	vol2->clear();
	cap->clear();
	deg->clear();
	freq->clear();
}
void SiliconTestSetup::RecordHeaderToFile() {
	std::cout << "writing header to file : " << dh->GetFullDataFilename() << std::endl;
//	std::cout << "writing header to file : " << dh->GetDataFilename() << std::endl;
	std::ofstream ofscon(dh->GetFullDataFilename().c_str(), std::ofstream::out);
//	std::ofstream ofscon(dh->GetDataFilename().c_str(), std::ofstream::out);
	ofscon << "==========================" << std::endl;
	ofscon << "Device : " << std::endl;
	if (activeps) ofscon << "    " << (*activepsname) << " " << activeps->get_address() << std::endl;
	if (activelcr ) ofscon << "    " << (*activelcrname) << " " << activelcr->get_address() << std::endl;
	ofscon << "Measurement type " << std::endl;
	ofscon << "    (runIV, runCV, runCF) = (" << runIV << ", " << runCV <<", " << runCF << ")" << std::endl;
	ofscon << "    LCRtype : " << (LCRfunctype==0?"Cp-D":"Z-theta(dig)")  << "  if LCR meter used..."<< std::endl;
	ofscon << "==========================" << std::endl;
	ofscon << "[Scan Parameters] " << std::endl;
	ofscon << "Negative voltage  ? " << isneg << " " << std::endl;
	ofscon << "Current limet     : " << curlimit << " uA " << std::endl;
	ofscon << "Start Voltage     : " << volstart << " V" << std::endl;
	ofscon << "EndVoltage        : " << volend << " V" << std::endl;
	ofscon << "Voltage Step      : " << volstep << " V" << std::endl;
	ofscon << "interval          : " << interval << " s " << std::endl;
	ofscon << "ramping time      : " << rampingt << " ms" << std::endl;
	ofscon << "Frequency (fixed) : " << frequency << " kHz" << std::endl;
	ofscon << "Start Freqequency : " << freqstart << " kHz" << std::endl;
	ofscon << "End Frequency     : " << freqend << " kHz" << std::endl;
	ofscon << "Voltage (fixed)   : " << fixedvolt << " V" << std::endl;

	ofscon.close();
}
void  SiliconTestSetup::DoOpenCalibration() {
	if (!activelcr) {
		std::cout << "no lcr meter set" << std::endl;
		return;
	}
	activelcr->zeroopen(si);
	std::cout << "zero open calibration done" << std::endl;
}
void SiliconTestSetup::DoShortCalibration() {
	if (!activelcr) {
		std::cout << "no lcr meter set" << std::endl;
		return;
	}
	activelcr->zeroshort(si);
	std::cout << "zero short calibration done" << std::endl;
}


void  SiliconTestSetup::ReadSingleCurrent(float& cur) {
	if (!activeps) {
		if (!activepsVtest) {
			std::cout << " To supply fixed voltage please set power supply device" << std::endl;
			return;
		}
	}
	if (!activepsVtest) {
		std::cout << "setting voltage to " << fixedvolt << std::endl;
		activeps->reset(si);
		activeps->configure(si);
		activeps->set_compliance(curlimit * uA);
		activeps->config_compliance(si);
		activeps->set_voltage(0.0);
		activeps->config_voltage(si);
		activeps->power_on(si);
		activeps->set_voltage(singlevolt);
		std::cout << "setting voltage : " << singlevolt << std::endl;
		activeps->set_sweep_steps(1);
		activeps->set_sweep_sleep_in_ms(rampingt);
		activeps->voltage_sweep(si);
		cur = activeps->read_current(si);
	}
	else if (!activeps) {
		std::cout << "setting voltage to " << fixedvolt << std::endl;
		activepsVtest->reset(si);
		activepsVtest->configure(si);
		activepsVtest->set_compliance(curlimit * uA);
		activepsVtest->config_compliance(si);
		activepsVtest->set_voltage(0.0);
		activepsVtest->config_voltage(si);
		activepsVtest->power_on(si);
		activepsVtest->set_voltage(singlevolt);
		std::cout << "setting voltage : " << singlevolt << std::endl;
		activepsVtest->set_sweep_steps(1);
		activepsVtest->set_sweep_sleep_in_ms(rampingt);
		activepsVtest->voltage_sweep(si);
		cur = activepsVtest->read_current(si);
	}
}
void  SiliconTestSetup::ReadSingleImpedance(float& cap, float& deg) {
	if (!activelcr) {
		std::cout << " LCR meter device is not set" << std::endl;
		return;
	}
	activelcr->reset(si);
	activelcr->configure(si);
	activelcr->set_displayfunc(si, LCRfunctype);
	std::cout << "setting freq : " << singlefreq << " [kHz]"  << std::endl;

	activelcr->set_frequency(singlefreq * kHz);
	activelcr->config_frequency(si);
	Sleep(interval);
	double ca = -1, de = -1;
	std::cout << singlefreq / kHz << " [kHz]" << std::endl;
	activelcr->read_capacitance_and_degree(si, ca, de);
	cap = ca;
	deg = de;
}

int SiliconTestSetup::stopRun() {
	std::cout << "==== scan terminated by Stop ==== " << std::endl;

	if (!activeps) {
		if (!activepsVtest) {
			std::cout << " Power Supply device is not set" << std::endl;
		}
	}
	else {
		if (!activepsVtest) {
			std::cout << "ramping down" << std::endl;
			int ram = 1;
			if (abs(fixedvolt) / 5 > 1)ram = abs(fixedvolt) / 5;
			activeps->set_sweep_steps(ram);
			activeps->set_sweep_sleep_in_ms(rampingt);
			activeps->set_voltage(0, fixedvolt);
			activeps->voltage_sweep(si);
			activeps->power_off(si);
		}
		else if (!activeps) {
			std::cout << "ramping down" << std::endl;
			int ram = 1;
			if (abs(fixedvolt) / 5 > 1)ram = abs(fixedvolt) / 5;
			activepsVtest->set_sweep_steps(ram);
			activepsVtest->set_sweep_sleep_in_ms(rampingt);
			activepsVtest->set_voltage(0, fixedvolt);
			activepsVtest->voltage_sweep(si);
			activepsVtest->power_off(si);
		}
	}

	if (!activelcr) {
		std::cout << " LCR meter device is not set" << std::endl;
	}
	else {
		activelcr->reset(si);
		activelcr->configure(si);
		activelcr->set_displayfunc(si, LCRfunctype);

	}
	std::cout << "==== scan finished ==== " << std::endl;

	return 0;
}

int SiliconTestSetup::runCFScan() {
	runCF = true;
	if (!activelcr) {
		std::cout << " LCR meter device is not set" << std::endl;
		return -1;
	}
	if (fixedvolt != 0) {
		if (!activeps) {
			std::cout << " To supply fixed voltage please set power supply device" << std::endl;
			return -1;
		}
		else {
			std::cout << "setting voltage to " << fixedvolt << std::endl;
			activeps->reset(si);
			activeps->configure(si);
			activeps->set_compliance(curlimit * uA);
			activeps->config_compliance(si);
			activeps->set_voltage(0.0);
			activeps->config_voltage(si);
			activeps->power_on(si);
			activeps->set_voltage(isneg ? -1 * fabs(fixedvolt) : fabs(fixedvolt));
			activeps->set_sweep_steps(1);
			activeps->set_sweep_sleep_in_ms(rampingt);
			activeps->voltage_sweep(si);
			double cu = activeps->read_current(si);
			std::ofstream ofscon(dh->GetFullDataFilename().c_str(), std::ofstream::app);
			ofscon << "==========================" << std::endl;
			ofscon << "[pre-set voltage] " << std::endl;
			ofscon << "fixed voltage : " << fixedvolt << std::endl;
			ofscon << "current value : " << cu << std::endl;
			std::cout << "current value : " << cu << std::endl;
			ofscon.close();

		}
	}
	double unit;
	if (LCRfunctype == 0)unit = pF;
	else if (LCRfunctype == 1) unit = kOhm;

	activelcr->reset(si);
	activelcr->configure(si);
	activelcr->set_displayfunc(si,LCRfunctype);


	RecordHeaderToFile();

	std::ofstream ofscon(dh->GetFullDataFilename().c_str(), std::ofstream::app);
	ofscon << "==========================" << std::endl;
	ofscon << "[Data Format] " << std::endl;
	if (LCRfunctype == 0)ofscon << "Frequency[Hz] capacitance[F] D time(dd-MM-yyyy HH:mm:ss)" << std::endl;
	else ofscon << "Frequency[Hz] impedance[Ohm] Phase[deg] time(dd-MM-yyyy HH:mm:ss)" << std::endl;

	int fst = 1;
	int thisfreq = freqstart*kHz;
	while (thisfreq <= freqend*kHz) {
		if (thisfreq > 0 && thisfreq < 1e2) fst = 5;
		else if (thisfreq >= 1e2 && thisfreq < 1e3) fst = 100;
		else if (thisfreq >= 1e3 && thisfreq < 1e4) fst = 1000;
		else if (thisfreq >= 1e4 && thisfreq < 1e5) fst = 10000;
		else if (thisfreq >= 1e5 && thisfreq <= 1e6) fst = 100000;
		else if (thisfreq > 1e6) break;
		activelcr->set_frequency(thisfreq);
		activelcr->config_frequency(si);
		Sleep(interval);
		double ca = -1, de = -1;
		std::cout << thisfreq / kHz << " [kHz]" << std::endl;
		activelcr->read_capacitance_and_degree(si, ca, de);
		freq->push_back(thisfreq/kHz);
		cap->push_back(ca / unit);
		deg->push_back(de);

		std::string nowtime = getNow();
		ofscon << thisfreq << " " << ca << " " << de << " " << nowtime << std::endl;
		std::cout  << thisfreq << " " << ca << " " << de << " " << nowtime << std::endl;

		thisfreq += fst;
	}
	if (fixedvolt != 0) {
		std::cout << "ramping down" << std::endl;
		int ram = 1;
		if (abs(fixedvolt) / 5 > 1)ram = abs(fixedvolt) / 5;
		activeps->set_sweep_steps(ram);
		activeps->set_sweep_sleep_in_ms(rampingt);
		activeps->set_voltage(0, fixedvolt);
		activeps->voltage_sweep(si);
		activeps->power_off(si);

	}
	ofscon.close();
	activelcr->reset(si);
	activelcr->configure(si);
	activelcr->set_displayfunc(si, LCRfunctype);

	std::cout << "==== scan finished ==== " << std::endl;

	return 0;
}
int SiliconTestSetup::runIVCVScan(bool iv, bool cv) {
	
	runIV = iv;
	runCV = cv;
	if ((runIV || runCV) && !activeps && !activepsVtest) {
		std::cout << " Power Supply device is not set" << std::endl;
		runIV = false;
	}
	if (runCV && !activelcr && !activelcr1 && !activelcr2) {
		std::cout << " LCR meter device is not set" << std::endl;
		runCV = false;
	}
	if (!runIV && !runCV) {
		std::cout << "nothing happened..  no device set" << std::endl;
		return -1;
	}
	std::cout << "setting active devices " << std::endl;
	if ((runIV || runCV) && !activepsVtest) {
		activeps->reset(si);
		activeps->configure(si);
		activeps->setvoltrange(si, volend);
		activeps->set_compliance(curlimit * uA);
		activeps->config_compliance(si);
		activeps->set_voltage(0.);
		activeps->config_voltage(si);
		activeps->power_on(si);
	}
	if ((runIV || runCV) && !activeps) {
		activepsVtest->reset(si);
		activepsVtest->configure(si);
		activepsVtest->setvoltrange(si, volend);
		activepsVtest->set_compliance(curlimit * uA);
		activepsVtest->config_compliance(si);
		activepsVtest->set_voltage(0.);
		activepsVtest->config_voltage(si);
		activepsVtest->power_on(si);
	}
	if (runCV) {
		activelcr->reset(si);
		activelcr->configure(si);
		activelcr->set_displayfunc(si, LCRfunctype);
		activelcr->set_frequency(frequency * kHz);
		activelcr->config_frequency(si);

	}
	std::cout << "setting active devices done..." << std::endl;
	RecordHeaderToFile();


	//	float curlimit;
	//	float rampingt;
	//	float frequency;
	std::ofstream ofscon(dh->GetFullDataFilename().c_str(), std::ofstream::app);
	ofscon << "==========================" << std::endl;
	ofscon << "[Data Format] " << std::endl;
	if (LCRfunctype == 0)ofscon << "voltage[V] current[A] capacitance[F] D time(dd-MM-yyyy HH:mm:ss)" << std::endl;
	else ofscon << "voltage[V] current[A] impedance[Ohm] Phase[deg] time(dd-MM-yyyy HH:mm:ss)" << std::endl;

	double thisvolt = volstart;
	bool reachedtocurrentlimit = false;
	while (thisvolt <= volend) {
		std::cout << "== set Voltage to " << thisvolt << "V =="<< std::endl;
		double volt;
		volt = isneg ? -1 * thisvolt : thisvolt;
//		activeps->set_voltage(0.);
//		activeps->config_voltage(si);
		/*
		double v1 = thisvolt;
		double v2 = thisvolt;
		double cu = sqrt(0.1 + thisvolt);
		double ca = 1 / (1 + sqrt(thisvolt));
		*/
		double v1=-1,v2=-1,cu=-1,ca=-1,de=-1;
		
		if ((runIV || runCV) && !activepsVtest) {
			activeps->set_voltage(volt);
			activeps->set_sweep_steps(1);
			activeps->set_sweep_sleep_in_ms(rampingt);
			std::cout << si << std::endl;
			//			activeps->config_voltage(si);
			//			activeps->read_voltage_and_current(si, v1, cu);
			cu = activeps->read_current(si);

			if (cu == 0) {
				cu = 1e-6 * uA;
				std::cout << " current value is 0... assigned 1e-6 uA" << std::endl;
			}
			std::cout << "(volt,curr)=(" << volt << ", " << cu << ")" << std::endl;

			if (runIV) {
				/*
				if (volt == 0) {
					// protection for the log scale plot
					vol1->push_back(1e-20);
				}
				else{
					vol1->push_back(fabs(volt));
				}
				*/
				vol1->push_back(fabs(volt));
				curr->push_back(fabs(cu / uA));
			}
			if (abs(cu) > abs(curlimit * uA)) reachedtocurrentlimit = true;
		}
		if ((runIV || runCV) && !activeps) {
			activepsVtest->set_voltage(volt);
			activepsVtest->set_sweep_steps(1);
			activepsVtest->set_sweep_sleep_in_ms(rampingt);
			std::cout << si << std::endl;
			//			activeps->config_voltage(si);
			//			activeps->read_voltage_and_current(si, v1, cu);
			cu = activepsVtest->read_current(si);

			if (cu == 0) {
				cu = 1e-6 * uA;
				std::cout << " current value is 0... assigned 1e-6 uA" << std::endl;
			}
			std::cout << "(volt,curr)=(" << volt << ", " << cu << ")" << std::endl;

			if (runIV) {
				/*
				if (volt == 0) {
					// protection for the log scale plot
					vol1->push_back(1e-20);
				}
				else{
					vol1->push_back(fabs(volt));
				}
				*/
				vol1->push_back(fabs(volt));
				curr->push_back(fabs(cu / uA));
			}
			if (abs(cu) > abs(curlimit * uA)) reachedtocurrentlimit = true;
		}
		Sleep(1000);
		if(runCV){
			activelcr->read_capacitance_and_degree(si,ca,de);
			double unit;
			if (LCRfunctype == 0)unit = pF;
			else if (LCRfunctype == 1) unit = kOhm;
			if (ca == 0)ca = 1e-6 * unit;
			if (de == 0)de = 1e-6 ;
			/*
			if (volt == 0) {
				//protection for the log scale plot
				vol2->push_back(1e-20);
			}
			else {
				vol2->push_back(fabs(volt));
			}
			*/
			vol2->push_back(fabs(volt));
			cap->push_back(ca / unit);
			if(!runIV) deg->push_back(de);
			std::cout << "(cap,deg)=(" << ca << ", " << de << ")" << std::endl;

		}
		if (reachedtocurrentlimit)break;
//		std::cout << volt << " " << cu << " " << ca << std::endl;

		std::string nowtime=getNow();
		ofscon << volt << " " << cu << " " << ca << " " << de << " " << nowtime << std::endl;		
		std::cout << volt << " " << cu << " " << ca << " " << de << " " <<nowtime << std::endl;


		Sleep(interval * 1000);
		thisvolt += volstep;

	}
	ofscon.close();
	stopRun();
	/*
	std::cout << "ramping down" << std::endl;
	int ram=1;
	if (abs(thisvolt) / 5 > 1)ram = abs(thisvolt) / 5;
	activeps->set_sweep_steps(ram);
	activeps->set_sweep_sleep_in_ms(rampingt);
	activeps->set_voltage(0, thisvolt);
	activeps->voltage_sweep(si);
	activeps->power_off(si);
	*/
	if (runCV) {
		activelcr->reset(si);
		activelcr->configure(si);
		activelcr->set_displayfunc(si, LCRfunctype);
	}
	std::cout << "==== scan finished ==== " << std::endl;
	return 0;
}

int SiliconTestSetup::removePowerSupply(std::string str, int addr) {
	std::map<power_supply*, std::string>::iterator it;
	for (it = psl->begin(); it != psl->end();) {
		if(it->first->get_address() == addr && it->second == str){
			it = psl->erase(it);
		}
		else {
			it++;
		}
	}
	return 0;
}

int SiliconTestSetup::removeLCRmeter(std::string str, int addr) {
	std::map<LCR_meter*, std::string>::iterator it;
	for (it = lcrl->begin(); it != lcrl->end();) {
		if (it->first->get_address() == addr && it->second == str) {
			it = lcrl->erase(it);
		}
		else {
			it++;
		}
	}
	return 0;
}
int SiliconTestSetup::setPowerSupply(std::string str, int addr) {
	std::map<power_supply*, std::string>::iterator it;
	std::cout << str << std::endl;
//	for (it = psl->begin(); it != psl->end(); it++) {
//		if (it->first->get_address() == addr && it->second == str) {
//			std::cout << "same device added already " << std::endl;
//			return -1;
//		}
//	}

	if (str == "keithley2410") {
		power_supply* ps = new keithley2410(addr);
		ps->set_voltage(0, 0); //initial voltage
		psl->insert(std::make_pair(ps,str));
	}
	else if (str == "keithley6517A") {
		power_supply* ps = new keithley6517A(addr);
		ps->set_voltage(0, 0); //initial voltage
		psl->insert(std::make_pair(ps,str));
	}
	else {
		std::cout << "strange source meter : " << str << std::endl;
		return -1;
	}

	std::cout << psl->size() << std::endl;
	return 0;
}
int SiliconTestSetup::setLCRmeter(std::string str, int addr) {
	std::map<LCR_meter*, std::string>::iterator it;
	for (it = lcrl->begin(); it != lcrl->end();it++) {
		if (it->first->get_address() == addr && it->second == str) {
			std::cout << "same device added already " << std::endl;
			return -1;
		}
	}

	if (str == "hp4192A") {
		LCR_meter* lcr = new hp4192A(addr);
		lcrl->insert(std::make_pair(lcr,str));
	}
	else if (str == "hp4284A") {
		LCR_meter* lcr = new hp4284A(addr);
		lcrl->insert(std::make_pair(lcr,str));
	}
	else {
		std::cout << "strange source meter : " << str << std::endl;
		return -1;
	}

//	std::cout << lcrl->size() << std::endl;
	return 0;
}

int SiliconTestSetup::OpenTeslaT200Control() {
	char szCmd[256] = "mstsc /w:1500 /h:820 C:\\Users\\atlasj\\Desktop\\prober.rdp";
	std::cout << "opening Remote Desktop for Probe Station : " << std::endl;
	std::cout << szCmd << std::endl;
	//	return OpenApplication(szCmd);
	return OpenApplication(szCmd);
}
int SiliconTestSetup::OpenApplication(char* szCmd) {
	PROCESS_INFORMATION pi = { 0 };
	STARTUPINFOA si = { sizeof(STARTUPINFO) };
	int ii = CreateProcessA(NULL, szCmd, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi);
	return ii;
}
int SiliconTestSetup::CreateNewConsole(char* szCmd) {
	PROCESS_INFORMATION pi = { 0 };
	STARTUPINFOA si = { sizeof(STARTUPINFO) };
	int ii = CreateProcessA(NULL, szCmd, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi);
	return ii;
}
void SiliconTestSetup::LegacyRun() {
//	CreateNewConsole("");



//	tpc->command_test();

	//	ivcv->test(si);
	ivcv->main_test(si);

	int PortN;
	std::cout << "Select Port No." << std::endl;
	std::cout << "PortA=0,PortB=1,PortC=2" << std::endl;
	std::cin >> PortN;
	usbpio->SetBit(PortN, 0x4);
	usbpio->SetSWon(0);
	system("PAUSE");



}
