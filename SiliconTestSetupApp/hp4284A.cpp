#include "pch.h"
#include "stdafx.h"
#include<iostream>
#include<chrono>
#include<thread>
#include<cmath>

#include"serial_interface.h"
#include"hp4284A.h"
//#include"SerialCom.h"

int hp4284A::initialize()
{
    return 0;
}
int hp4284A::finalize()
{
    return 0;
}

int hp4284A::reset(serial_interface* si)
{
    return 0;
}

int hp4284A::zeroopen(serial_interface* si)
{
    si->set_address(m_address);
    si->write("DISP:PAGE CSET\r\n");
    si->write("CORR:OPEN\r\n"); //calibration between all frequency
    si->write("CORR:OPEN:STAT ON\r\n");
    si->make_listener();
    return 0;
}


int hp4284A::zeroshort(serial_interface* si)
{
    si->set_address(m_address);
    si->write("DISP:PAGE CSET\r\n");
    si->write("CORR:SHOR\r\n");
    si->write("CORR:SHOR:STAT ON\r\n");
    si->make_listener();
    return 0;
}


int hp4284A::configure(serial_interface* si)
{
    si->set_address(m_address);
    si->make_listener();
    si->write("DISP:PAGE MSET\r\n");
    //  si->write("FUNC:IMP CSD\r\n"); //series circuit
    //  si->write("FUNC:IMP CPD"); //parallel circuit
    si->write("FUNC:IMP:RANG:AUTO ON\r\n"); //impedance range:ON
    //  si->write("TRIG:SOUR INT\r\n"); //trigger mode INT
    si->write("INIT:CONT ON\r\n");
    si->write("DISP:PAGE MEAS\r\n");
    si->write("FUNC:IMP CPD\r\n");   // CPD : measure Cp-D,  ZTD : measure Z-theta(deg)    
//    si->write("FUNC:IMP ZTD\r\n");   // CPD : measure Cp-D,  ZTD : measure Z-theta(deg)    
    si->write("TRIG:SOUR INT\r\n"); //trigger mode INT
    //  si->write("TRIG\r\n");
    si->make_listener();
    return 0;
}
void hp4284A::set_displayfunc(serial_interface* si, int type) {
    if (type == 0) {
        si->write("FUNC:IMP CPD\r\n");   // CPD : measure Cp-D,  ZTD : measure Z-theta(deg)    
    }
    else if (type == 1) {
        si->write("FUNC:IMP ZTD\r\n");   // CPD : measure Cp-D,  ZTD : measure Z-theta(deg)    
    }
}


int hp4284A::config_frequency(serial_interface* si)
{
    char cmd[MAX];
    si->set_address(m_address);
    si->make_listener();
    si->write("DISP:PAGE MSET\r\n");
    sprintf_s(cmd, "FREQ %9.4lfHZ\r\n", m_frequency);
    si->write(cmd);
    si->write("DISP:PAGE MEAS\r\n");
    si->make_listener();
    return 0;
}

void hp4284A::read_capacitance_and_degree(serial_interface* si, double& cap, double& deg)
{
    double dummy = 0;
    std::string buffer = "";
    //  memset(buffer.c_str(),0x00,MAX);
    si->set_address(m_address);
    //  si->write("*TRG\r\n"); //triger
    si->write("TRIG\r\n");
    si->write("FETC?\r\n");
    //  si->write(":READ?\r\n");
    while (buffer.length() == 0) {
//        Sleep(200);
        si->write("++read\r\n");
//       Sleep(200);
        si->make_talker();
        si->read(buffer);
        si->make_listener();
    }
    sscanf_s(buffer.c_str(), "%lf,%lf,%lf", &cap, &deg, &dummy);
    std::cout<<buffer<<std::endl;
 
}


void hp4284A::read_capacitance_and_degree2(serial_interface* si, double& cap, double& deg)
{
    std::string buffer2;
    //  memset(buffer.c_str(),0x00,MAX);
    si->set_address(m_address);
    si->write("EX");
    si->write(":READ?\r\n");
    si->make_talker();
    si->read2(buffer2);
    sscanf_s(buffer2.c_str(), "NCPN%lf,NDFN%lf", &cap, &deg);
    si->make_listener();
}

void hp4284A::sweep_frequency(serial_interface* si, double& cap, double& deg, double& freq)
{
    std::string buffer;
    const char* dummy;
    char cmd_sf[MAX], cmd_tf[MAX], cmd_pf[MAX];
    si->set_address(m_address);
    si->write("LIST:MODE STEP\r\n");
    si->write("LIST:FREQ 1E2,2E2,4E2,8E2,1E3,2E3,4E3,1E4,5E4,1E5\r\n");
    si->write("DISP:PAGE LIST\r\n");
    si->write("INIT:CONT ON\r\n");
    si->write("");
    si->write("TRIG\r\n");
    si->write("FETC?\r\n");
    Sleep(200);
    si->write("++read\r\n");
    Sleep(200);

    si->make_talker();
    si->read(buffer);
    //    std::cout<<buffer<<std::endl;
    // //    sscanf(buffer.c_str(),"NCSN%lf,%2sFN%lf,K%lf",&cap,&dummy,&deg,&freq); 
    // //    if(int ii==0) std::cout<<"freq[Hz]"<<"\t"<<"capacitance[F]"<<"\t"<<"degree[deg]"<<"\t"<<std::endl;
    // std::cout<<freq*1000<<"\t\t"<<cap<<"\t\t"<<deg<<"\t"<<std::endl;
    si->make_listener();
}

