#pragma once
#include <string>

class DataHandler
{
private:
	std::string dirpath;
	std::string basedirpath;

	std::string datafilename;
	std::string datafilenameNext;
	std::string imagefilename;
	std::string imagefilenameNext;


public:
	DataHandler();
	~DataHandler() {}
	std::string GetDataDirPath() { return dirpath; }
	std::string GetDataFilename() { return datafilenameNext; }
//	std::string GetFullDataFilename() { return dirpath + datafilenameNext; }
	std::string GetFullDataFilename() { return dirpath + datafilename; }
	std::string GetImageFilename() { return imagefilenameNext; }
	std::string GetFullImageFilename() { return dirpath + imagefilenameNext; }
	void StartNewData();
	void SetDataDirPath(std::string str) { dirpath = str; }
	void SetFilename(std::string str);
	void SetFullFilename(std::string str);
	void SetNextFullImageFilename();
	void MoveTmpFilesToBackup();
	std::string DuplicationChecker(std::string fullpath,int numlength=4);
	std::string DuplicationChecker(std::string dir, std::string file,int numlength=4);

};

