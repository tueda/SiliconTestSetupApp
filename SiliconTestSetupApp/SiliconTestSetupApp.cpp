#include "pch.h"
#include <windows.h>
#include "ControlGUI.h"
#include <iostream>
#include "prologix_gpibusb.h"
#include "IVCVCtrl.h"
#include "TUSBPIOCtrl.h"
#include "TeslaProbeCtrl.h"
using namespace System;
using namespace SiliconTestSetupApp;

void TestComport();

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{

	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	Application::Run(gcnew ControlGUI());
//	TestComport();

     return 0;
}

void TestComport() {

	//---Making serial interface for devices---
	serial_interface* serial_interface = new prologix_gpibusb();
	std::string device_name = "COM5";
	serial_interface->set_device_name(device_name);
	serial_interface->initialize();
	if (!serial_interface->is_initialized()) {
		std::cout << " [ERROR] failed to create an instance of the serial interface for " << device_name << "." << std::endl;
		return;
	}

	TeslaProbeCtrl tpc;
	tpc.Initialize();
	tpc.SetSerialInterface(serial_interface);
	tpc.command_test();

	TUSBPIOCtrl usbpio(0);
	int PortN;
	std::cout << "Select Port No." << std::endl;
	std::cout << "PortA=0,PortB=1,PortC=2" << std::endl;
	std::cin >> PortN;
	usbpio.SetBit(PortN, 0x4);
	usbpio.SetSWon(0);
	system("PAUSE");

	IVCVCtrl ivcv;
//	ivcv.test(serial_interface);
	ivcv.main_test(serial_interface);

}