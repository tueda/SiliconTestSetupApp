#include "stdafx.h"
#include "pch.h"
#include "prologix_gpibusb.h"
//#define DEBUG

prologix_gpibusb::prologix_gpibusb() {};
prologix_gpibusb::~prologix_gpibusb() {};

int prologix_gpibusb::manual_send_command() {
    ///////////////////////////
         // Append CR and LF
    std::string cmd;

    std::cout << "Command: ";

    // Read command from console
    //gets( cmd );
    std::cin >> cmd;

    std::string  buffer;
    int ret = m_serial->writeread(cmd, buffer);
    return ret;
 
}
int prologix_gpibusb::initialize()
{
    m_address = -1;
    m_initialized = false;

    m_serial = new PrologixGPIB(m_device_name);
    m_serial->initialize();
    if (!m_serial->is_initialized()) return -1;
 
    /////////////////////////////
  //  std::string command = "++mode 1\r\n";
    std::string command = "++mode 1\r\n";
#ifdef DEBUG
    std::cout << this->get_device_name() << ": " << command << std::endl;
#endif
    m_serial->write(command);
    m_initialized = true;
    return 0;
};

int prologix_gpibusb::finalize()
{
    delete m_serial;
    return 0;
};

int prologix_gpibusb::set_address(int address)
{
    m_address = address;
    //  std::string command = "++addr "+std::to_string(address)+R"(\r\n)";
    std::string command = "++addr " + std::to_string(address) + "\r\n";

    m_serial->write(command);

    return 0;
}

int prologix_gpibusb::write(std::string command)
{
#ifdef DEBUG
    std::cout << this->get_device_name() << ": " << command << std::endl;
#endif
    m_serial->write(command);
    return 0;
}

std::string prologix_gpibusb::writeread(std::string command,std::string& buffer)
{
    m_serial->writeread(command,buffer);
    return "";
}
std::string prologix_gpibusb::read(std::string& buffer)
{
    m_serial->read(buffer);
    return "";
}

std::string prologix_gpibusb::read1(std::string& buffer1)
{
    m_serial->read(buffer1);
    return "";
}

std::string prologix_gpibusb::read2(std::string& buffer2)
{
    m_serial->read(buffer2);
    return "";
}

int prologix_gpibusb::make_listener()
{
    std::string command = "++auto 0\r\n";
#ifdef DEBUG
    std::cout << this->get_device_name() << ": " << command << std::endl;
#endif
    m_serial->write(command);
    return 0;
}

int prologix_gpibusb::make_talker()
{
    std::string command = "++auto 1\r\n";
#ifdef DEBUG
    std::cout << this->get_device_name() << ": " << command << std::endl;
#endif
    m_serial->write(command);

    return 0;
}
