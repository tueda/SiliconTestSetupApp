#include "pch.h"
#include "PrologixGPIB.h"

//#define DEBUG 
PrologixGPIB::PrologixGPIB() {
    port = "";
    initialized = false;

}
int PrologixGPIB::initialize() {
    // Open port
    std::cout << "opening port " << port << std::endl;
    error = PxSerialOpen(port.c_str());
    if (error != 0)
    {
 //       printf("Error %08x opening %s.\n", error, port);
        std::cout << "Error " << error << " opening " << port << std::endl;

        return -1;
    }
    initialized = true;

    return 0;
}
int PrologixGPIB::finalize() {
    return ClosePort();
}
int PrologixGPIB::ClosePort() {
    // Close port
    error = PxSerialClose();
    if (error != 0)
    {
 //       printf("Error %08x closing %s.\n", error, port);
        std::cout << "Error " << error << " closing " << port << std::endl;

    }
    return 0;
}
int PrologixGPIB::write(char* buf, size_t length) {
    //   return ::write(dev, buf, length);
    DWORD  m_written = 0;
#ifdef DEBUG
    std::cout << "writing" << ": " << buf << std::endl;
#endif
    return PxSerialWrite(buf, length, &m_written);
}

int PrologixGPIB::write(std::string bufstr) {
//    return ::write(dev, buf.c_str(), buf.size());
    const char* bufc = bufstr.c_str();
    DWORD m_written = 0;
#ifdef DEBUG
    std::cout << "writing" << ": " << bufc << std::endl;
#endif
    return PxSerialWrite((char*)bufc, bufstr.size(), &m_written);
}
int PrologixGPIB::read(char* buf, size_t length) {
    //    return ::read(dev, buf, length);
    DWORD* const m_read = 0;
    int ret= PxSerialRead(buf, length, m_read);
#ifdef DEBUG
    std::cout << "c reading" << " : " << buf << std::endl;
#endif
    return ret;
}



int PrologixGPIB::read(std::string& buf) {

 //   char* tmp = new char[MAX_READ];
    char tmp[MAX_READ]="";
//    std::cout << MAX_READ << std::endl;
 //    char tmp[MAX_READ];
    //    unsigned n_read = ::read(dev, tmp, MAX_READ);
    DWORD m_read;
//    unsigned n_read = PxSerialRead(tmp, MAX_READ, m_read);
    error = PxSerialRead(tmp, MAX_READ, &m_read);
    if (m_read > 0) {
//        std::cout << m_read << std::endl;
//        for (int ii = 0; ii < m_read - 2; ii++) std::cout << (int)(tmp[ii]+5) << std::endl;
        tmp[m_read] = 0;
        buf = std::string(tmp, m_read - 2);
#ifdef DEBUG
        std::cout << "tmp: " << tmp << " " << m_read << " " << MAX_READ << std::endl;
        std::cout << "s reading" << " : " << buf << std::endl;
#endif
    }
    else {
//        std::cout << "received 0 size " << std::endl;
        buf = "";
    }
    return error;
}

int PrologixGPIB::writeread(std::string _cmd, std::string& buf) {
    cmd = _cmd;


    // Append CR and LF
    char buffer[256];
    sprintf_s(buffer, "%s\r\n", _cmd.c_str());
    // Write command to port
    DWORD written = 0;
    error = PxSerialWrite(buffer, (DWORD)strlen(buffer), &written);


//    const char* bufc = _cmd.c_str();
//    DWORD m_written = 0;
//    error = PxSerialWrite((char*)bufc, _cmd.size(), &m_written);
//    error = PxSerialWrite((char*)buffer, _cmd.size(), &m_written);

    if (error != 0)
    {
//        printf("Error %08x writing %s.\n", error, port.c_str());
        std::cout << "Error " << error << " writing " << port << std::endl;
        return -1;
    }
    // TODO: Adjust timeout as needed
    DWORD elapsedTime = 0;
    DWORD lastRead = timeGetTime();


    // Read until TIMEOUT time has elapsed since last
    // successful read.
    while (elapsedTime <= TIMEOUT)
    {
        DWORD bytesRead;

        error = PxSerialRead(buffer, sizeof(buffer) - 1, &bytesRead);
        if (error != 0)
        {
    //        printf("Error %08x reading %s.\n", error, port);
            std::cout << "Error " << error << " reading " << port << std::endl;

            break;
        }

        if (bytesRead > 0)
        {
            buffer[bytesRead] = 0;    // Append NULL to print to console
   //         printf("[return] %s", buffer);
            std::cout << "[return] " << buffer << std::endl;
            buf = buffer;

            lastRead = timeGetTime();
        }
        elapsedTime = timeGetTime() - lastRead;
    }
    std::cout << std::endl;
    return 0;
}

