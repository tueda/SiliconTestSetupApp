#pragma once
// prologix_gpibusb.h
#ifndef INCLUDED_PROLOGIX_GPIBUSB
#define INCLUDED_PROLOGIX_GPIBUSB
#include "pch.h"
#include "serial_interface.h"
#include "PrologixGPIB.h"

//#define DEBUG


 class prologix_gpibusb :
	public serial_interface
{
public:
    prologix_gpibusb();
    ~prologix_gpibusb();

    int initialize();
    int finalize();

    int set_address(int address);
    int manual_send_command();
    int write(std::string command);
    std::string writeread(std::string command, std::string& buffer);
    std::string read(std::string& buffer);
    std::string read1(std::string& buffer1);
    std::string read2(std::string& buffer2);

    int make_listener();
    int make_talker();

private:
    PrologixGPIB* m_serial;
};

#endif
